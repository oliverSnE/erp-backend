<?php

use Illuminate\Http\Request;
use App\Models\Insumo;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::POST('modelos/import','ModeloController@import'); 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('ubicacion/destajo', 'UbicacionController@empeladosDestajo');
Route::post('login', 'UserController@authenticate');
Route::group(['middleware' => ['jwt.verify']], function() {
    /* USUARIOS */
    Route::post('register', 'UserController@register');
    Route::get('users/destroy/{id}', 'UserController@destroy');
    Route::get('users', 'UserController@users');
    Route::get('users/roles', 'UserController@roles');
    Route::get('users/{id}', 'UserController@userById');
    Route::get('user', 'UserController@user');
    Route::put('users/update', 'UserController@update');
    Route::get('refresh', 'UserController@refresh');
    Route::get('logout', 'UserController@logout');
    Route::post('users/change-locations', 'UserController@changeUbicaciones');
    Route::get('users/availables-locations/{user_id}', 'UserController@ubicacionesDisponibles')->where('user_id', '[0-9]+');
    /**********   DESTAJOS  ********/
    Route::POST('destajos','DestajoController@create');
    Route::PUT('destajos/detalle/edit','DestajoController@editDetalle');
    Route::GET('destajos','DestajoController@index');
    Route::GET('destajos/{id}','DestajoController@show')->where('id', '[0-9]+');
    Route::GET('destajos/detalle/{detalle_id}','DestajoController@getDetalleById')->where('detalle_id', '[0-9]+');
    Route::POST('destajos/name','DestajoController@searchByName');
    Route::DELETE('destajos/destroy/{id}','DestajoController@destroy')->where('id', '[0-9]+');
    Route::get('destajos/tipos','DestajoController@tipos');

    // ORDEN DE COMPRA
    Route::get('orden-compra', 'OrdenCompraController@findAll');
    Route::get('orden-compra/fraccionamiento/{fraccionamiento_id}', 'OrdenCompraController@findAllByFraccionamiento')->where('fraccionamiento_id', '[0-9]+');
    Route::POST('orden-compra/ubicacion', 'OrdenCompraController@ubicacion');
    Route::get('orden-compra/verificacion/{id}', 'OrdenCompraController@verificacion')->where('id', '[0-9]+');
    Route::POST('orden-compra/destajos', 'OrdenCompraController@destajos');
    Route::POST('orden-compra/subcontratos', 'OrdenCompraController@subcontratos');
    Route::POST('orden-compra/subcontratos/insumos', 'OrdenCompraController@subcontratosInsumos');
    Route::post('orden-compra/create', 'OrdenCompraController@create');
    Route::post('orden-compra/insumos/create', 'OrdenCompraController@createOnlyInsumos');
    Route::get('orden-compra/{id}', 'OrdenCompraController@find')->where('id', '[0-9]+');
    Route::get('orden-compra/folio/{folio}', 'OrdenCompraController@findFolio');
    Route::get('orden-compra/aprove/{id}', 'OrdenCompraController@aprove')->where('id', '[0-9]+');
    Route::get('orden-compra/cancel/{id}', 'OrdenCompraController@cancel')->where('id', '[0-9]+');
    Route::get('orden-compra/completed/{id}', 'OrdenCompraController@completed')->where('id', '[0-9]+');
    Route::put('orden-compra/update-detalle', 'OrdenCompraController@updateDetalle');
    Route::get('orden-compra/pdf/{id}','OrdenCompraController@pdf')->where('id', '[0-9]+');
    Route::post('orden-compra/add-factura','OrdenCompraController@addFactura');
    Route::get('orden-compra/facturas/{id}','OrdenCompraController@facturas')->where('id', '[0-9]+');
    Route::get('orden-compra/facturas/download/{factura_id}','OrdenCompraController@downloadFactura')->where('factura_id', '[0-9]+');
    Route::get('orden-compra/special/{model_id}','OrdenCompraController@getSpecialOrders')->where('model_id', '[0-9]+');
    Route::post('orden-compra/re-orden', 'OrdenCompraController@reOrden');


    Route::GET('insumos', 'InsumoController@index');
    Route::GET('insumos/{id}', 'InsumoController@show')->where('id', '[0-9]+');
    Route::POST('insumos', 'InsumoController@store');
    Route::PUT('insumos/update/{id}', "InsumoController@update");
    Route::DELETE('insumos/destroy/{id}', 'InsumoController@destroy')->where('id', '[0-9]+');
    Route::POST('insumos/import', 'ImportController@import')->name('import');

    //PROVEEDORES
    Route::GET('proveedores', 'ProveedorController@index');
    Route::POST('proveedores', 'ProveedorController@store');
    Route::GET('proveedores/{id}', 'ProveedorController@find')->where('id', '[0-9]+');
    Route::PUT('proveedores/update/{id}', "ProveedorController@update");
    Route::DELETE('proveedores/destroy/{id}', 'ProveedorController@destroy')->where('id', '[0-9]+');

    Route::POST('mail','MailController@notificacion');


    Route::GET('modelos','ModeloController@index');
    Route::POST('modelos','ModeloController@store');
    Route::PUT('modelos/update/{id}','ModeloController@update');
    Route::GET('modelos/{id}','ModeloController@show')->where('id','[0-9]+');
    Route::DELETE('modelos/destroy/{id}','ModeloController@destroy')->where('id','[0-9]+');


    Route::POST('destajos/quema','DestajoController@destajosByUbicacion');
    Route::POST('destajos/quema/subcontrato','DestajoController@destajosBySubcontrato');

    //Route::PUT('destajos/update/{id}','DestajoController@update');


    Route::get('familias', 'FamiliaController@index');
    Route::get('unidades', 'UnidadController@index');


    Route::post('entradas/create', 'EntradaController@create');
    Route::post('entradas/add-factura', 'EntradaController@addFactura');
    Route::get('entradas/factura/{id}', 'EntradaController@getFactura')->where('id', '[0-9]+');;
    Route::get('entradas/orden-compra/{ordenCompraId}', 'EntradaController@allByOrdenCompra')->where('ordenCompraId', '[0-9]+');
    Route::get('entradas/{id}', 'EntradaController@find')->where('id', '[0-9]+');
    Route::get('entradas/orden-compra/restante/{ordenCompraId}', 'EntradaController@insumosRestantes')->where('ordenCompraId', '[0-9]+');

    Route::POST('orden-salida/ubicaciones', 'OrdenSalidaController@ubicaciones');
    Route::POST('orden-salida/destajos', 'OrdenSalidaController@destajos');
    Route::POST('orden-salida/create', 'OrdenSalidaController@create');
    Route::GET('orden-salida/{id}', 'OrdenSalidaController@find')->where('id', '[0-9]+');;
    Route::GET('orden-salida/folio/{folio}', 'OrdenSalidaController@findByFolio');
    Route::GET('orden-salida/completed/{id}', 'OrdenSalidaController@completed')->where('id', '[0-9]+');
    Route::GET('orden-salida/cancel/{id}', 'OrdenSalidaController@cancel')->where('id', '[0-9]+');
    Route::GET('orden-salida/ready/{id}', 'OrdenSalidaController@ready')->where('id', '[0-9]+');
    Route::GET('orden-salida/processing/{id}', 'OrdenSalidaController@processing')->where('id', '[0-9]+');
    Route::get('orden-salida/fraccionamiento/{fraccionamiento_id}', 'OrdenSalidaController@findAllByFraccionamiento')->where('fraccionamiento_id', '[0-9]+');
    Route::POST('orden-salida/change-estatus', 'OrdenSalidaController@changeEstatus');
    Route::POST('orden-salida/ticket', 'OrdenSalidaController@ticketDestajoUbiacion');
    Route::post('orden-salida/reporte/ubicacion', 'OrdenSalidaController@insumosPorUbicaciones');

    Route::POST('quema/create','QuemaController@create');
    Route::POST('quema/create-subcontrato','QuemaController@createSubcontrato');
    Route::GET('quema/verificar/{fracc_id}','QuemaController@verificaciones')->where('fracc_id', '[0-9]+');
    Route::GET('quema/verificar-subcontrato/{fracc_id}','QuemaController@verificacionesSubcontratos')->where('fracc_id', '[0-9]+');
    Route::GET('quema/aprobar/{qDestajo_id}','QuemaController@aprobar')->where('qDestajo_id', '[0-9]+');
    Route::GET('quema/destroy/{quema_id}','QuemaController@destroy')->where('quema_id', '[0-9]+');

    Route::get('empleados', 'EmpleadoController@all');
    Route::get('empleados/{id}', 'EmpleadoController@find')->where('id', '[0-9]+');
    Route::post('empleados', 'EmpleadoController@create');
    Route::put('empleados/update', "EmpleadoController@update");
    Route::delete('empleados/destroy/{id}', 'EmpleadoController@destroy')->where('id', '[0-9]+');
    Route::get('empleados/puestos', 'EmpleadoController@puestos');

    Route::get('cuadrillas', 'CuadrillaController@all');
    Route::get('cuadrillas/{id}', 'CuadrillaController@find')->where('id', '[0-9]+');
    Route::post('cuadrillas', 'CuadrillaController@create');
    Route::put('cuadrillas/update/{id}', "CuadrillaController@update");
    Route::delete('cuadrillas/destroy/{id}', 'CuadrillaController@destroy')->where('id', '[0-9]+');
    Route::post('cuadrillas/import', "CuadrillaController@import");

    Route::get('fraccionamientos/etapas-modelos/{id}', 'FraccionamientoController@etapasModelos');
    Route::get('fraccionamientos', 'FraccionamientoController@all');
    Route::post('fraccionamientos/create', 'FraccionamientoController@create');
    Route::put('fraccionamientos/update', 'FraccionamientoController@update');
    Route::get('fraccionamientos/{id}', 'FraccionamientoController@find')->where('id', '[0-9]+')->middleware('fracc_rule:fraccionamiento');

    Route::post('etapas/create', 'EtapaController@create')->middleware('fracc_rule:etapa,create');
    Route::put('etapas/update', 'EtapaController@update');
    Route::get('etapas/{id}', 'EtapaController@find')->where('id', '[0-9]+')->middleware('fracc_rule:etapa,get');
    Route::delete('etapas/destroy/{id}', 'EtapaController@destroy')->where('id', '[0-9]+');

    Route::post('ubicacion/create', 'UbicacionController@create')->middleware('fracc_rule:ubicacion,create');;
    Route::put('ubicacion/update', 'UbicacionController@update');
    Route::get('ubicacion/{id}', 'UbicacionController@find')->where('id', '[0-9]+');
    Route::post('ubicacion/create-multiple', 'UbicacionController@createMultiple');
    Route::patch('ubicacion/destajos/agregar', 'UbicacionController@agregarDestajo');
    Route::delete('ubicacion/destajos/eliminar', 'UbicacionController@eliminarDestajo');
    Route::get('ubicacion/progreso/{id}', 'UbicacionController@progresoDeObra')->where('id', '[0-9]+');;
    Route::get('ubicacion/compras/{id}', 'UbicacionController@compras')->where('id', '[0-9]+');;
    Route::get('ubicacion/delete/{ubicacion_id}', 'UbicacionController@delete')->where('ubicacion_id', '[0-9]+');
    Route::post('ubicacion/avance/report', 'UbicacionController@avanceUbicaciones');
    Route::get('ubicacion/all/{fracc_id}', 'UbicacionController@ubicacionesByFracc')->where('fracc_id', '[0-9]+');


    Route::GET('nominas','NominaController@index');
    Route::POST('nominas','NominaController@create');
    Route::DELETE('nominas/destroy/{id}','NominaController@destroy')->where('id','[0-9]+');
    Route::GET('nominas/get-nomina/{fracc_id}','NominaController@nomina')->where('fracc_id','[0-9]+');
    Route::GET('nominas/{id}','NominaController@find')->where('id','[0-9]+');
    Route::POST('nominas/report','NominaController@report');
    //Reporte facturas
    Route::POST('facturas/ubicacion','FacturaUbicacionController@ubicacionReport')->where('ubicacion_id', '[0-9]+');
    Route::POST('facturas/fraccionamiento','FacturaFraccionamientoController@fraccionamientoReport')->where('ubicacion_id', '[0-9]+');
    Route::POST('reporte/destajoubicacion','ReporteDestajoUbicacionController@destajoUbicacionReport');

    Route::get('inventario/{fracc_id}','InventarioController@inventario')->where('fracc_id', '[0-9]+');
    Route::get('inventario/report/{fracc_id}','InventarioController@inventarioReport')->where('fracc_id', '[0-9]+');

    Route::POST('destajos/quema/costo','DestajoController@costoDestajoByUbicacion');

});

