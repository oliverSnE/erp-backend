<?php

use App\Models\User;


class UserFindTestCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function testFindOneSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');

        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/user/1');
        // check expected response code
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            "id" => $user->id
        ]);
    }

    public function testFindAllSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');
        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/users');
        // check expected response code
        $I->seeResponseCodeIs(200);
    }

    public function testFindRolesSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de roles');
        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/users/roles');
        // check expected response code
        $I->seeResponseCodeIs(200);
    }
}
