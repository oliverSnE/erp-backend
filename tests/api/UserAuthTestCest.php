<?php



class UserAuthTestCest
{
    public function _before(ApiTester $I)
    {
    }

    public function testLoginIsSuccess(ApiTester $I)
    {
        $I->wantToTest('login is success');

        // send credential data
        $I->sendPOST('api/login', ['email' => 'test@email.com', 'password' => 'secret']);

        // login success
        $I->seeResponseCodeIs(200);
    }

    public function testLoginIsFailed(ApiTester $I)
    {
        $I->wantToTest('login is failed');


        // send invalid credential data
        $I->sendPOST('api/login', ['email' => 'test@test.com', 'password' => 'rahasia12311']);

        // check expected response code
        $I->seeResponseCodeIs(401);
    }

    public function testUserLogOutSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de Log Out');

        // create user data
        $user = App\Models\User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendGET('api/logout');

        // check expected response code
        $I->seeResponseCodeIs(200);
    }

    public function testUserLogOutFail(ApiTester $I)
    {
        $I->wantToTest('Prueba de fallo Log Out');

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated("tokenNoValido");

        // send request
        $I->sendGET('api/logout');

        // check expected response code
        $I->seeResponseCodeIs(401);
    }

    public function testTokenRefreshSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de refrescar token');

        $user = App\Models\User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendGET('api/refresh');

        // check expected response code
        $I->seeResponseCodeIs(200);
    }

    public function testTokenRefreshFail(ApiTester $I)
    {
        $I->wantToTest('Prueba de refrescar token');

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated("jahsidjsnajlsd");

        // send request
        $I->sendGET('api/refresh');

        // check expected response code
        $I->seeResponseCodeIs(401);
    }


    public function authenticatedUserSuccessFetchProfile(ApiTester $I)
    {
        $I->wantToTest('authenticated user success fetch profile');

        // create user data
        $user = App\Models\User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendGET('api/user');

        // check expected response code
        $I->seeResponseCodeIs(200);

        // check if response data is same with our init user data
        $I->seeResponseContainsJson(['email' => $user->email]);
    }
}
