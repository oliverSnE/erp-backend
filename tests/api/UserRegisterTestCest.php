<?php

use App\Models\User;

class UserRegisterTestCest
{
    public function _before(ApiTester $I)
    {
    }

    public function testRegisterSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de crear usuario');

        $userTest =  new User();
        $userTest->name = "testRegisterSuccess";
        $userTest->email = "testRegisterSuccess@testRegisterSuccess.com";
        $userTest->password = 'testRegisterSuccess';
        $userTest->rol = 'admin';

        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendPOST('api/register',
            [
                'name'                  => $userTest->name,
                'email'                 => $userTest->email,
                'password'              => $userTest->password,
                'password_confirmation' => $userTest->password,
                'rol'                   => $userTest->rol
            ]
        );
        // check expected response code
        $userFind = User::whereEmail($userTest->email)->first();
        $I->seeResponseCodeIs(201);
        $I->assertTrue(User::destroy($userFind->id) === 1);
    }

    public function testRegisterDuplicateEmail(ApiTester $I)
    {
        $I->wantToTest('Prueba de crear usuario');
        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendPOST('api/register',
            [
                'name'                  => $user->name,
                'email'                 => $user->email,
                'password'              => $user->password,
                'password_confirmation' => $user->password,
                'rol'                   => $user->rol
            ]
        );
        // check expected response code
        $I->seeResponseCodeIs(422);
    }

}
