<?php

use App\Models\User;
use Faker\Factory;

class UserUpdateTestCest
{
    public function _before(ApiTester $I)
    {
    }

    public function testUpdateSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');

        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);
        $faker = Factory::create();
        $name = $faker->name;
        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendPUT('api/update',
            [
                'id' => $user->id,
                'name' => $name,
                'email' => $user->email,
                'password' => "secret",
                'password_confirmation' => "secret"
            ]
        );
        // check expected response code
        $userFind = User::whereEmail($user->email)->first();
        $I->seeResponseCodeIs(201);
        $I->assertTrue($userFind->name === $name);
    }

    public function testUpdateFail(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');

        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);
        $faker = Factory::create();
        $email = $faker->name;
        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendPUT('api/update',
            [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $email,
                'password' => "secret",
                'password_confirmation' =>  "secret"
            ]
        );
        // check expected response code
        $I->seeResponseCodeIs(422);
    }

    public function testUpdateUniqueEmailFail(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');

        $user = User::find(1);
        $user2 = User::find(2);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);
        $faker = Factory::create();
        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);

        // send request
        $I->sendPUT('api/update',
            [
                'id' => $user->id,
                'name' => $user->name,
                'email' =>  $user2->email,
                'password' => "secret",
                'password_confirmation' =>  "secret"
            ]
        );
        // check expected response code
        $I->seeResponseCodeIs(422);
    }


}
