<?php

use App\Models\Empleado;
use App\Models\User;


class EmpleadosTestCest
{
    public function _before(ApiTester $I)
    {
    }

    public function testFindOneSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');

        $user = User::first();
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $empleado = Empleado::first();

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/empleados/'.$empleado->id);
        // check expected response code
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'data' =>
            [
                'empleado' => [
                    "id" => $empleado->id
                ]
            ]
        ]);
    }

    public function testFindAllSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de actualizar usuario');
        $user = User::first();
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/empleados');
        // check expected response code
        $I->seeResponseCodeIs(200);
    }

    public function testFindPuestosSuccess(ApiTester $I)
    {
        $I->wantToTest('Prueba de roles');
        $user = User::find(1);
        // create valid token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // set header token Authorization: Bearer {token}
        $I->amBearerAuthenticated($token);
        // send request
        $I->sendGET('api/empleados/puestos');
        // check expected response code
        $I->seeResponseCodeIs(200);
    }
}
