<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EntradaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\EntradaRepositoryInterface',
            'App\Repositories\EntradaRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
