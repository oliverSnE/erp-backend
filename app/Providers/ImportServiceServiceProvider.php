<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ImportServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Service\ImportServiceInterface',
            'App\Service\ImportService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
