<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CuadrillaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\CuadrillaServiceInterface',
            'App\Services\CuadrillaService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
