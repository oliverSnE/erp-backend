<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProveedoresRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\ProveedoresRepositoryInterface',
            'App\Repositories\ProveedoresRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
