<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DestajoRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\DestajoRepositoryInterface',
            'App\Repositories\DestajoRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
