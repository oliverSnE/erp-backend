<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FamiliaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\FamiliaServiceInterface',
            'App\Services\FamiliaService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
