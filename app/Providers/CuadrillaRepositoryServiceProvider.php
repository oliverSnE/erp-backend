<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CuadrillaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\CuadrillaRepositoryInterface',
            'App\Repositories\CuadrillaRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
