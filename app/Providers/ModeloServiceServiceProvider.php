<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ModeloServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\ModeloServiceInterface',
            'App\Services\ModeloService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
