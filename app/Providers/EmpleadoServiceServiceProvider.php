<?php

namespace App\Providers;

use App\Service\EmpleadoService;
use App\Service\EmpleadoServiceInterface;
use Illuminate\Support\ServiceProvider;

class EmpleadoServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Service\EmpleadoServiceInterface',
            'App\Service\EmpleadoService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
