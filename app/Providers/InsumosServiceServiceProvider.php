<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InsumosServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\InsumosServiceInterface',
            'App\Services\InsumosService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
