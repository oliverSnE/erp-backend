<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UnidadServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\UnidadServiceInterface',
            'App\Services\UnidadService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
