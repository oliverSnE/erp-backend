<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrdenSalidaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\OrdenSalidaServiceInterface',
            'App\Services\OrdenSalidaService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
