<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InsumosRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\InsumosRepositoryInterface',
            'App\Repositories\InsumosRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
