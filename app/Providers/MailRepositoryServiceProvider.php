<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MailRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\MailRepositoryInterface',
            'App\Repositories\MailRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
