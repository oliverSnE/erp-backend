<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ImportRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\ImportRepositoryInterface',
            'App\Repositories\ImportRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
