<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrdenSalidaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\OrdenSalidaRepositoryInterface',
            'App\Repositories\OrdenSalidaRepository'
        );
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
