<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EtapaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\EtapaRepositoryInterface',
            'App\Repositories\EtapaRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
