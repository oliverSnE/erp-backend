<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProveedoresServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\ProveedorServiceInterface',
            'App\Services\ProveedorService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
