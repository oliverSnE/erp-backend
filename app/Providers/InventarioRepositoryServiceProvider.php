<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InventarioRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\InventarioRepositoryInterface',
            'App\Repositories\InventarioRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
