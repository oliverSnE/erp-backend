<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UbicacionServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Services\UbicacionServiceInterface',
            'App\Services\UbicacionService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
