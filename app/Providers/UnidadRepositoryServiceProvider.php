<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UnidadRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UnidadRepositoryInterface',
            'App\Repositories\UnidadRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
