<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrdenCompraServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\OrdenCompraServiceInterface',
            'App\Services\OrdenCompraService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
