<?php

namespace App\Providers;

use App\Services\EntradaService;
use Illuminate\Support\ServiceProvider;

class EntradaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\EntradaServiceInterface',
            'App\Services\EntradaService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
