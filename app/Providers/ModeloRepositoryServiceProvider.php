<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ModeloRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\ModeloRepositoryInterface',
            'App\Repositories\ModeloRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
