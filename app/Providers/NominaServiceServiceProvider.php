<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NominaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\NominaServiceInterface',
            'App\Services\NominaService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
