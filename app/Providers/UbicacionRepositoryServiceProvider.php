<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UbicacionRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UbicacionRepositoryInterface',
            'App\Repositories\UbicacionRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
