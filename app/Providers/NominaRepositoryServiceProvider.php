<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NominaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\NominaRepositoryInterface',
            'App\Repositories\NominaRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
