<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrdenCompraRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\OrdenCompraRepositoryInterface',
            'App\Repositories\OrdenCompraRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
