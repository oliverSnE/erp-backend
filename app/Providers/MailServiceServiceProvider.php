<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MailServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\MailServiceInterface',
            'App\Services\MailService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
