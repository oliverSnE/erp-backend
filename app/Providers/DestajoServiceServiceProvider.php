<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DestajoServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\DestajoServiceInterface',
            'App\Services\DestajoService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
