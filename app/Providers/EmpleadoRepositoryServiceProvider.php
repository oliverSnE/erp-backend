<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EmpleadoRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\EmpleadoRepositoryInterface',
            'App\Repositories\EmpleadoRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
