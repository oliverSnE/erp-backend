<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FraccionamientoServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Services\FraccionamientoServiceInterface',
            'App\Services\FraccionamientoService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
