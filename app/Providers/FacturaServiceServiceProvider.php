<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FacturaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\FacturaUbicacionServiceInterface',
            'App\Services\FacturaUbicacionService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
