<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FamiliaRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\FamiliaRepositoryInterface',
            'App\Repositories\FamiliaRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
