<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FacturaFraccionamientoServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\FacturaFraccionamientoServiceInterface',
            'App\Services\FacturaFraccionamientoService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
