<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InventarioServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\InventarioServiceInterface',
            'App\Services\InventarioService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
