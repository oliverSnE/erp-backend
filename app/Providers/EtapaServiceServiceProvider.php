<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EtapaServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Services\EtapaServiceInterface',
            'App\Services\EtapaService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
