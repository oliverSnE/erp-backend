<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ReporteDestajoUbicacionServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\ReporteDestajoUbicacionServiceInterface',
            'App\Services\ReporteDestajoUbicacionService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
