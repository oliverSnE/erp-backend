<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FraccionamientoRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\FraccionamientoRepositoryInterface',
            'App\Repositories\FraccionamientoRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
