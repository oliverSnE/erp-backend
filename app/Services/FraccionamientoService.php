<?php

namespace App\Services;

use App\Http\Requests\FraccionamientoRequest;
use App\Repositories\FraccionamientoRepositoryInterface;

class FraccionamientoService implements FraccionamientoServiceInterface
{
    protected $fraccionamientoRepository;

    public function __construct(FraccionamientoRepositoryInterface $fraccionamientoRepository)
    {
        $this->fraccionamientoRepository = $fraccionamientoRepository;
    }

    public function all()
    {
        $fraccionamientos = $this->fraccionamientoRepository->all();

        return response()->json(['data' =>compact('fraccionamientos')],201);
    }

    public function create(FraccionamientoRequest $request)
    {
        $fraccionamientos = $this->fraccionamientoRepository->create($request);

        return response()->json(['data' =>compact('fraccionamientos')],201);
    }

    public function update(FraccionamientoRequest $request)
    {
        $fraccionamiento = $this->fraccionamientoRepository->update($request);

        return response()->json(['data' =>compact('fraccionamiento')],201);
    }

    public function find(int $id)
    {
        $fraccionamiento = $this->fraccionamientoRepository->find($id);

        return response()->json(['data' =>compact('fraccionamiento')],201);
    }

    public function etapasModelos(int $id)
    {
        $fraccionamiento = $this->fraccionamientoRepository->getEtapasModelos($id);

        return response()->json(['data' =>compact('fraccionamiento')],201);

    }

}
