<?php

namespace App\Services;

use App\Http\Requests\EtapaRequest;

interface EtapaServiceInterface
{
    public function create(EtapaRequest $request);

    public function update(EtapaRequest $request);

    public function find(int $id);

    public function destroy(int $id);
}
