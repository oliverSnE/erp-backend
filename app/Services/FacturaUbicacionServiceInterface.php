<?php
namespace App\Services;

use App\Http\Requests\FacturaUbicacionRequest;

interface FacturaUbicacionServiceInterface {

    public function ubicacionReport(FacturaUbicacionRequest $ubicacion_id);
}
