<?php

namespace App\Services;

use App\Http\Requests\OrdenCompraAddFacturaRequest;
use App\Http\Requests\OrdenCompraCreateInsumosRequest;
use App\Http\Requests\OrdenCompraCreateRequest;
use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\OrdenCompraReOrdenRequest;
use App\Http\Requests\OrdenCompraSubcontratosInsumosRequest;
use App\Http\Requests\OrdenCompraUpdateRequest;
use App\Http\Requests\UbicacionByModeloEtapaRequest;
use Illuminate\Http\Request;

interface OrdenCompraServiceInterface
{
    public function create(OrdenCompraCreateRequest $request);

    public function createOnlyInsumos(OrdenCompraCreateInsumosRequest $request);

    public function updateDetalle(OrdenCompraUpdateRequest $request);

    public function aprove(int $id);

    public function cancel(int $id);

    public function completed($id);

    public function find(int $id);

    public function findByFolio(String $folio);

    public function findAll();

    public function findAllByFraccionamiento(int $fraccionamientoId);

    public function pdf($id);

    public function ubicacion(UbicacionByModeloEtapaRequest $request);

    public function destajos(OrdenCompraDestajosRequest $request);

    public function subcontratos(OrdenCompraDestajosRequest $request);

    public function subcontratosInsumos(OrdenCompraSubcontratosInsumosRequest $request);

    public function verificacion(int $id);

    public function addFactura(OrdenCompraAddFacturaRequest $request);

    public function getSpecialOrders(int $model_id);

    public function facturas(int $orden_id);

    public function downloadFactura(int $factura_id);

    public function reOrden(OrdenCompraReOrdenRequest $request);
}
