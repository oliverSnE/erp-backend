<?php

namespace App\Services;

use App\Http\Requests\ProveedorRequest;
use App\Repositories\ProveedoresRepositoryInterface;

class ProveedorService implements ProveedorServiceInterface
{
    protected $proveedores;

    public function __construct(ProveedoresRepositoryInterface $proveedores)
    {
        $this->proveedores = $proveedores;
    }
    public function index()
    {
        $proveedores = $this->proveedores->index();
        return response()->json(['data' => compact('proveedores')], 200);
    }
    public function find($id)
    {
        $proveedor = $this->proveedores->find($id);
         return response()->json(['data' => compact('proveedor')],200);
    }
    public function store(ProveedorRequest $request)
    {
       $proveedores = $this->proveedores->store($request);
       return response()->json(['data' => compact('proveedores')],201);
    }
    public function update(ProveedorRequest $request)
    {
        $proveedores = $this->proveedores->update($request);
       return response()->json(['data' => compact('proveedores')], 201);
    }
    public function destroy($id)
    {
        $this->proveedores->destroy($id);
        return response()->json(['data' => [
            'code' => 200,
            'message' => 'Eliminado correctamente'
        ]], 200);
    }

}

