<?php

namespace App\Services;

use App\Http\Requests\ModeloImportRequest;
use App\Http\Requests\ModeloRequest;
use App\Repositories\ModeloRepositoryInterface;

class ModeloService implements ModeloServiceInterface
{
    protected $modelos;

    public function __construct(ModeloRepositoryInterface $modelo)
	{
		$this->modelos = $modelo;
    }
    public function index()
    {
        $modelos = $this->modelos->index();
        return response()->json(['data' => compact('modelos')], 200);
    }
    public function show($id)
    {
        $modelos = $this->modelos->show($id);
        return response()->json(['data' => compact('modelos')],200);
    }
    public function store(ModeloRequest $request)
    {
       $modelos = $this->modelos->store($request);
       return response()->json(['data' => compact('modelos')],201);
    }
    public function update(ModeloRequest $request)
    {
        $modelos = $this->modelos->update($request);
       return response()->json(['data' => compact('modelos')], 201);
    }
    public function destroy($id)
    {
        $modelos = $this->modelos->destroy($id);
        return response()->json(['data' => [
            'code' => 200,
            'message' => 'Eliminado correctamente'
        ]], 200);
    }

    public function import(ModeloImportRequest $request){
        $modelo = $this->modelos->import($request);
        return response()->json(['data' => compact('modelo')], 201);
    }

}
