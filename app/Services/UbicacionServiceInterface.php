<?php

namespace App\Services;

use App\Http\Requests\AvanceUbicacionReportRequest;
use App\Http\Requests\UbicacionDestajosRequest;
use App\Http\Requests\UbicacionEmpleadoDestajoRequest;
use App\Http\Requests\UbicacionRequest;
use App\Http\Requests\UbicacionMultipleRequest;
use Illuminate\Http\Request;

interface UbicacionServiceInterface
{
    public function create(UbicacionRequest $request);

    public function createMultiple(UbicacionMultipleRequest $request);

    public function update(UbicacionRequest $request);

    public function find(int $id);

    public function agregarDestajo(UbicacionDestajosRequest $request);

    public function eliminarDestajo(UbicacionDestajosRequest $request);

    public function progresoDeObra(int $ubicacion_id);

    public function compras(int $ubicacion_id);

    public function delete(int $ubicacion_id);

    public function avanceUbicaciones(AvanceUbicacionReportRequest $request);

    public function ubicacionesByFracc(int $fracc_id);

    public function empeladosDestajo(UbicacionEmpleadoDestajoRequest $request);
}
