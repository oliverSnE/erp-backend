<?php

namespace App\Services;

use App\Http\Requests\FraccionamientoRequest;

interface FraccionamientoServiceInterface
{
    public function all();

    public function create(FraccionamientoRequest $request);

    public function update(FraccionamientoRequest $request);

    public function find(int $id);

    public function etapasModelos(int $id);
}
