<?php

namespace App\Services;

use App\Http\Requests\InsumoRegisterRequest;
use App\Repositories\InsumosRepositoryInterface;
use Illuminate\Http\Request;

interface InsumosServiceInterface
{
    public function index();
    public function show($id);
    public function store(InsumoRegisterRequest $request);
    public function update(InsumoRegisterRequest $request);
    public function destroy($id);
}
