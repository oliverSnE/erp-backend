<?php
namespace App\Services;

use App\Exports\DestajoPorUbicacionExport;
use App\Http\Requests\AvanceUbicacionReportRequest;
use Maatwebsite\Excel\Facades\Excel;

class ReporteDestajoUbicacionService implements ReporteDestajoUbicacionServiceInterface {

    public function __construct() {

    }

    public function destajoUbicacionReport(AvanceUbicacionReportRequest $request)
    {
        $ubicaciones =  $request->get('ubicaciones');
        $fracc_id = $request->get('fraccionamiento_id');;
        $ldate = date('Y-m-d');
        return Excel::download(new DestajoPorUbicacionExport($ubicaciones, $fracc_id), 'Destajos por ubicacion '.$ldate.'.xlsx');
    }
}
