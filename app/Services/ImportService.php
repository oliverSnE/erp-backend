<?php
namespace App\Services;

class ImportService implements ImportServiceInterface {
    protected $import;

    public function __construct(ImportRepositoryInterface $import) {
        $this->import = $import;
    }

    public function import(Request $request) {
        $imp = $this->$import->import($request);
        return response()->json(['data' => compact('imp')], 200);
    }

    public function onError(\Throwable $e) {
        $imp = $this->$import->onError($e);
        return response()->json(['data' => compact('imp')], 200);
    }

}