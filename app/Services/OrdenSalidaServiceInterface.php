<?php
namespace App\Services;

use App\Http\Requests\InsumosUbicacionReportRequest;
use App\Http\Requests\OrdenSalidaDestajosEstatusRequest;
use App\Http\Requests\OrdenSalidaRequest;
use Illuminate\Http\Request;

interface OrdenSalidaServiceInterface
{
    public function ubicaciones(Request $request);
    public function destajos(Request $request);
    public function create(OrdenSalidaRequest $request);
    public function find(int $id);
    public function findByFolio(string $folio);
    public function findAllByFraccionamiento(int $fraccionamientoId);
    public function processing(int $id);
    public function ready(int $id);
    public function cancel(int $id);
    public function completed(int $id);
    public function changeEstatus(OrdenSalidaDestajosEstatusRequest $request);
    public function ticketDestajoUbiacion(OrdenSalidaDestajosEstatusRequest $request);
    public function insumosPorUbicaciones(InsumosUbicacionReportRequest $request);
}
