<?php

namespace App\Services;

use App\Http\Requests\OrdenCompraAddFacturaRequest;
use App\Http\Requests\OrdenCompraCreateInsumosRequest;
use App\Http\Requests\OrdenCompraCreateRequest;
use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\OrdenCompraReOrdenRequest;
use App\Http\Requests\OrdenCompraSubcontratosInsumosRequest;
use App\Http\Requests\OrdenCompraUpdateRequest;
use App\Http\Requests\UbicacionByModeloEtapaRequest;
use App\Models\Fraccionamiento;
use App\Models\User;
use App\Repositories\OrdenCompraRepositoryInterface;
use Illuminate\Http\Request;
use JWTAuth;
use PDF;

class OrdenCompraService implements OrdenCompraServiceInterface
{
    protected $ordenRepository;

    public function __construct(OrdenCompraRepositoryInterface $ordenRepository)
    {
        $this->ordenRepository = $ordenRepository;
    }

    public function create(OrdenCompraCreateRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compras =  $this->ordenRepository->create($request, $user);
        return response()->json(['data' => compact('orden_compras')], 200);
    }

    public function createOnlyInsumos(OrdenCompraCreateInsumosRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compras =  $this->ordenRepository->createOnlyInsumos($request, $user);
        return response()->json(['data' => compact('orden_compras')], 200);
    }

    public function updateDetalle(OrdenCompraUpdateRequest $request)
    {
        $orden_compras =  $this->ordenRepository->updateDetalle($request);
        return response()->json(['data' => compact('orden_compras')], 200);
    }

    public function aprove($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compra = $this->ordenRepository->aprove($id, $user);
        return response()->json(['data' => compact('orden_compra')], 200);
    }

    public function cancel($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compra = $this->ordenRepository->cancel($id, $user);
        return response()->json(['data' => compact('orden_compra')], 200);
    }

    public function completed($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compra = $this->ordenRepository->completed($id, $user);
        return response()->json(['data' => compact('orden_compra')], 200);
    }

    public function find($id)
    {
        $orden_compra = $this->ordenRepository->find($id);
        return response()->json(['data' => compact('orden_compra')], 200);
    }

    public function findByFolio(String $folio)
    {
        $orden_compra = $this->ordenRepository->findByFolio($folio);
        return response()->json(['data' => compact('orden_compra')], 200);
    }

    public function findAll()
    {
        $ordenes_compras = $this->ordenRepository->findAll();
        return response()->json(['data' => compact('ordenes_compras')], 200);
    }

    public function findAllByFraccionamiento(int $fraccionamientoId)
    {
        $ordenes_compras= $this->ordenRepository->findAllByFraccionamiento($fraccionamientoId);;
        return response()->json(['data' => compact('ordenes_compras')], 200);
    }

    public function ubicacion(UbicacionByModeloEtapaRequest $request)
    {
        $ubicacion = $this->ordenRepository->ubicacion($request);
        return response()->json(['data' => compact('ubicacion')], 200);
    }

    public function destajos(OrdenCompraDestajosRequest $request)
    {
        $destajos = $this->ordenRepository->destajos($request);
        return response()->json(['data' => compact('destajos')], 200);
    }

    public function subcontratos(OrdenCompraDestajosRequest $request)
    {
        $destajos = $this->ordenRepository->subcontratos($request);
        return response()->json(['data' => compact('destajos')], 200);
    }

    public function subcontratosInsumos(OrdenCompraSubcontratosInsumosRequest $request)
    {
        $insumos = $this->ordenRepository->subcontratosInsumos($request);
        return response()->json(['data' => compact('insumos')], 200);
    }


    public function verificacion(int $id)
    {
        $verificacion = $this->ordenRepository->verificacion($id);
        return response()->json(['data' => compact('verificacion')], 200);
    }

    public function pdf($id)
    {
        $orden = $this->ordenRepository->find($id);
        $user = null;
        if($orden->data !== null && array_key_exists("estatus_aprobada", $orden->data)){
            $user = User::where('id', $orden->data["estatus_aprobada"]["user_id"])->first();
        }
        $fraccionamiento = Fraccionamiento::where('id', $orden->fraccionamiento_id)->first();
        $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'])->loadView('pdf.orden-compra', [
            "orden" => $orden,
            "fraccionamiento" => $fraccionamiento,
            "usuario_aprobo"  => $user,
            "etapa"           => $orden->ubicaciones[0] ? $orden->ubicaciones[0]->etapa : collect(['nombre' => ' '])
        ]);
        return $pdf->download('Orden de Compra '.$orden->folio.' .pdf');
    }

    public function addFactura(OrdenCompraAddFacturaRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $factura = $this->ordenRepository->addFactura($request,  $user);
        return response()->json(['data' => compact('factura')], 200);
    }

    public function getSpecialOrders(int $model_id)
    {
        $ordenes = $this->ordenRepository->getSpecialOrders($model_id);
        return response()->json(['data' => compact('ordenes')], 200);
    }

    public function facturas(int $orden_id)
    {
        $facturas = $this->ordenRepository->getFacturas($orden_id);
        return response()->json(['data' => compact('facturas')], 200);
    }

    public function downloadFactura(int $factura_id)
    {
        $factura = $this->ordenRepository->getFactura($factura_id);

        return response()->download(storage_path('app/'.$factura->factura), $factura->folio.'pdf');
    }

    public function reOrden(OrdenCompraReOrdenRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_compras = $this->ordenRepository->reOrden($request,  $user);
        return response()->json(['data' => compact('orden_compras')], 200);
    }

}
