<?php

namespace App\Service;

use App\Http\Requests\EmpleadoRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface EmpleadoServiceInterface
{
    /**
     * Funcion para crear un nuevo e,pleado.
     * Regresa el empleado creado.
     *
     * @param EmpleadoRequest $request
     * @return JsonResponse
     */
    public function create(EmpleadoRequest $request): JsonResponse;

    /**
     * Funcion para actualizar la informacion de un empleado.
     * Regresa el empleado actualizado.
     *
     * @param EmpleadoRequest $request
     * @return JsonResponse
     */
    public function update(EmpleadoRequest $request): JsonResponse;

    /**
     * Funcion para obtener un empleado en particular por medio de un id.
     * Regresa la informacion del empleado al que corresponde el id.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function findById(int $id): JsonResponse;

    /**
     * Funcion para obtener todos los empleados de la base de datos.
     * Regresa un listado con todos los empleados de la base de datos
     *
     * @return JsonResponse
     */
    public function all(): JsonResponse;

    /**
     * Funcion para eliminar un empleado de la base de datos.
     * Regresa un estatus de success si el empleado fue eliminado.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse;

    /**
     * Funcion que regresa una lista de puestos posibles
     *
     * @return JsonResponse
     */
    public function puestos(): JsonResponse;
}
