<?php

namespace App\Services;

use App\Http\Requests\CostoDestajoRequest;
use App\Http\Requests\DestajoRequest;
use App\Http\Requests\DetalleDestajoRequest;
use App\Repositories\DestajoRepositoryInterface;
use Illuminate\Http\Request;
use JWTAuth;

class DestajoService implements DestajoServiceInterface
{
    protected $destajos;

    public function __construct(DestajoRepositoryInterface $destajo)
	{
		$this->destajos = $destajo;
    }
    public function index()
    {
        $destajos = $this->destajos->index();
        return response()->json(['data' => compact('destajos')], 200);
    }
    public function show($id)
    {
        $destajo = $this->destajos->show($id);
        return response()->json(['data' => compact('destajo')],200);
    }
    public function getDetalleById($detalle_id) {
        $destajo = $this->destajos->getDetalleById($detalle_id);
        return response()->json(['data' => compact('destajo')], 200);
    }
    public function create(DestajoRequest $request)
    {
       $destajo = $this->destajos->create($request);
       return response()->json(['data' => compact('destajo')],201);
    }
    // public function update(DestajoRequest $request)
    // {
    //     $destajo = $this->destajos->update($request);
    //     return response()->json(['data' => compact('destajo')], 201);
    // }

    public function editDetalle(DetalleDestajoRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $destajo = $this->destajos->editDetalle($request, $user);
        return response()->json(['data' => compact('destajo')], 201);
    }
    public function destroy($id)
    {
        $this->destajos->destroy($id);
        return response()->json(['data' => [
            'code' => 200,
            'message' => 'Eliminado correctamente'
        ]], 200);
    }
    public function getTipos(){
        $tipos = $this->destajos->getTipos();
        return response()->json(['data' => compact('tipos')], 200);
    }

    public function destajosByUbicacion(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $destajos = $this->destajos->destajosByUbicacion($request, $user);
        return response()->json(['data' => compact('destajos')], 200);
    }
    public function destajosBySubcontrato(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $destajos = $this->destajos->destajosBySubcontrato($request, $user);
        return response()->json(['data' => compact('destajos')], 200);
    }

    public function costoDestajoByUbicacion(CostoDestajoRequest $request)
    {
        $costo = $this->destajos->costoDestajoByUbicacion($request);
        return response()->json(['data' => compact('costo')], 200);
    }
    public function searchByName(Request $request){
        $destajo = $this->destajos->searchByName($request);
        return response()->json(['data' => compact('destajo')], 200);
    }



}
