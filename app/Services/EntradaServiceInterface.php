<?php

namespace App\Services;

use Illuminate\Http\Request;

interface EntradaServiceInterface {
    public function allByOrdenCompra(int $ordenCompraId);

    public function find(int $id);

    public function create(Request $request);

    public function addFactura(Request $request);

    public function getFactura(int $id);

    public function insumosRestantes(int $orden_compra_id);
}
