<?php
namespace App\Services;

use App\Exports\InsumosPorUbicacionExport;
use App\Http\Requests\InsumosUbicacionReportRequest;
use App\Http\Requests\OrdenSalidaDestajosEstatusRequest;
use App\Http\Requests\OrdenSalidaRequest;
use App\Models\Fraccionamiento;
use App\Repositories\OrdenSalidaRepositoryInterface;
use PDF;
use JWTAuth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OrdenSalidaService implements OrdenSalidaServiceInterface
{
    protected $ordenRepository;


    public function __construct(OrdenSalidaRepositoryInterface $ordenRepository)
    {
        $this->ordenRepository = $ordenRepository;
    }

    public function create(OrdenSalidaRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orden_salida = $this->ordenRepository->create($request, $user);
        return response()->json(['data' => compact('orden_salida')], 200);
    }
    public function destajos(Request $request)
    {

        $destajos = $this->ordenRepository->destajos($request);
        return response()->json(['data' => compact('destajos')], 200);

    }
    public function ubicaciones(Request $request)
    {
        $ubicaciones = $this->ordenRepository->ubicaciones($request);
        return response()->json(['data' => compact('ubicaciones')], 200);

    }
    public function findAllByFraccionamiento(int $id)
    {
        $ordenes = $this->ordenRepository->findAllByFraccionamiento($id);
        return response()->json(['data' => compact('ordenes')], 200);

    }
    public function find(int $id)
    {
        $orden = $this->ordenRepository->find($id);
        return response()->json(['data' => compact('orden')], 200);

    }
    public function findByFolio(string $folio)
    {
        $orden = $this->ordenRepository->findByFolio($folio);
        return response()->json(['data' => compact('orden')], 200);

    }
    public function completed($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $this->ordenRepository->completed($id, $user);
    }

    public function cancel($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $this->ordenRepository->cancel($id, $user);
    }

    public function ready($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $this->ordenRepository->ready($id, $user);
    }

    public function processing($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $this->ordenRepository->processing($id, $user);
    }

    public function changeEstatus(OrdenSalidaDestajosEstatusRequest $request)
    {
        return $this->ordenRepository->changeEstatus($request);
    }

    public function ticketDestajoUbiacion(OrdenSalidaDestajosEstatusRequest $request)
    {
        $orden = $this->ordenRepository->find((int) $request->get('orden_salida_id'));
        $detalle =$this->ordenRepository->getDestajoUbicacion((int) $request->get('destajo_id'), (int)$request->get('ubicacion_id'),(int)$request->get('orden_salida_id'));

        $fraccionamiento = Fraccionamiento::where('id', $orden["fraccionamiento_id"])->first();
        $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'])->loadView('pdf.ticket-orden-salida', [
            "orden" => $orden,
            "detalles" =>  $detalle,
            "fraccionamiento" => $fraccionamiento
        ]);
        return $pdf->download('Orden de salida '.$orden["folio"].' .pdf');
    }

    public function insumosPorUbicaciones(InsumosUbicacionReportRequest $request)
    {
        $ldate = date('Y-m-d');
        return Excel::download(
            new InsumosPorUbicacionExport(
                    $request->get('ubicaciones'),
                    $request->get('fecha'),
                    $request->get('fraccionamiento_id')
                ), 'Materiales por ubicacion '.$ldate.'.xlsx');
    }

}
