<?php
namespace App\Services;

use App\Http\Requests\AvanceUbicacionReportRequest;

interface ReporteDestajoUbicacionServiceInterface {

    public function destajoUbicacionReport(AvanceUbicacionReportRequest $request);
}
