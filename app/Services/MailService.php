<?php

namespace App\Services;

use App\Http\Requests\MailRequest;
use App\Repositories\MailRepositoryInterface;


class MailService implements MailServiceInterface
{
    protected $mail;
    public function __construct(MailRepositoryInterface $mail)
	{
		$this->mail = $mail;
    }
    public function notificacion(MailRequest $request)
    {
        $this->mail->notificacion($request);
        return response()->json(['data' => [
            'status' => 200,
            'message' => 'Mensaje enviado correctamente'
        ]], 200);
    }
}
