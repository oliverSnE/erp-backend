<?php
namespace App\Services;

use App\Exports\FacturaFraccionamientoExport;
use App\Http\Requests\FacturaFraccionamientoRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FacturaFraccionamientoService implements FacturaFraccionamientoServiceInterface {

    public function __construct() {

    }

    public function fraccionamientoReport(FacturaFraccionamientoRequest $request)
    {
        $fraccionamiento = $request['fraccionamiento_id'];
        $ldate = date('Y-m-d');
        return Excel::download(new FacturaFraccionamientoExport($fraccionamiento), 'Ordenes de compra(facturas por ubicacion) '.$ldate.'.xlsx');
    }

}
