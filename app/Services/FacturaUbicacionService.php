<?php
namespace App\Services;

use App\Exports\FacturaUbicacionExport;
use App\Http\Requests\FacturaUbicacionRequest;
use Maatwebsite\Excel\Facades\Excel;

class FacturaUbicacionService implements FacturaUbicacionServiceInterface {

    public function __construct() {

    }

    public function ubicacionReport(FacturaUbicacionRequest $request)
    {
        $ubicaciones = $request['ubicaciones'];
        $ldate = date('Y-m-d');
        return Excel::download(new FacturaUbicacionExport($ubicaciones), 'Factura por ubicacion '.$ldate.'.xlsx');
    }
}
