<?php
namespace App\Services;

use App\Exports\InventarioExport;
use App\Repositories\ImportRepositoryInterface;
use App\Repositories\InventarioRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InventarioService implements InventarioServiceInterface {
    protected $inventario;

    public function __construct(InventarioRepositoryInterface $inventario) {
        $this->inventario = $inventario;
    }

    public function inventario($fracc_id)
    {
        $insumos = $this->inventario->getInventarioActual($fracc_id);
        return response()->json(['data' => compact("insumos")], 200);
    }

    public function inventarioReport($fracc_id)
    {
        $ldate = date('Y-m-d');
        return Excel::download(new InventarioExport($fracc_id), 'Inventario '.$ldate.'.xlsx');
    }

}
