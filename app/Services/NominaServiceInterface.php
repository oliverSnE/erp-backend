<?php

namespace App\Services;

use App\Http\Requests\NominaReportRequest;
use App\Http\Requests\NominaRequest;

interface NominaServiceInterface
{
    public function index();
    public function find($id);
    public function create(NominaRequest $request);
    public function destroy($id);
    public function nomina(int $fracc_id);
    public function report(NominaReportRequest $request);
}
