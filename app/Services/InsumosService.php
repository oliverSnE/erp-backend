<?php

namespace App\Services;

use App\Http\Requests\InsumoRegisterRequest;
use App\Repositories\InsumosRepositoryInterface;
use Illuminate\Http\Request;

class InsumosService implements InsumosServiceInterface
{
    protected $insumos;
    public function __construct(InsumosRepositoryInterface $insumos)
    {
        $this->insumos = $insumos;
    }
    public function index()
    {
        $insumos = $this->insumos->allInsumos();
        return response()->json(['data' => compact("insumos")], 200);
    }
    public function show($id)
    {
        $insumo = $this->insumos->insumo($id);
         return response()->json(['data' => compact("insumo")],200);
    }
    public function store(InsumoRegisterRequest $request)
    {
       $insumo = $this->insumos->store($request);
       return response()->json(['data' => compact("insumo")],201);
    }
    public function update(InsumoRegisterRequest $request)
    {
        $insumo = $this->insumos->update($request);
        return response()->json(['data' => compact("insumo")], 201);
    }
    public function destroy($id)
    {
        $this->insumos->destroy($id);
        return response()->json(['data' => [
            'code' => 200,
            'message' => 'Eliminado correctamente'
        ]], 200);
    }
}
