<?php
namespace App\Services;

use App\Http\Requests\ModeloImportRequest;
use App\Http\Requests\ModeloRequest;

interface ModeloServiceInterface
{
    public function index();
    public function show($id);
    public function store(ModeloRequest $request);
    public function update(ModeloRequest $request);
    public function destroy($id);
    public function import(ModeloImportRequest $request);
}
