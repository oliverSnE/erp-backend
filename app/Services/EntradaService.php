<?php

namespace App\Services;

use App\Repositories\EntradaRepositoryInterface;
use Illuminate\Http\Request;

class EntradaService implements EntradaServiceInterface
{
    protected $entradas;

    public function __construct(EntradaRepositoryInterface $entrada) {
        $this->entradas = $entrada;
    }

    public function allByOrdenCompra(int $ordenCompraId) {
        $entradas = $this->entradas->allByOrdenCompra($ordenCompraId);
        return response()->json(['data' => compact('entradas')], 200);
    }

    public function find(int $id) {
        $entrada = $this->entradas->find($id);
        return response()->json(['data' => compact('entrada')], 200);
    }

    public function create(Request $request) {
        $entrada = $this->entradas->create($request);
        return response()->json(['data' => compact('entrada')], 201);
    }

    public function addFactura(Request $request) {
        $entrada = $this->entradas->addFactura($request);
        return response()->json(['data' => compact('entrada')], 201);
    }

    public function getFactura(int $id) {
        $entrada = $this->entradas->getFactura($id);
        return response()->json(['data' => compact('entrada')], 200);
    }


    public function insumosRestantes(int $orden_compra_id) {
        $detalle = $this->entradas->getInsumosRestantes($orden_compra_id);
        return response()->json(['data' => compact('detalle')], 200);
    }

}
