<?php
namespace App\Services;

use Illuminate\Http\Request;

interface ImportServiceInterface {
    public function import(Request $request);
    public function onError(\Throwable $e);

}