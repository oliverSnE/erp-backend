<?php

namespace App\Services;

use App\Http\Requests\CuadrillaRequest;
use App\Http\Requests\CuadrillaImportRequest;
use App\Repositories\CuadrillaRepositoryInterface;
use App\Imports\CuadrillasImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\JsonResponse;

class CuadrillaService implements CuadrillaServiceInterface
{
    protected $cuadrillaRepository;

    public function __construct(CuadrillaRepositoryInterface $cuadrilla)
	{
        $this->cuadrillaRepository = $cuadrilla;
    }


    public function create(CuadrillaRequest $request): JsonResponse
    {
        $cuadrilla = $this->cuadrillaRepository->create($request);

        return response()->json(['data' =>compact('cuadrilla')],201);
    }

    public function update(cuadrillaRequest $request): JsonResponse
    {
        $cuadrilla = $this->cuadrillaRepository->update($request);

        return response()->json(['data' =>compact('cuadrilla')],201);
    }

    public function findById(int $id): JsonResponse
    {
        $cuadrilla = $this->cuadrillaRepository->findById($id);
        return response()->json(['data' =>compact('cuadrilla')], 200);
    }

    public function findAll(): JsonResponse
    {
        $cuadrillas = $this->cuadrillaRepository->findAll();
        return response()->json(['data' =>compact('cuadrillas')], 200);
    }

    public function destroy(int $id): JsonResponse
    {
        $this->cuadrillaRepository->destroy($id);
        return response()->json(['data' =>["success" => true]]);
    }

    public function import(CuadrillaImportRequest $request): JsonResponse
    {
        try {
            Excel::import(new CuadrillasImport, request()->file('file'));
         } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
             $failures = $e->failures();
             foreach ($failures as $failure) {
                 $failure->row(); // row that went wrong
                 $failure->attribute(); // either heading key (if using heading row concern) or column index
             }
             return response()->json(['data' => [
                 'code' => 406,
                 'message' => 'No fue posible importar el archivo'
             ]], 406); // Expectation Failed
         }
         return response()->json(['data' => [
             'code' => 201,
             'message' => 'Importación exitosa'
         ]], 201);   //200
    }
}
