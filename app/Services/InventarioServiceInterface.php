<?php
namespace App\Services;

use Illuminate\Http\Request;

interface InventarioServiceInterface {

    public function inventario($fracc_id);
    public function inventarioReport($fracc_id);
}
