<?php

namespace App\Services;

use App\Repositories\FamiliaRepositoryInterface;

class FamiliaService implements FamiliaServiceInterface
{
    protected $familia;
    public function __construct(FamiliaRepositoryInterface $familia)
    {
        $this->familia = $familia;
    }
    public function index()
    {
        $familias = $this->familia->all();
        return response()->json(['data' => compact("familias")],201);
    }




}
