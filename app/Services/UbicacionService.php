<?php

namespace App\Services;

use App\Exports\AvanceUbicacionExport;
use App\Http\Requests\AvanceUbicacionReportRequest;
use App\Http\Requests\UbicacionDestajosRequest;
use App\Http\Requests\UbicacionEmpleadoDestajoRequest;
use App\Http\Requests\UbicacionMultipleRequest;
use App\Http\Requests\UbicacionRequest;
use App\Repositories\UbicacionRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UbicacionService implements UbicacionServiceInterface
{
    protected $ubicacionRepository;

    public function __construct(UbicacionRepositoryInterface $ubicacionRepository)
    {
        $this->ubicacionRepository = $ubicacionRepository;
    }

    public function create(UbicacionRequest $request)
    {
        $ubicacion = $this->ubicacionRepository->create($request);

        return response()->json(['data' =>compact('ubicacion')],201);
    }

    public function createMultiple(UbicacionMultipleRequest $request)
    {

        $ubicaciones = $this->ubicacionRepository->createMultiple($request);

        return response()->json(['data' =>compact('ubicaciones')],201);
    }

    public function agregarDestajo(UbicacionDestajosRequest $request)
    {
        $ubicacion = $this->ubicacionRepository->agregarDestajo($request);

        return response()->json(['data' =>compact('ubicacion')],201);
    }

    public function eliminarDestajo(UbicacionDestajosRequest $request)
    {
        $ubicacion = $this->ubicacionRepository->eliminarDestajo($request);

        return response()->json(['data' =>compact('ubicacion')],201);
    }

    public function update(UbicacionRequest $request)
    {
        $ubicacion = $this->ubicacionRepository->update($request);

        return response()->json(['data' =>compact('ubicacion')],201);
    }

    public function find(int $id)
    {
        $ubicacion = $this->ubicacionRepository->find($id);

        return response()->json(['data' =>compact('ubicacion')],201);
    }

    public function progresoDeObra(int $ubicacion_id)
    {
        $progreso = $this->ubicacionRepository->getProgresoDeObra($ubicacion_id);

        return response()->json(['data' =>compact('progreso')],201);
    }

    public function compras(int $ubicacion_id)
    {
        $compras = $this->ubicacionRepository->getCompras($ubicacion_id);

        return response()->json(['data' =>compact('compras')],201);
    }

    public function delete(int $ubicacion_id)
    {
        $this->ubicacionRepository->delete($ubicacion_id);

        return response()->json(['data' =>['message' => 'La ubicacion a sido eliminada']],200);
    }

    public function avanceUbicaciones(AvanceUbicacionReportRequest $request)
    {
        $ldate = date('Y-m-d');
        return Excel::download(
            new AvanceUbicacionExport(
                    $request->get('ubicaciones'),
                    $request->get('fecha'),
                    $request->get('fraccionamiento_id')
                ), 'Avance por ubicacion '.$ldate.'.xlsx');
    }

    public function ubicacionesByFracc(int $fracc_id)
    {
        $ubicaciones = $this->ubicacionRepository->ubicacionesByFracc($fracc_id);

        return response()->json(['data' =>compact('ubicaciones')],201);
    }

    public function empeladosDestajo(UbicacionEmpleadoDestajoRequest $request)
    {
        $empleados = $this->ubicacionRepository->getEmpeladosDestajo($request);

        return response()->json(['data' =>compact('empleados')],201);
    }

}
