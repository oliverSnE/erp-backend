<?php
namespace App\Services;

use App\Http\Requests\FacturaFraccionamientoRequest;

interface FacturaFraccionamientoServiceInterface {

    public function fraccionamientoReport(FacturaFraccionamientoRequest $request);
}
