<?php

namespace App\Services;

use App\Exports\NominaExport;
use App\Http\Requests\NominaReportRequest;
use App\Http\Requests\NominaRequest;
use App\Repositories\NominaRepositoryInterface;
use Maatwebsite\Excel\Facades\Excel;

class NominaService implements NominaServiceInterface
{
    protected $nominas;

    public function __construct(NominaRepositoryInterface $nominas) {
        $this->nominas = $nominas;
    }

    public function index()
    {
        $nominas = $this->nominas->index();
        return response()->json(['data' => compact('nominas')],200);

    }
    public function create(NominaRequest $request)
    {
        $nominas = $this->nominas->create($request);
        return response()->json(['data' => compact('nominas')],200);
    }
    public function destroy($id)
    {
        $this->nominas->destroy($id);

        return response()->json(['data' => [
            'code' => 200,
            'message' => 'Eliminado correctamente'
        ]], 200);
    }

    public function nomina(int $fracc_id)
    {
        $nominas = $this->nominas->getNomina($fracc_id);
        return response()->json(['data' => compact('nominas')],200);
    }

    public function find($id)
    {
        $nomina = $this->nominas->find($id);
        return response()->json(['data' => compact('nomina')],200);
    }

    public function report(NominaReportRequest $request)
    {
        $ldate = date('Y-m-d');
        return Excel::download(
            new NominaExport(
                    $request->get('nomina_id'),
                    $request->get('fraccionamiento_id')
                ), 'Nomina'.$ldate.'.xlsx');
    }
}
