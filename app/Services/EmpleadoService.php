<?php

namespace App\Service;

use App\Http\Requests\EmpleadoRequest;
use App\Repositories\EmpleadoRepositoryInterface;
use Illuminate\Http\JsonResponse;

class EmpleadoService implements EmpleadoServiceInterface
{
    protected $empleadoRepository;

    public function __construct(EmpleadoRepositoryInterface $empleado)
	{
		$this->empleadoRepository = $empleado;
    }


    public function create(EmpleadoRequest $request): JsonResponse
    {
        $empleado = $this->empleadoRepository->create($request);

        return response()->json(['data' =>compact('empleado')],201);
    }

    public function update(EmpleadoRequest $request): JsonResponse
    {
        $empleado = $this->empleadoRepository->update($request);

        return response()->json(['data' =>compact('empleado')],201);
    }

    public function findById(int $id): JsonResponse
    {
        $empleado = $this->empleadoRepository->findById($id);
        return response()->json(['data' =>compact('empleado')], 200);
    }

    public function all(): JsonResponse
    {
        $empleados = $this->empleadoRepository->findAll();
        return response()->json(['data' =>compact('empleados')], 200);
    }

    public function destroy(int $id): JsonResponse
    {
        $this->empleadoRepository->destroy($id);
        return response()->json(['data' =>["success" => true]]);
    }

    public function puestos(): JsonResponse
    {
        $puestos  =$this->empleadoRepository->puestos();
        return response()->json(['data' =>compact('puestos')]);
    }

}
