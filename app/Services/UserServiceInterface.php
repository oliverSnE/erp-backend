<?php

namespace App\Service;

use App\Http\Requests\UserChangeUbicacionesRequest;
use App\Http\Requests\UserRegisterRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface UserServiceInterface
{
    /**
     * Funcion para crear un token de autentificacion para un usuario
     * regresa un token valido.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Request $request): JsonResponse;

    /**
     * Funcion para registrar un nuevo usuario.
     * Regresa el usuario registrado.
     *
     * @param UserRegisterRequest $request
     * @return JsonResponse
     */
    public function register(UserRegisterRequest $request): JsonResponse;

    /**
     * Funcion para actualizar la informacion de un usuario.
     * Regresa el usuario actualizado.
     *
     * @param UserRegisterRequest $request
     * @return JsonResponse
     */
    public function update(UserRegisterRequest $request): JsonResponse;


    /**
     * Funcion para obtener los roles de usuarios.
     * Regresa los roles.
     *
     * @return JsonResponse
     */
    public function roles(): JsonResponse;

    /**
     * Funcion para invalidar el token de autentificacion del usuario.
     * Regresa un mensaje si la operacion funciono.
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse;

    /**
     * Funcion para crear un token valido apartir del token del usuario.
     * Regresa un nuevo token validad con duracion de 5 minutos.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse;

    /**
     * Funcion para obtener el usuario al que le pertenece el token.
     * Regresa la informacion del usuario, si no se encuetra mandara un error.
     *
     * @return JsonResponse
     */
    public function user(): JsonResponse;

    /**
     * Funcion para obtener un usuario en particular por medio de un id.
     * Regresa la informacion del usuario sin la contraseña al que corresponde el id.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function userById(int $id): JsonResponse;

    /**
     * Funcion para obtener todos los usuarios de la base de datos.
     * Regresa un listado con todos los usuarios de la base de datos
     *
     * @return JsonResponse
     */
    public function users(): JsonResponse;

    /**
     * Funcion para eliminar un usuario de la base de datos.
     * Regresa un estatus de success si el usuario fue eliminado.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse;


    /**
     * Funcion para asignar ubicaciones a un usuario en particular
     * Regresa usuario junto con sus ubicaciones.
     * @param Request $request
     * @return JsonResponse
     */
    public function changeUbicaciones(UserChangeUbicacionesRequest $request): JsonResponse;

    /**
     * Regresa las ubicaciones que se le puede asignar al usuario
     *
     * @param integer $user_id
     * @return JsonResponse
     */
    public function ubicacionesDisponibles(int $user_id): JsonResponse;

}
