<?php

namespace App\Services;

use App\Http\Requests\EtapaRequest;
use App\Repositories\EtapaRepositoryInterface;

class EtapaService implements EtapaServiceInterface
{
    protected $etapaRepository;

    public function __construct(EtapaRepositoryInterface $etapaRepository)
    {
        $this->etapaRepository = $etapaRepository;
    }

    public function create(EtapaRequest $request)
    {
        $etapa = $this->etapaRepository->create($request);

        return response()->json(['data' =>compact('etapa')],201);
    }

    public function update(EtapaRequest $request)
    {
        $etapa = $this->etapaRepository->update($request);

        return response()->json(['data' =>compact('etapa')],201);
    }

    public function find(int $id)
    {
        $etapa = $this->etapaRepository->find($id);

        return response()->json(['data' =>compact('etapa')],201);
    }
    public function destroy(int $id)
    {
        $etapa = $this->etapaRepository->destroy($id);


        return response()->json(['data' =>['success' => $etapa]]);
    }

}
