<?php

namespace App\Services;

use App\Http\Requests\CuadrillaRequest;
use App\Http\Requests\CuadrillaImportRequest;
use Illuminate\Http\JsonResponse;


interface CuadrillaServiceInterface
{
    /**
     * Funcion para crear un nuevo e,pleado.
     * Regresa el empleado creado.
     *
     * @param CuadrillaRequest $request
     * @return JsonResponse
     */
    public function create(CuadrillaRequest $request): JsonResponse;

    /**
     * Funcion para actualizar la informacion de un empleado.
     * Regresa el empleado actualizado.
     *
     * @param CuadrillaRequest $request
     * @return JsonResponse
     */
    public function update(CuadrillaRequest $request): JsonResponse;

    /**
     * Funcion para obtener un empleado en particular por medio de un id.
     * Regresa la informacion del empleado al que corresponde el id.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function findById(int $id): JsonResponse;

    /**
     * Funcion para obtener todos los empleados de la base de datos.
     * Regresa un listado con todos los empleados de la base de datos
     *
     * @return JsonResponse
     */
    public function findAll(): JsonResponse;

    /**
     * Funcion para eliminar un empleado de la base de datos.
     * Regresa un estatus de success si el empleado fue eliminado.
     *
     * @param integer $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse;

    /**
     * Funcion que regresa una lista de puestos posibles
     *
     * @return JsonResponse
     */

    /**
     * Funcion para importar los archivos de excel
     * 
     * @param CuadrillaImportRequest $request
     * @return JsonResponse
     */
    public function import(CuadrillaImportRequest $request): JsonResponse;
}
