<?php
namespace App\Services;

use App\Http\Requests\CostoDestajoRequest;
use App\Http\Requests\DestajoRequest;
use App\Http\Requests\DetalleDestajoRequest;
use Illuminate\Http\Request;

interface DestajoServiceInterface
{
    public function index();
    public function show($id);
    public function getDetalleById($detalle_id);
    public function create(DestajoRequest $request);
   // public function update(DestajoRequest $request);
    public function destroy($id);
    public function editDetalle(DetalleDestajoRequest $request);
    public function getTipos();
    public function destajosByUbicacion(Request $request);
    public function costoDestajoByUbicacion(CostoDestajoRequest $request);
    public function destajosBySubcontrato(Request $request);
    public function searchByName(Request $request);
}
