<?php

namespace App\Service;

use App\Http\Requests\UserChangeUbicacionesRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\JsonResponse;

class UserService implements UserServiceInterface
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $user)
	{
		$this->userRepository = $user;
    }

    public function authenticate(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(UserRegisterRequest $request): JsonResponse
    {
        $user = $this->userRepository->create($request);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function update(UserRegisterRequest $request): JsonResponse
    {
        $user = $this->userRepository->update($request);

        return response()->json(['data' => compact('user')],JsonResponse::HTTP_OK);
    }

    public function roles(): JsonResponse
    {
        $roles = $this->userRepository->getRoles();
        return response()->json(['data' => compact('roles')]);
    }

    public function logout(): JsonResponse
    {
        JWTAuth::invalidate();

        return response()->json([ "data" =>
            [
                'message' => 'success',
                'code' => 200
            ]
        ], 200);
    }

    public function refresh(): JsonResponse
    {
        $error = $this->getAuthenticatedUser();
        if ($error->code === null) {
            $token = JWTAuth::getToken();
            $new_token = JWTAuth::refresh($token);
            return response()->json(compact('new_token'));
        }

        return response()->json($error, $error->code);
    }

    public function user(): JsonResponse
    {
        $user = $this->getAuthenticatedUser();
        if($user->code === null){
            return response()->json(['data' => compact('user')]);
        }
        return response()->json(["error" => $user], $user->code);
    }

    public function userById(int $id): JsonResponse
    {
        $user = $this->userRepository->findById($id);
        return response()->json(['data' => compact('user')]);
    }

    public function users(): JsonResponse
    {
        $users = $this->userRepository->findAll();
        return response()->json(['data' => compact('users')]);
    }

    public function destroy(int $id): JsonResponse
    {
        $this->userRepository->destroy($id);
        return response()->json(['data' => ["message" => "El usuario a sido eliminado"]]);
    }

    public function changeUbicaciones(UserChangeUbicacionesRequest $request): JsonResponse
    {
        $user = $this->userRepository->changeUbicaciones($request);
        return response()->json(['data' =>compact('user')],200);
    }

    public function ubicacionesDisponibles(int $user_id): JsonResponse
    {
        $ubicaciones = $this->userRepository->getUbicacionesDisponibles($user_id);
        return response()->json(['data' =>compact('ubicaciones')],200);
    }

    /**
     * Funcion que obtiene el usuario del token.
     *
     * @return object
     */
    private function getAuthenticatedUser(): object
    {
        try {

            return JWTAuth::parseToken()->authenticate();

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return [
                'message' =>'token_expired',
                'code' => $e->getStatusCode()
            ];

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return [
                'message' =>'token_invalid',
                'code' => $e->getStatusCode()
            ];

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return [
                'message' =>'token_absent',
                'code' => $e->getStatusCode()
            ];

        } catch (Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            return [
                'message' =>'token_black_listen',
                'code' => $e->getStatusCode()
            ];
        }
    }

}
