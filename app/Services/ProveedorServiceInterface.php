<?php
namespace App\Services;

use App\Http\Requests\ProveedorRequest;

interface ProveedorServiceInterface
{
    public function index();
    public function find($id);
    public function store(ProveedorRequest $request);
    public function update(ProveedorRequest $request);
    public function destroy($id);
}
