<?php

namespace App\Services;

use App\Repositories\UnidadRepositoryInterface;

class UnidadService implements UnidadServiceInterface
{
    protected $unidad;
    public function __construct(UnidadRepositoryInterface $unidad)
    {
        $this->unidad = $unidad;
    }
    public function index()
    {
        $unidades =  $this->unidad->all();
        return response()->json(['data' => compact("unidades")],201);
    }




}
