<?php
namespace App\Services;

use App\Http\Requests\MailRequest;

interface MailServiceInterface
{
    public function notificacion(MailRequest $request);
}
