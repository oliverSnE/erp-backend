<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class DetalleDestajoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'PATH': {
                return [
                    'detalle_id' => 'required|exists:detalle_destajos,id',
                    'cantidad' => 'required|max:1000000|numeric'
                ];
            }
        }

    }
    public function messages()
    {
        return [
            'detalle_id.required' => 'El detalle es necesario.',
            'detalle_id.exists' => 'El detalle no existe.',

            'cantidad.required' => 'La cantidad es necesaria.',
            'cantidad.max'  =>  'La cantidad no debe ser mayor a 6 dígitos.',
            'cantidad.numeric'  =>  'La cantidad deben ser sólo números.',
        ];
    }
    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
