<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class FraccionamientoRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':{
                return [
                    'nombre' => 'required|string|max:100|min:5|unique:fraccionamientos|regex:/^[A-Za-z0-9 ,.]+$/',
                    'direccion' => 'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                    'logo' =>'required|mimetypes:image/jpeg,image/png,image/bmp|max:10000',
                    'razon_social_factura' =>'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                    'rfc_factura' => 'required|string|max:12|min:10|regex:/^[A-Za-z0-9 ,.]+$/',
                    'cp_factura' =>'required|string|max:10|min:4|regex:/^[0-9]+$/',
                    'direccion_factura' => 'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'id' => 'required|int|max:255',
                    'nombre' => 'required|string|max:100|min:5|unique:fraccionamientos,nombre,'.$this->get('id').'|regex:/^[A-Za-z0-9 ,.]+$/',
                    'direccion' => 'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                    'logo' =>'mimetypes:image/jpeg,image/png,image/bmp|max:10000',
                    'razon_social_factura' =>'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                    'rfc_factura' => 'required|string|max:12|min:10|regex:/^[A-Za-z0-9 ,.]+$/',
                    'cp_factura' =>'required|string|max:10|min:4|regex:/^[0-9]+$/',
                    'direccion_factura' => 'required|string|max:100|min:5|regex:/^[A-Za-z0-9 ,.]+$/',
                ];
            }
        }
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es necesario!',
            'nombre.max' => 'El nombre no puede contener mas de 100 caracteres!',
            'nombre.min' => 'El nombre no puede contener menos de 5 caracteres!',
            'nombre.unique' => 'El nombre del fraccionamiento ya existe!',
            'nombre.regex' => 'El nombre del fraccionamiento es invalido!',
            'direccion.required' => 'La direccion es necesario!',
            'direccion.max' => 'La direccion no puede contener mas de 100 caracteres!',
            'direccion.min' => 'La direccion no puede contener menos de 5 caracteres!',
            'direccion.regex' => 'La direccion del fraccionamiento es invalida!',
            'direccion_factura.required' => 'La direccion de facturacion es necesaria!',
            'direccion_factura.max' => 'La direccion no puede contener mas de 100 caracteres!',
            'direccion_factura.min' => 'La direccion no puede contener menos de 5 caracteres!',
            'direccion_factura.regex' => 'La direccion es invalida',
            'razon_social_factura.required' => 'La razon social es necesaria!',
            'razon_social_factura.max' => 'La razon social no puede contener mas de 100 caracteres!',
            'razon_social_factura.min' => 'La razon social no puede contener menos de 5 caracteres!',
            'razon_social_factura.regex' => 'La razon social es invalido!',
            'rfc_factura.required' => 'El RFC es necesario!',
            'rfc_factura.max' => 'El RFC no puede contener mas de 12 caracteres!',
            'rfc_factura.min' => 'El RFC no puede contener menos de 10 caracteres!',
            'rfc_factura.regex' => 'El RFC es invalido!',
            'cp_factura.required' => 'El codigo postal es necesario!',
            'cp_factura.max' => 'El codigo postal no puede contener mas de 10 caracteres!',
            'cp_factura.min' => 'El codigo postal no puede contener menos de 4 caracteres!',
            'cp_factura.regex' => 'El codigo postal es invalido!',
            'logo.required' => 'El logo es necesario!',
            'logo.max' => 'El logotipo no puede contener mas de 10 mb!',
            'logo.mimetypes' => 'El logotipo solo puede ser JPEG, PNG y BMP!',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors,
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
