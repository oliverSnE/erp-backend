<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProveedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
           case 'POST' : {
                return [
                    'nombre' => 'required|string|max:255|unique:proveedores,nombre|min:4',
                    'direccion' => 'required|string|max:255|min:4',
                    'municipio' => 'required|string|max:255|min:4',
                    'estado'    =>  'required|string|max:255|min:4',
                    'cp'    =>  'required|digits_between:4,6',
                    'tel'    =>  'required|digits_between:6,15',
                    'email'    =>  'required|string|email|max:255|unique:proveedores,email',
                    'familias'    =>  'required|exists:familias,id'
                ];
            }
            case 'PUT' :
            case 'PATCH' : {
                return [
                    'nombre' => 'required|string|max:255|min:4|unique:proveedores,nombre,'.$this->id,
                    'direccion' => 'required|string|max:255|min:4',
                    'municipio' => 'required|string|max:255|min:4',
                    'estado'    =>  'required|string|max:255|min:4',
                    'cp'    =>  'required|digits_between:4,6',
                    'tel'    =>  'required|digits_between:6,15',
                    'email'    =>  'required|string|email|max:255|unique:proveedores,email,'.$this->id,
                    'familias'    =>  'required|exists:familias,id'
                ];
            }
        }

    }

    public function messages()
    {
        return [

            'nombre.required' => 'El nombre es necesario',
            'nombre.string' => 'El nombre no puede contener sólo números',
            'nombre.max' => 'El nombre no puede contener más de 255 caracteres',
            'nombre.min' => 'El nombre no puede contener menos de 4 caracteres',
            'nombre.unique' => 'El nombre ya existe',

            'direccion.required' => 'La dirección es necesaria',
            'direccion.string' => 'La dirección no puede contener sólo números',
            'direccion.max' => 'La dirección no puede contener más de 255 caracteres',
            'direccion.min' => 'La dirección no puede contener menos de 4 caracteres',

            'municipio.required' => 'El municipio es requerido',
            'municipio.string' => 'El municipio no puede contener sólo números',
            'municipio.max' => 'El municipio no puede contener más de 255 caracteres',
            'municipio.min' => 'El municipio no puede contener menos de 4 caracteres',

            'estado.required' => 'El estado es requerido',
            'estado.string' => 'El estado no puede contener sólo nùmeros',
            'estado.max' => 'El estado no puede contener más de 255 caracteres',
            'estado.min' => 'El estado no puede contener menos de 4 caracteres',

            'cp.required' => 'El código postal es requerido',
            'cp.digits_between' => 'El código postal no es válido debe contener entre 4 y 6 dígitos.',

            'tel.required' => 'El número telefónico es necesario',
            'tel.digits_between' => 'El número telefónico no es válido',

            'email.required' => 'El correo es necesario',
            'email.string' => 'El correo no pueden ser sólo números.',
            'email.email' => 'El correo debe ser válido',
            'email.max' => 'El estado no puede contener más de 255 caracteres',
            'email.unique' => 'El email ya existe',

            'familias.required' => 'La familia es necesaria',
            'familias.exists' => 'La familia no existe'

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }
}
