<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':{
                return [
                    'nombre' => 'required|string|max:100',
                    'direccion' => 'required|string|max:100',
                    'rfc' =>
                        [
                            'required','string','max:15',
                            Rule::unique('empleados','rfc')->using(function ($q){
                                $q->whereNull('deleted_at');
                            })
                        ],
                    'puesto' => 'required|string|max:100',
                    'nss' =>
                        [
                            'required','string','max:11',
                            Rule::unique('empleados','nss')->using(function ($q){
                                $q->whereNull('deleted_at');
                            })
                        ],
                    'alias' =>
                        [
                            'required','string','max:191',
                            Rule::unique('empleados','alias')->using(function ($q){
                                $q->whereNull('deleted_at');
                            })
                        ],
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'id' => 'required|int|max:255',
                    'nombre' => 'required|string|max:100',
                    'direccion' => 'required|string|max:100',
                    'rfc' =>
                    [
                        'required','string','max:15',
                        Rule::unique('empleados','rfc')->ignore($this->get('id'))->using(function ($q){
                            $q->whereNull('deleted_at');
                        })
                    ],
                    'puesto' => 'required|string|max:100',
                    'nss' =>
                    [
                        'required','string','max:11',
                        Rule::unique('empleados','nss')->ignore($this->get('id'))->using(function ($q){
                            $q->whereNull('deleted_at');
                        })
                    ],
                    'alias' =>
                    [
                        'required','string','max:191',
                        Rule::unique('empleados','alias')->ignore($this->get('id'))->using(function ($q){
                            $q->whereNull('deleted_at');
                        })
                    ],
                ];
            }
        }
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es necesario!',
            'direccion.required' => 'La direccion es necesario!',
            'nss.required' => 'El numero de seguro social es necesario!',
            'rfc.required' => 'El rfc es necesario!',
            'puesto.required' => 'El puesto es necesario!',
            'nombre.max' => 'El nombre no puede contener mas de 100 caracteres!',
            'direccion.max' => 'La direccion no puede contener mas de 100 caracteres!',
            'rfc.max' => 'El rfc no puede contener mas de 100 caracteres!',
            'puesto.max' => 'El puesto no puede contener mas de 100 caracteres!',
            'nss.max' => 'El numero de seguro social no puede contener mas de 11 caracteres!',
            'rfc.unique' => 'El rfc ya se utilizo en otro empleado',
            'nss.unique' => 'El numero de seguro social ya se utilizo en otro empleado',
            'nss.min' => 'El numero de seguro social no puede contener menos de 11 caracteres!',
            'alias.unique' => 'El alias ya se utilizo en otro empleado',
            'alias.max' => 'El alias no puede contener mas de 191 caracteres!',
            'alias.required' => 'El alias es necesario!'
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
