<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrdenCompraUpdateRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'PATCH': {
                    return [
                        'orden_id' => 'required|exists:orden_compras,id',
                        'proveedor_id' => 'required|exists:proveedores,id',
                        'detalle.*.detalle_orden_id' => 'required|exists:detalle_orden_compras,id',
                        'detalle.*.costo' => 'required|numeric|max:1000000'
                    ];
                }
        }

    }
    public function messages()
    {
        return [
            'orden_id.required'                     => 'La orden de compra es necesaria',
            'orden_id.exists'                       => 'La orden de compra no existe no existe',
            'proveedor_id.required'                 => 'El proveedor es necesario',
            'proveedor_id.exists'                   => 'El proveedor no existe',
            'detalle.*.detalle_orden_id.required'   => 'El insumo es necesaraio',
            'detalle.*.detalle_orden_id.exists'     => 'El insumo no existe en la orden de compra',
            'detalle.*.costo.required'              => 'El costo del insumo es necesario',
            'detalle.*.costo.numeric'               => 'El costo debe de ser un numero valido',
            'detalle.*.costo.max'                   => 'El costo no debe exceder los 6 digitos'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
