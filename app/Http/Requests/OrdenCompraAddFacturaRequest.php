<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrdenCompraAddFacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':{
                return [
                    'orden_compra_id' => 'required|exists:orden_compras,id',
                    'folio' => 'required|string|max:100',
                    'fecha' => 'required|date_format:Y-m-d',
                    'factura' =>'required|mimes:doc,docx,pdf,PDF|max:50000',
                    'importe' => 'required|numeric|max:1000000000000'
                ];
            }
        }
    }
    public function messages()
    {
        return [
            'orden_compra_id.required'  => 'La orden de compra es necesaria',
            'orden_compra_id.exists'    => 'La orden de compra no existe',
            'folio.required'            => 'El folio es necesario',
            'folio.max'                 => 'El folio no puede pasar de los 100 caracteres',
            'importe.required'          => 'El importe es necesario',
            'importe.numeric'           => 'El importe debe ser tipo numerico',
            'importe.max'               => 'El importe a excedido el monto maximo',
            'fecha.required'            => 'La fecha es requerida',
            'fecha.date_format'         => 'La fecha no tiene el formato correcto',
            'factura.required'          => 'La factura es necesaraia es necesario',
            'factura.mimes'             => 'La factura debe ser tipo pdf',
            'factura.max'               => 'La factura es demaciado grande para el servidor'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
