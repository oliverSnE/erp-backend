<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class UbicacionMultipleRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numeros.*' => [
                'int','max:1000',
                Rule::unique('ubicaciones','numero')->using(function ($q){
                    $q->whereNull('deleted_at');
                    $q->where('manzana',$this->get('manzana'));
                    $q->where('etapa_id',$this->get('etapa_id'));
                })
            ],
            'manzana' => 'required|string|max:100',
            'etapa_id' => 'required|int|max:255',
            'modelo_id' => 'required|int|max:255'
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id'                    => 'El id es necesario!',
            'numeros.required'      => 'Los numeros es necesario!',
            'manzana.required'      => 'El manzana es necesario!',
            'numeros.*.unique'      => 'El numero ya se encuentra utilizado por otra ubicacion en la misma manzana!',
            'numeros.*.int'         => 'El numero debe ser un entero!',
            'etapa_id.required'     => 'El identificador de etapa es necesario!',
            'modelo_id.required'    => 'El identificador de modelo es necesario!',
            'numero.*.max'          => 'El numero no puede contener mas de 100 caracteres!',
            'manzana.max'           => 'El manzana no puede contener mas de 100 caracteres!',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
