<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
class ModeloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nombre' => 'required|string|max:255|unique:modelos,nombre',
                        'descripcion' => 'required|string|max:255',
                        'fraccionamiento_id' => 'required|exists:fraccionamientos,id',
                        'detalle.*.destajo_id' => 'required|exists:detalle_destajos,id'
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'nombre' => 'required|string|max:255|unique:modelos,nombre,'.$this->id,
                        'descripcion' => 'required|string|max:255',
                        'fraccionamiento_id' => 'required|exists:fraccionamientos,id',
                        'detalle.*.destajo_id' => 'required|exists:detalle_destajos,id'
                    ];
                }
        }

    }
    public function messages()
    {
        return [
            'nombre.unique' => 'El modelo ya existe',

            'nombre.required' => 'El nombre es necesario',
            'descripcion.required' => 'La descripción es necesaria',
            'fraccionamiento_id.required' => 'El fraccionamiento es necesario',
            'detalle_destajo_is.required' => 'El el destajo es necesario',

            'nombre.string' => 'El nombre no puede ser sólo números',
            'descripcion.string' => 'La descripcion no puede ser sólo números',

            'nombre.max' => 'El nombre no puede contener más de 255 caractéres',
            'descripcion.max' => 'La descripcion no puede contener más de 255 caractéres',

            'fraccionamiento_id.exists' => 'El fraccionamiento no existe',
            'detalle.*.destajo_id.exists' => 'El destajo no existe no existe',

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
