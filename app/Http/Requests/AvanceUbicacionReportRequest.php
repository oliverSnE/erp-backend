<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class AvanceUbicacionReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'fraccionamiento_id'    => 'required|exists:fraccionamientos,id',
                    'ubicaciones.*'         => 'required|exists:ubicaciones,id',
                    'fecha'                 => 'required|date_format:Y-m-d'
                ];
            }
        }

    }

    public function messages()
    {
        return [
            'fraccionamiento_id.required' => 'El fraccionamiento es necesario',
            'fraccionamiento_id.exists' => 'El fraccionamiento no existe',
            'ubicaciones.*.required' => 'La ubicación es necesaria',
            'ubicaciones.*.exists' => 'La ubicación no existe',
            'fecha.required' => 'La fecha es necesaria',
            'fecha.date_format' => 'La fecha debe de estar en el formarto yyyy-mm-dd'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
