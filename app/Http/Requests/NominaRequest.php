<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class NominaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'nombre' => 'required|string|max:255|unique:nominas,nombre',
                    'fecha' => 'required|date',
                    'pagado' => 'required|numeric',
                    'descuento' => 'required|numeric',
                    'quemas' => 'required|exists:quemas,id'
                ];
            }
        }
    }
    public function messages() {
        return [
            'nombre.required' => 'El nombre es necesario.',
            'nomnbre.string' => 'El nombre no pueden ser sólo números.',
            'nombre.max' => 'El nombre no puede exceder los 255 caractéres.',
            'nombre.unique' => 'El nombre ya existe.',

            'fecha.required' => 'La fecha es necesaria',
            'fecha.date' => 'Ingrese una fecha válida YYYY/MM/DD',

            'pagado.required' => 'El monto pagado es necesario',
            'pagado.numeric' => 'El monto pagado debe ser numérico',

            'descuento.required' => 'El descuento es necesario.',
            'descuento.numeric' => 'El descuento debe ser numérico.',

            'quemas.required' => 'La quema es necesaria.',
            'quemas.exists' => 'La quema no existe.',

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
