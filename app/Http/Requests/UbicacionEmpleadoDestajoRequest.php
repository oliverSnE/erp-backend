<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class UbicacionEmpleadoDestajoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'ubicacion_id' => 'required|exists:ubicaciones,id',
                        'destajo_id' => 'required|exists:destajos,id'
                    ];
                }
        }
    }
    public function messages()
    {
        return [
            'ubicacion_id.required'     => 'La ubicacion es necesaria',
            'ubicacion_id.exists'       => 'La ubicacion no existe',
            'destajo_id.required'    => 'El destajo es necesario',
            'destajo_id.exists'      => 'El destajo no existe'

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
