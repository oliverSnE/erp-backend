<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrdenSalidaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'fraccionamiento_id' => 'required|exists:fraccionamientos,id',
                    'detalle.*.detalle_destajo_id' => 'required|exists:detalle_destajos,id',
                    'detalle.*.cantidad' => 'required|numeric',
                    'detalle.*.ubicacion_id' => 'required|exists:ubicaciones,id',
                ];
            }
        }

    }

    public function messages()
    {
        return [
            'fraccionamiento_id.required' => 'El fraccionamiento es necesario',
            'detalle.*.detalle_destajo_id.required' => 'El detalle del destajo es necesario',
            'detalle.*.cantidad.required' => 'La cantidad es necesaria',
            'detalle.*.ubicacion_id.required' => 'La ubicación es necesaria',
            'fraccionamiento_id.exists' => 'El fraccionamiento no existe',
            'detalle.*.detalle_destajo_id.exists' => 'El detalle del destajo no existe',
            'detalle.*.ubicacion_id.exists' => 'La ubicación no existe',
            'detalle.*.cantidad.numeric' => 'La cantidad debe ser numérica',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'error' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
