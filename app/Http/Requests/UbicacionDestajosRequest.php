<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class UbicacionDestajosRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'PATCH': {
                    return [
                        'ubicacion_id' => 'required|exists:ubicaciones,id|max:255',
                        'destajo_id' => 'required|exists:destajos,id|max:255'
                    ];
                }
            case "DELETE": {
                return [
                    'ubicacion_id' => 'required|exists:ubicaciones,id|max:255',
                    'destajo_id' => 'required|exists:destajos,id|max:255'
                ];
            }
        }
    }
    public function messages()
    {
        return [
            'ubicacion_id.required' => 'El nombre es necesario',
            'destajo_id.required' => 'La descripcion es necesario',

            'ubicacion_id.exists' => 'La ubicacion no existe',
            'destajo_id.exists' => 'El destajo no existe',

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

}
