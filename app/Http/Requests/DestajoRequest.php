<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class DestajoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nombre' => 'required|string|max:100|min:4|unique:destajos,nombre',
                        'descripcion' => 'required|string|max:500|min:4',
                        'costo' => 'required|numeric|max:1000000',
                        'tipo' => 'required|in:mano_de_obra,mano_obra_materiales,subcontrato',
                        'cantidad' => 'required|numeric|max:1000000',
                        'unidad_id' => 'required|exists:unidades,id',
                        'detalle.*.cantidad' => 'required_if:tipo,==,mano_obra_materiales|numeric|max:1000000',
                        'detalle.*.costo' => 'required_if:tipo,==,mano_obra_materiales|numeric|max:1000000',
                        'detalle.*.insumo_id' => 'required_if:tipo,==,mano_obra_materiales|exists:insumos,id'
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'nombre' => 'required|string|max:100|min:4|unique:destajos,nombre,'.$this->id,
                        'descripcion' => 'required|string|max:500|min:4',
                        'costo' => 'required|numeric|max:1000000',
                        'tipo' => 'required|in:mano_de_obra,mano_obra_materiales,subcontrato',
                        'cantidad' => 'required|numeric|max:1000000',
                        'unidad_id' => 'required|exists:unidades,id',
                        'detalle.*.cantidad' => 'required_if:tipo,==,mano_obra_materiales|numeric|max:1000000',
                        'detalle.*.costo' => 'required_if:tipo,==,mano_obra_materiales|numeric|max:1000000',
                        'detalle.*.insumo_id' => 'required_if:tipo,==,mano_obra_materiales|exists:insumos,id'
                    ];
                }
        }
    }
    public function messages()
    {
        return [
            'nombre.unique' => 'El destajo ya existe.',
            'nombre.required' => 'El nombre es necesario.',
            'nombre.string' => 'El nombre no pueden ser sólo números.',
            'nombre.max' => 'El nombre no puede contener más de 100 caractéres.',
            'nombre.min' => 'El nombre no puede contener menos de 4 caractéres.',

            'descripcion.required' => 'La descripción es necesaria.',
            'descripcion.string' => 'La descripción no puede ser sólo números.',
            'descripcion.max' => 'La descripción no puede contener más de 500 caractéres.',
            'descripcion.min' => 'La descripción no puede contener menos de 4 caractéres.',

            'costo.required' => 'El costo es necesario.',
            'costo.numeric' => 'El costo deben de ser sólo números.',
            'costo.max' => 'El costo no debe ser mayor a 6 dígitos.',

            'tipo.required' => 'El tipo es necesario.',
            'tipo.in' => 'El tipo sólo pueden ser mano_de_obra, mano_obra_materiales o subcontrato.',

            'cantidad.required' => 'La cantidad es necesaria.',
            'cantidad.numeric' => 'La cantidad deben ser sólo números.',
            'cantidad.max' => 'La cantidad no debe ser mayor a 6 dígitos.',

            'unidad_id.required' => 'La unidad de medida es necesaria.',
            'unidad_id.exists' => 'La unidad no existe',

            'detalle.*.cantidad.required_if-' => 'La cantidad es requerida.',
            'detalle.*.cantidad.numeric' => 'La cantidad deben de ser sólo números.',
            'detalle.*.cantidad.max' => 'La cantidad deben ser mayor a 6 dígitos.',

            'detalle.*.costo.required_if' => 'El costo del detalle es necesario.',
            'detalle.*.costo.numeric' => 'El costo del detalle debe de ser numérico.',
            'detalle.*.costo.max' => 'El costo del detalle debe ser mayor a 6 dígitos.',

            'detalle.*.insumo_id.required_if' => 'El insumo es requerido.',
            'detalle.*.insumo_id.exists' => 'La insumo no existe.'

        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
