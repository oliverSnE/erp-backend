<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\JsonResponse;

class InsumoRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST' : {
                return [
                    'nombre' => 'required|string|min:2|max:255|unique:insumos,nombre',
                    'unidad_id' => 'required|exists:unidades,id',
                    'familia' => 'required|string|min:4|max:100|'
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'nombre' => 'required|string|min:2|max:255|unique:insumos,nombre,'.$this->id,
                    'unidad_id' => 'required|exists:unidades,id',
                    'familia' => 'required|string|min:4|max:100|'
                ];
            }
        }

    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es necesaria',
            'nombre.string' => 'El nombre no puede ser sólo números',
            'nombre.max' => 'El nombre no puede contener más de 255 caractéres',
            'nombre.min' => 'El nombre no puede contener menos de 2 caractéres',
            'nombre.unique' => 'El nombre ya existe',
            'unidad_id.required' => 'La unidad es necesaria',
            'unidad_id.exists' => 'La unidad no existe',
            'familia.required' => 'La familia es necesaria',
            'familia.string' => 'La familia debe ser tipo texto',
            'familia.max' => 'La familia no puede contener más de 100 caractéres',
            'familia.min' => 'La familia no puede contener menos de 4 caractéres'
        ];
    }

    public function response(array $errors) {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

}
