<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class UbicacionRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':{
                return [
                    'numero' => [
                        'required','int','max:100',
                        Rule::unique('ubicaciones','numero')->using(function ($q){
                            $q->whereNull('deleted_at');
                            $q->where('manzana',$this->get('manzana'));
                            $q->where('etapa_id',$this->get('etapa_id'));
                        })
                    ],
                    'manzana' => 'required|string|max:100',
                    'etapa_id' => 'required|int|max:999999999|exists:etapas,id'
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'id' => 'required|int|max:999999999|exists:ubicaciones,id',
                    'numero' => [
                        'required','int','max:10000000',
                        Rule::unique('ubicaciones','numero')->ignore($this->get('id'))->using(function ($q){
                            $q->whereNull('deleted_at');
                            $q->where('manzana',$this->get('manzana'));
                            $q->where('etapa_id',$this->get('etapa_id'));
                        })
                    ],
                    'manzana' => 'required|string|max:100',
                    'etapa_id' => 'required|int|max:999999999|exists:etapas,id'
                ];
            }
        }
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'El id es necesario',
            'id.int' => 'El id debe ser numérico',
            'id.max' => 'El id supera en número permitido',
            'id.exists' => 'La ubicación no existe',

            'numero.required' => 'El número es necesario',
            'numero.int' => 'El número no pueden ser letras',
            'numero.max' => 'El número supera el límite permitido',
            'numero.unique' => 'El número ya se encuentra utilizado por otra ubicación en la misma manzana',

            'manzana.required' => 'El nombre de la manzana es necesario',
            'manzana.string' => 'La manzana no debe contener sólo números',
            'manzana.max' => 'La manzana no puede contener más de 100 caracteres',

            'etapa_id.required' => 'La etapa es necesaria',
            'etapa_id.int' => 'La etapa debe ser numérica',
            'etapa_id.max' => 'La etapa supera el límite permitido',
            'etapa_id.exists' => 'La etapa no existe',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
