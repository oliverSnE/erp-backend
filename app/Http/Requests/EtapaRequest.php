<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class EtapaRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':{
                return [
                    'logo' =>'required|mimetypes:image/jpeg,image/png,image/bmp|max:2000',
                    'nombre' => ['required','string','max:100','min:4', Rule::unique('etapas','nombre')->using(function ($q){
                        $q->where('fraccionamiento_id',$this->get('fraccionamiento_id'));
                    })],
                    'fraccionamiento_id' => 'required|int|max:255|exists:fraccionamientos,id'
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'id' => 'required|int|max:255|exists:etapas,id',
                    'logo' =>'mimetypes:image/jpeg,image/png,image/bmp|max:2000',
                    'nombre' => 'required|string|max:100|min:4|unique:etapas,nombre,'.$this->id,
                ];
            }
        }
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'La etapa es necesaria',
            'id.int' => 'El id de la etapa debe de ser numérica',
            'id.exists' => 'La etapa no existe',
            'id.max' => 'El id de la etapa es muy grande',

            'logo.required' => 'El logo es necesario',
            'logo.max' => 'El logotipo no puede contener mas de 2000 mb',
            'logo.mimetypes' => 'El logotipo sólo puede ser JPEG, PNG y BMP',

            'nombre.required' => 'El nombre es necesario',
            'nombre.string' => 'El nombre no puede contener sólo números',
            'nombre.max' => 'El nombre no puede contener mas de 100 caracteres',
            'nombre.min' => 'El nombre no puede contener menos de 4 caracteres',
            'nombre.unique' => 'El nombre ya existe',

            'fraccionamiento_id.required' => 'El identificador del fraccionamiento es necesario',
            'fraccionamiento_id.exists' => 'El fraccionamiento no existe',
            'fraccionamiento_id.int' => 'El fraccionamiento debe ser numérico',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
