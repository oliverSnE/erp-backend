<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class CuadrillaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':{
                return [
                   'nombre' => 'required|string|max:100|unique:cuadrillas,nombre',
                   'detalle.*.empleado_id' => 'required|exists:empleados,id|distinct',
                   'detalle.*.is_jefe' => 'required'
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'nombre' => 'required|string|max:100|unique:cuadrillas,nombre,'.$this->id,
                    'detalle.*.empleado_id' => 'required|exists:empleados,id|distinct',
                    'detalle.*.is_jefe' => 'required'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'nombre.unique' => 'El nombre ya existe.',
            'nombre.required' => 'El nombre es necesario.',
            'nombre.string' => 'El nombre no pueden ser sólo números.',
            'nombre.max' => 'El nombre no puede contener más de 100 caractéres.',
            'detalle.*.empleado_id.required' => 'El empleado es necesario.',
            'detalle.*.empleado_id.exists' => 'El empleado no existe.',
            'detalle.*.empleado_id.distinct' => 'El empleado ya esta asignado en la cuadrilla.',
            'detalle.*.is_jefe.required' => 'Se necesita especificar si el empleado es jefe de cuadrilla.',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['error' =>
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
