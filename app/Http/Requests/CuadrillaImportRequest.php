<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class CuadrillaImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'POST': {
                return [
                    // 'nombre' => 'required|string|max:100|unique:cuadrillas,nombre',
                    // 'empleado_id' => 'required|exists:empleados,id',
                    'file' => 'required|max:50000'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            // 'nombre.unique' => 'El nombre ya existe.',
            // 'nombre.required' => 'El nombre es necesario.',
            'file.required' => 'El archivo Excel es necesario',
            // 'nombre.string' => 'El nombre no pueden ser sólo números.',
            // 'nombre.max' => 'El nombre no puede contener más de 100 caractéres.',
            'file.mimes' => 'El archivo tiene que ser tipo excel.',
        ];
    }

    public function response(array $errors) 
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator) 
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
