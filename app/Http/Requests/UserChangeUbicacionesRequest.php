<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserChangeUbicacionesRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'user_id' => 'required|exists:users,id',
                        'ubicaciones' => 'required|array|min:1',
                        'ubicaciones.*' => 'required|exists:ubicaciones,id'
                    ];
                }
        }
    }
    public function messages()
    {
        return [
            'user_id.required'      => 'El usuario es necesario',
            'user_id.exists'        => 'El usuario no existe',
            'ubicaciones.required'  => 'Las ubicaciones son necesarias',
            'ubicaciones.array'     => 'Las ubicaciones tienen que ser en formato array',
            'ubicaciones.*.exists'  => 'Una de las ubicaciones no existe',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
