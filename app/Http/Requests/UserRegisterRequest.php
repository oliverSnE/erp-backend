<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':{
                return [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                    'rol' => 'required|in:admin,almacenista,admin_proyectos,compras,invitado,gerente_construccion,supervisor_edificacion,supervisor_urbanizacion,contador,residente_vivienda,residente_urbanizacion,control_obra',
                    'fraccionamientos.*' => 'required|exists:fraccionamientos,id'
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'id' => 'required|int|max:255',
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users,email,'.$this->get('id').',id',
                    'password' => 'string|min:6|confirmed',
                    'rol' => 'required|in:admin,almacenista,admin_proyectos,compras,invitado,gerente_construccion,supervisor_edificacion,supervisor_urbanizacion,contador,residente_vivienda,residente_urbanizacion,control_obra',
                    'fraccionamientos.*' => 'required|exists:fraccionamientos,id'
                ];
            }
        }
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'         => 'El nombre es necesario!',
            'name.regex'            => 'No se permiten caracteres especiales en el nombre!',
            'email.required'        => 'El email es necesario!',
            'rol.required'          => 'El rol es necesario!',
            'name.max'              => 'El nombre no puede contener mas de 255 caracteres!',
            'email.max'             => 'El email no puede contener mas de 255 caracteres!',
            'password.max'          => 'La contraseña no puede contener mas de 255 caracteres!',
            'email.unique'          => 'El email ya se utilizo en otro usuario',
            'email.email'           => 'El correo debe tener un formato valido',
            'password.min'          => 'La contraseña tiene que ser mayor a 6 caracteres',
            'password.confirmed'    => 'La contraseña de confirmacion no coincide',
            'rol.in'                => 'El rol no existe'
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
