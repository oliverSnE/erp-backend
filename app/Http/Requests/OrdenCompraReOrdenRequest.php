<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrdenCompraReOrdenRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'orden_id' => 'required|exists:orden_compras,id',
                        'fraccionamiento_id' => 'required|exists:fraccionamientos,id',
                        'destajos' => 'required|array|min:1',
                        'ubicaciones' => 'required|array|min:1',
                        'ubicaciones.*' => 'exists:ubicaciones,id',
                        'destajos.*' => 'exists:destajos,id',
                        'detalle.*.proveedor_id' => 'required|exists:proveedores,id',
                        'detalle.*.insumo_id' => 'required|exists:insumos,id',
                        'detalle.*.costo' => 'required|numeric|max:1000000',
                        'detalle.*.cantidad' => 'required|numeric|max:1000000',
                        'detalle.*.cambios' => 'required|bool'
                    ];
                }
        }

    }
    public function messages()
    {
        return [
            'orden_id.required'                 => 'La orden de compra es necesario',
            'orden_id.exists'                   => 'La orden de compra no existe',
            'fraccionamiento_id.required'       => 'El fraccionamiento es necesario',
            'fraccionamiento_id.exists'         => 'El fraccionamiento no existe',
            'ubicaciones.required'              => 'Las ubicaciones son necesarios',
            'ubicaciones.*.exists'              => 'Una de las ubicaciones seleccionadas no existe',
            'destajos.required'                 => 'Los destajos son necesarias',
            'destajos.*.exists'                 => 'Uno de los destajos no existe',
            'detalle.*.cambios.required'        => 'Es necesario el campo cambios',
            'detalle.*.cambios.bool'            => 'Cambios tiene que ser tipo booleano',
            'detalle.*.proveedor_id.required'   => 'Es necesario el proveedor',
            'detalle.*.proveedor_id.exist'      => 'El proveedor no existe',
            'detalle.*.insumo_id.required'      => 'Es necesario seleccionar un insumo',
            'detalle.*.insumo_id.exist'         => 'El insumo no existe',
            'detalle.*.costo.required'          => 'El costo del insumo es necesario',
            'detalle.*.costo.numeric'           => 'El costo debe de ser un numero valido',
            'detalle.*.costo.max'               => 'El costo no debe exceder los 6 digitos',
            'detalle.*.cantidad.required'       => 'La cantidad de insumos es necesaria',
            'detalle.*.cantidad.numeric'        => 'La cantidad debe ser un numero valido',
            'detalle.*.cantidad.max'            => 'La cantidad no deve de ser mayor a 6 digitos'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            'errors' => [
                'code' => 422,
                'message' => $errors
            ]
        ], 422);
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $errors
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
