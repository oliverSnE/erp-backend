<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmpleadoRequest;
use App\Service\EmpleadoServiceInterface;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    protected $empleadoService;

    public function __construct(EmpleadoServiceInterface $empleadoService)
    {
        $this->empleadoService = $empleadoService;
    }

    public function create(EmpleadoRequest $request)
    {
        return $this->empleadoService->create($request);
    }

    public function update(EmpleadoRequest $request)
    {
        return $this->empleadoService->update($request);
    }

    public function find($id)
    {
        return $this->empleadoService->findById($id);
    }

    public function all()
    {
        return $this->empleadoService->all();
    }

    public function destroy($id)
    {
        return $this->empleadoService->destroy($id);
    }

    public function puestos()
    {
        return $this->empleadoService->puestos();
    }

}
