<?php

namespace App\Http\Controllers;

use App\Models\Entrada;
use Illuminate\Http\Request;
use App\Services\EntradaServiceInterface;

class EntradaController extends Controller
{
    protected $entradaService;

    public function __construct(EntradaServiceInterface $entradaService) {
        $this->entradaService = $entradaService;
    }

    public function allByOrdenCompra(int $ordenCompraId){
        return $this->entradaService->allByOrdenCompra($ordenCompraId);
    }

    public function find(int $id){
        return $this->entradaService->find($id);
    }

    public function create(Request $request)
    {
        return $this->entradaService->create($request);
    }

    public function addFactura(Request $request)
    {
        return $this->entradaService->addFactura($request);
    }

    public function getFactura(int $id)
    {
        return $this->entradaService->getFactura($id);
    }

    public function insumosRestantes(int $ordenCompraId)
    {
        return $this->entradaService->insumosRestantes($ordenCompraId);
    }
}
