<?php

namespace App\Http\Controllers;

use App\Services\UnidadServiceInterface;
use Illuminate\Http\Request;

class UnidadController extends Controller
{
    protected $unidadService;

    public function __construct(UnidadServiceInterface $unidadService)
    {
        $this->unidadService = $unidadService;
    }

    public function index()
    {
        return $this->unidadService->index();
    }
}
