<?php

namespace App\Http\Controllers;

use App\Http\Requests\FacturaUbicacionRequest;
use App\Services\FacturaUbicacionServiceInterface;

class FacturaUbicacionController extends Controller
{
    protected $ubicacion;
    public function __construct(FacturaUbicacionServiceInterface $ubicacion)
    {
        $this->ubicacion = $ubicacion;
    }
    public function ubicacionReport(FacturaUbicacionRequest $ubicacion_id)
    {
        return $this->ubicacion->ubicacionReport($ubicacion_id);
    }
}
