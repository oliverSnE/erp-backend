<?php

namespace App\Http\Controllers;

use App\Exports\InsumosPorUbicacionExport;
use App\Http\Requests\InsumosUbicacionReportRequest;
use App\Http\Requests\OrdenSalidaDestajosEstatusRequest;
use App\Http\Requests\OrdenSalidaRequest;
use App\Services\OrdenSalidaServiceInterface;
use App\Repositories\InventarioRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OrdenSalidaController extends Controller
{

    protected $ordenService;
    protected $inventario;
    public function __construct(OrdenSalidaServiceInterface $ordenService, InventarioRepositoryInterface $inventario)
    {
        $this->inventario = $inventario;
        $this->ordenService = $ordenService;
    }

     public function create(OrdenSalidaRequest $request)
    {
        return $this->ordenService->create($request);
    }
    public function ubicaciones(Request $request)
    {
        return $this->ordenService->ubicaciones($request);
    }

    public function destajos(Request $request)
    {
        return $this->ordenService->destajos($request);
    }
    public function findAllByFraccionamiento(int $id)
    {
        return $this->ordenService->findAllByFraccionamiento($id);
    }
    public function find(int $id)
    {
        return $this->ordenService->find($id);
    }
    public function findByFolio(string $folio)
    {
        return $this->ordenService->findByFolio($folio);
    }
    public function completed($id)
    {
        return $this->ordenService->completed($id);
    }

    public function cancel($id)
    {
        return $this->ordenService->cancel($id);
    }

    public function ready($id)
    {
        return $this->ordenService->ready($id);
    }

    public function processing($id)
    {
        return $this->ordenService->processing($id);
    }

    public function changeEstatus(OrdenSalidaDestajosEstatusRequest $request)
    {
        return $this->ordenService->changeEstatus($request);
    }


    public function ticketDestajoUbiacion(OrdenSalidaDestajosEstatusRequest $request)
    {
        return $this->ordenService->ticketDestajoUbiacion($request);
    }

    public function insumosPorUbicaciones(InsumosUbicacionReportRequest $request)
    {
        return $this->ordenService->insumosPorUbicaciones($request);
    }

}
