<?php

namespace App\Http\Controllers;

use App\Exports\NominaExport;
use App\Http\Requests\NominaReportRequest;
use App\Http\Requests\NominaRequest;
use App\Services\NominaServiceInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NominaController extends Controller
{
    protected $nominas;

    public function __construct(NominaServiceInterface $nominas) {
        $this->nominas = $nominas;
    }

    public function index() {
        return $this->nominas->index();
    }

    public function create(NominaRequest $request)
    {
        return $this->nominas->create($request);
    }

    public function destroy($id)
    {
        return $this->nominas->destroy($id);
    }


    public function nomina(int $fracc_id)
    {
        return $this->nominas->nomina($fracc_id);
    }

    public function find($id)
    {
        return $this->nominas->find($id);
    }


    public function report(NominaReportRequest $request)
    {
        return $this->nominas->report($request);
    }

}
