<?php

namespace App\Http\Controllers;

use App\Http\Requests\CostoDestajoRequest;
use App\Http\Requests\DestajoRequest;
use App\Http\Requests\DetalleDestajoRequest;
use App\Models\DetalleDestajo;
use App\Services\DestajoServiceInterface;
use Illuminate\Http\Request;

class DestajoController extends Controller
{
    protected $destajos;
    public function __construct(DestajoServiceInterface $destajos)
    {
        $this->destajos = $destajos;
    }
    public function index()
    {
        return $this->destajos->index();
    }
    public function show($id)
    {
        return $this->destajos->show($id);
    }

    public function getDetalleById($detalle_id)
    {
        return $this->destajos->getDetalleById($detalle_id);
    }

    public function create(DestajoRequest $request)
    {
        return $this->destajos->create($request);
    }
    // public function update(DestajoRequest $request)
    // {
    //     return $this->destajos->update($request);
    // }
    public function editDetalle(DetalleDestajoRequest $request)
    {
        return $this->destajos->editDetalle($request);
    }
    public function destroy($id)
    {
        return $this->destajos->destroy($id);
    }

    public function tipos()
    {
        return $this->destajos->getTipos();
    }

    public function destajosByUbicacion(Request $request)
    {
        return $this->destajos->destajosByUbicacion($request);
    }
    public function destajosBySubcontrato(Request $request)
    {
        return $this->destajos->destajosBySubcontrato($request);
    }

    public function costoDestajoByUbicacion(CostoDestajoRequest $request)
    {
        return $this->destajos->costoDestajoByUbicacion($request);
    }
    public function searchByName(Request $request)
    {
        return $this->destajos->searchByName($request);
    }
}
