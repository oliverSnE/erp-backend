<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequest;
use App\Services\MailServiceInterface;

class MailController extends Controller
{
    protected $mail;
    public function __construct(MailServiceInterface $mail)
    {
        $this->mail = $mail;
    }
    public function notificacion(MailRequest $request)
    {

        return $this->mail->notificacion($request);

    //   $data = [
    //        'subject' => $request['subject'],
    //        'link' => $request['link'],
    //        'to' => $request['to'],
    //        'content' => $request['content']
    //   ];

    //   foreach ($data['to'] as $para) {
    //       Mail::to($para)->queue(new NotificacionEmail($data));

    //   }

    //   return response()->json(['status' => 200, 'message' => 'Envío exitoso']);
     }
}
