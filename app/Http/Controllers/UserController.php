<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserChangeUbicacionesRequest;
use Illuminate\Http\Request;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\UserRepositoryInterface;
use App\Service\UserServiceInterface;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    public function authenticate(Request $request)
    {
        return $this->userService->authenticate($request);
    }

    public function register(UserRegisterRequest $request)
    {
        return $this->userService->register($request);
    }

    public function user()
    {
        return $this->userService->user();
    }

    public function userById($id)
    {
        return $this->userService->userById($id);
    }

    public function destroy($id)
    {
        return $this->userService->destroy($id);
    }

    public function users()
    {
        return $this->userService->users();
    }

    public function update(UserRegisterRequest $request)
    {
        return $this->userService->update($request);
    }

    public function roles()
    {
        return $this->userService->roles();
    }

    public function logout()
    {
        return $this->userService->logout();
    }

    public function refresh()
    {
        return $this->userService->refresh();
    }

    public function changeUbicaciones(UserChangeUbicacionesRequest $request)
    {
        return $this->userService->changeUbicaciones($request);
    }

    public function ubicacionesDisponibles(int $user_id)
    {
        return $this->userService->ubicacionesDisponibles($user_id);
    }

}
