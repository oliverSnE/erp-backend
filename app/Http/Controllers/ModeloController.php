<?php

namespace App\Http\Controllers;

use App\Http\Requests\ModeloRequest;
use App\Http\Requests\ModeloImportRequest;
use App\Services\ModeloServiceInterface;
use App\Imports\DestajosInsumosImport;
use App\Models\Destajo;
use App\Models\DetalleDestajo;
use App\Models\Familia;
use App\Models\Insumo;
use App\Models\Modelo;
use App\Models\Unidade;
use Helper\Unit;
use Maatwebsite\Excel\Facades\Excel;
class ModeloController extends Controller
{
    protected $modelos;
    public function __construct(ModeloServiceInterface $modelos)
    {
        $this->modelos = $modelos;
    }

    public function index()
    {
        return $this->modelos->index();
    }

    public function show($id)
    {
        return $this->modelos->show($id);
    }

    public function store(ModeloRequest $request)
    {
        return $this->modelos->store($request);
    }

    public function update(ModeloRequest $request)
    {
        return $this->modelos->update($request);
    }

    public function destroy($id)
    {
        return $this->modelos->destroy($id);
    }

    public function import(ModeloImportRequest $request)
    {
        return $this->modelos->import($request);
    }
}
