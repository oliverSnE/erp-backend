<?php

namespace App\Http\Controllers;

use App\Services\FamiliaServiceInterface;
use Illuminate\Http\Request;

class FamiliaController extends Controller
{
    protected $familiaService;

    public function __construct(FamiliaServiceInterface $familiaService)
    {
        $this->familiaService = $familiaService;
    }

    public function index()
    {
        return $this->familiaService->index();
    }
}
