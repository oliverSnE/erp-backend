<?php

namespace App\Http\Controllers;

use App\Http\Requests\EtapaRequest;
use App\Http\Requests\FraccionamientoRequest;
use App\Repositories\EtapaRepositoryInterface;
use App\Repositories\FraccionamientoRepositoryInterface;
use App\Services\FraccionamientoServiceInterface;

class FraccionamientoController extends Controller
{

    protected $fraccionamientoService;

    public function __construct(FraccionamientoServiceInterface $fraccionamientoService)
    {
        $this->fraccionamientoService = $fraccionamientoService;
    }

    public function all()
    {
        return $this->fraccionamientoService->all();
    }


    public function create(FraccionamientoRequest $request)
    {
        return $this->fraccionamientoService->create($request);
    }

    public function update(FraccionamientoRequest $request)
    {
        return $this->fraccionamientoService->update($request);
    }

    public function find(int $id)
    {
        return $this->fraccionamientoService->find($id);
    }

    public function etapasModelos(int $id)
    {
        return $this->fraccionamientoService->etapasModelos($id);
    }

}
