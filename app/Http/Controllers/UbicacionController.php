<?php

namespace App\Http\Controllers;

use App\Exports\AvanceUbicacionExport;
use App\Http\Requests\AvanceUbicacionReportRequest;
use App\Http\Requests\UbicacionDestajosRequest;
use App\Http\Requests\UbicacionEmpleadoDestajoRequest;
use App\Http\Requests\UbicacionMultipleRequest;
use App\Http\Requests\UbicacionRequest;
use App\Models\Destajo;
use App\Models\Proveedores;
use App\Models\Ubicacion;
use App\Services\UbicacionServiceInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UbicacionController extends Controller
{
    protected $ubicacionService;

    public function __construct(UbicacionServiceInterface $ubicacionService)
    {
        $this->ubicacionService = $ubicacionService;
    }

    public function create(UbicacionRequest $request)
    {
       return $this->ubicacionService->create($request);
    }

    public function createMultiple(UbicacionMultipleRequest $request)
    {

        return $this->ubicacionService->createMultiple($request);
    }

    public function agregarDestajo(UbicacionDestajosRequest $request)
    {
        return $this->ubicacionService->agregarDestajo($request);
    }

    public function eliminarDestajo(UbicacionDestajosRequest $request)
    {
        return $this->ubicacionService->eliminarDestajo($request);
    }

    public function update(UbicacionRequest $request)
    {
        return $this->ubicacionService->update($request);
    }

    public function find(int $id)
    {
       return $this->ubicacionService->find($id);
    }


    public function progresoDeObra(int $ubicacion_id)
    {
        return $this->ubicacionService->progresoDeObra($ubicacion_id);
    }

    public function compras(int $ubicacion_id)
    {
        return $this->ubicacionService->compras($ubicacion_id);
    }

    public function delete(int $ubicacion_id)
    {
        return $this->ubicacionService->delete($ubicacion_id);
    }

    public function avanceUbicaciones(AvanceUbicacionReportRequest $request)
    {
        return $this->ubicacionService->avanceUbicaciones($request);
    }

    public function ubicacionesByFracc(int $fracc_id)
    {
        return $this->ubicacionService->ubicacionesByFracc($fracc_id);
    }

    public function empeladosDestajo(UbicacionEmpleadoDestajoRequest $request)
    {
        return $this->ubicacionService->empeladosDestajo($request);
    }

}
