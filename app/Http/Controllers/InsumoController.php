<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsumoRegisterRequest;
use App\Services\InsumosServiceInterface;

class InsumoController extends Controller
{
    protected $insumos;
    public function __construct(InsumosServiceInterface $insumos)
    {
        $this->insumos = $insumos;
    }
    public function index()
    {
        return $this->insumos->index();
    }
    public function show($id)
    {
         return $this->insumos->show($id);
    }
    public function store(InsumoRegisterRequest $request)
    {
       return $this->insumos->store($request);

    }
    public function update(InsumoRegisterRequest $request)
    {
      return $this->insumos->update($request);
    }
    public function destroy($id)
    {
        return $this->insumos->destroy($id);
    }

}
