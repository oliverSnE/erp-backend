<?php

namespace App\Http\Controllers;

use App\Models\Basic\Destajo;
use App\Models\DetalleNomina;
use App\Models\Quema;
use App\Models\QuemaDestajos;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class QuemaController extends Controller
{
    public function create(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $quema = Quema::create([
            'user_id' => $user->id,
            'fraccionamiento_id' => $request['fraccionamiento_id']
        ]);

        $detallesDestajos = $request->get('destajos');
        foreach ($detallesDestajos as $detalleDestajo) {
            $pagoActual = QuemaDestajos::where('ubicacion_id', '=', $detalleDestajo["ubicacion_id"])
                ->where('destajo_id', '=', $detalleDestajo['destajo_id'])
                ->get()->sum('costo');
            $destajo = Destajo::find($detalleDestajo['destajo_id']);
            $diferencia = round(($destajo->costo * $destajo->cantidad), 3) - ($pagoActual + $detalleDestajo["pago"]);

            $quema->detalleDestajos()->create([
               'ubicacion_id'   => $detalleDestajo["ubicacion_id"],
               'destajo_id'     => $detalleDestajo["destajo_id"],
               'costo'          => $diferencia < 1 ? $detalleDestajo["pago"] + $diferencia :  $detalleDestajo["pago"]
            ]);
        }

        $detallesNomina = $request->get('empleados');
        foreach ($detallesNomina as $detalleNomina) {
            $quema->detalleNomina()->create([
                'empleado_id'   => $detalleNomina["empleado_id"],
                'is_jefe'       => $detalleNomina["es_jefe"],
                'pago'          => $detalleNomina["pago"]
            ]);
        }

        $quema = Quema::with('detalleDestajos.destajo','detalleDestajos.ubicacion', 'detalleNomina.empleado')->find($quema->id);
        return response()->json(['data' =>compact('quema')],201);
    }
    public function createSubcontrato(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $quema = Quema::create([
            'user_id' => $user->id,
            'tipo' => 'subcontrato',
            'fraccionamiento_id' => $request['fraccionamiento_id']
        ]);

        $detallesDestajos = $request->get('destajos');

        foreach ($detallesDestajos as $detalleDestajo) {
            $destajo = Destajo::find($detalleDestajo['destajo_id']);
            $quema->detalleDestajos()->create([
               'ubicacion_id'   => $detalleDestajo['ubicacion_id'],
               'destajo_id'     => $detalleDestajo['destajo_id'],
               'costo'          => round($destajo->cantidad * $destajo->costo, 2)
           ]);
        }

        $quema = Quema::with('detalleDestajos.destajo','detalleDestajos.ubicacion')->find($quema->id);
        return response()->json(['data' =>compact('quema')],201);
    }

    public function destroy(int $quema_id)
    {
        $message = 'La quema ya se encuetra pagada en nomina';
        if(!DetalleNomina::where('quema_id', '=', $quema_id)->exists())
        {
            $quema = Quema::find($quema_id);
            if($quema)
            {
                $quema->detalleDestajos()->delete();
                $quema->detalleNomina()->delete();
                $quema->delete();

                return response()->json(['data' => [
                    'code' => 200,
                    'message' => 'Eliminado correctamente'
                ]], 200);

            }

            $message = 'La quema no existe';
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                [
                    'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => $message
                ]
            ]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function verificaciones(int $fracc_id)
    {
        $quemasQuery = Quema::has('pendientesDetalleDestajos')
                ->has('detalleNomina')
                ->where('tipo','destajo')
                ->where('fraccionamiento_id', $fracc_id)
                ->with('user','pendientesDetalleDestajos.destajo.unidad', 'pendientesDetalleDestajos.ubicacion', 'detalleNomina.empleado')
                ->orderBy('quemas.id')
                ->get();
        $quemas = [];
        foreach ($quemasQuery as $quema) {
            $ubicaciones = [];
            foreach ($quema->pendientesDetalleDestajos as $detalleDestajo) {
                if(!array_key_exists($detalleDestajo->ubicacion_id, $ubicaciones))
                {
                    $ubicaciones[$detalleDestajo->ubicacion_id] = $detalleDestajo->ubicacion->toArray();
                    $ubicaciones[$detalleDestajo->ubicacion_id]['detalle'] = [$detalleDestajo->makeHidden('ubicacion')->toArray()];
                }else {
                    $ubicaciones[$detalleDestajo->ubicacion_id]['detalle'][] = $detalleDestajo->makeHidden('ubicacion')->toArray();
                }
            }
            $quema->makeHidden('pendientesDetalleDestajos');
            $quemaArray =  array_merge( $quema->toArray(), ["detalle" => array_values($ubicaciones)]);
            $quemas[] = $quemaArray;
        }



        return response()->json(['data' =>compact('quemas')],201);
    }
    public function verificacionesSubcontratos(int $fracc_id)
    {
        $quemasQuery = Quema::has('pendientesDetalleDestajos')
                ->where('tipo','subcontrato')
                ->where('fraccionamiento_id', $fracc_id)
                ->with('user','pendientesDetalleDestajos.destajo.unidad', 'pendientesDetalleDestajos.ubicacion')
                ->orderBy('quemas.id')
                ->get();
        $quemas = [];
        foreach ($quemasQuery as $quema) {
            $ubicaciones = [];
            foreach ($quema->pendientesDetalleDestajos as $detalleDestajo) {
                if(!array_key_exists($detalleDestajo->ubicacion_id, $ubicaciones))
                {
                    $ubicaciones[$detalleDestajo->ubicacion_id] = $detalleDestajo->ubicacion->toArray();
                    $ubicaciones[$detalleDestajo->ubicacion_id]['detalle'] = [$detalleDestajo->makeHidden('ubicacion')->toArray()];
                }else {
                    $ubicaciones[$detalleDestajo->ubicacion_id]['detalle'][] = $detalleDestajo->makeHidden('ubicacion')->toArray();
                }
            }
            $quema->makeHidden('pendientesDetalleDestajos');
            $quemaArray =  array_merge( $quema->toArray(), ["detalle" => array_values($ubicaciones)]);
            $quemas[] = $quemaArray;
        }



        return response()->json(['data' =>compact('quemas')],201);
    }

    public function aprobar(int $qDestajo_id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $quemaDestajo =  QuemaDestajos::where('id', $qDestajo_id)->first();
        if($quemaDestajo->data === null){
            $quemaDestajo->update([
                'estatus' => 'aprobada',
                'data'    => [
                    'estatus_aprobada' => [
                        "user_id" => $user->id,
                        "update_at" => now()
                    ]
                ]
            ]);
            return $quemaDestajo;
        }
        $data = $quemaDestajo->data;
        $data['estatus_aprobada'] = [
            "user_id" => $user->id,
            "update_at" => now()
        ];
        $quemaDestajo->update([
            'estatus' => 'aprobada',
            'data'    => $data
            ]);

    return $quemaDestajo;
    }
}
