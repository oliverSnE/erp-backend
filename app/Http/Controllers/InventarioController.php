<?php

namespace App\Http\Controllers;

use App\Services\InventarioServiceInterface;

class InventarioController extends Controller
{
    protected $inventario;
    public function __construct(InventarioServiceInterface $inventario)
    {
        $this->inventario = $inventario;
    }

    public function inventario($fracc_id)
    {
        return $this->inventario->inventario($fracc_id);
    }

    public function inventarioReport($fracc_id)
    {
        return $this->inventario->inventarioReport($fracc_id);
    }
}
