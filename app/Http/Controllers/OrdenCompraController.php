<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrdenCompraAddFacturaRequest;
use App\Http\Requests\OrdenCompraCreateInsumosRequest;
use App\Http\Requests\OrdenCompraCreateRequest;
use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\OrdenCompraReOrdenRequest;
use App\Http\Requests\OrdenCompraSubcontratosInsumosRequest;
use App\Http\Requests\OrdenCompraUpdateRequest;
use App\Http\Requests\UbicacionByModeloEtapaRequest;
use App\Services\OrdenCompraServiceInterface;
use Illuminate\Http\Request;
class OrdenCompraController extends Controller
{

    protected $ordenService;

    public function __construct(OrdenCompraServiceInterface $ordenService)
    {
        $this->ordenService = $ordenService;
    }

    public function create(OrdenCompraCreateRequest $request)
    {
        return $this->ordenService->create($request);
    }

    public function createOnlyInsumos(OrdenCompraCreateInsumosRequest $request)
    {
        return $this->ordenService->createOnlyInsumos($request);
    }

    public function updateDetalle(OrdenCompraUpdateRequest $request)
    {
        return $this->ordenService->updateDetalle($request);
    }

    public function aprove($id)
    {
        return $this->ordenService->aprove($id);
    }

    public function cancel($id)
    {
        return $this->ordenService->cancel($id);
    }

    public function completed($id)
    {
        return $this->ordenService->completed($id);
    }

    public function find($id)
    {
        return $this->ordenService->find($id);
    }

    public function findFolio($folio)
    {
        return $this->ordenService->findByFolio($folio);
    }

    public function findAll()
    {
        return $this->ordenService->findAll();
    }

    public function findAllByFraccionamiento(int $fraccionamientoId)
    {
        return $this->ordenService->findAllByFraccionamiento($fraccionamientoId);
    }

    public function pdf($id)
    {
        return  $this->ordenService->pdf($id);
    }

    public function verificacion($id)
    {
        return  $this->ordenService->verificacion($id);
    }

    public function ubicacion(UbicacionByModeloEtapaRequest $request)
    {
        return $this->ordenService->ubicacion($request);
    }

    public function destajos(OrdenCompraDestajosRequest $request)
    {
        return $this->ordenService->destajos($request);
    }

    public function subcontratos(OrdenCompraDestajosRequest $request)
    {
        return $this->ordenService->subcontratos($request);
    }

    public function subcontratosInsumos(OrdenCompraSubcontratosInsumosRequest $request)
    {
        return $this->ordenService->subcontratosInsumos($request);
    }

    public function addFactura(OrdenCompraAddFacturaRequest $request)
    {
        return $this->ordenService->addFactura($request);
    }

    public function getSpecialOrders(int $model_id)
    {
        return $this->ordenService->getSpecialOrders($model_id);
    }

    public function facturas(int $id)
    {
        return $this->ordenService->facturas($id);
    }

    public function downloadFactura(int $factura_id)
    {
        return $this->ordenService->downloadFactura($factura_id);
    }

    public function reOrden(OrdenCompraReOrdenRequest $request)
    {
        return $this->ordenService->reOrden($request);
    }

}
