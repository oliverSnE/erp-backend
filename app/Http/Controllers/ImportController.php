<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ImportServiceInterface;

// use App\Imports\InsumosImport;
// use Maatwebsite\Excel\Facades\Excel;
// use Maatwebsite\Excel\HeadingRowImport;

class ImportController extends Controller
{
    protected $importService;

    public function __construct(ImportServiceInterface $importService) {
        $this->importService = $importService;
    } 

    public function import(Request $request) {
        return $this->importService->import($request);
    }

    public function onError(\Throwable $e) {
        return $this->importService->onError($e);
    }
}
/*
    
    public function import(Request $request)
    {

        if ($request->hasFile('file')) {
            $this->validate($request, [
                'file' => 'required|file|mimes:xls,xlsx,csv|max:10240', //max 10Mb
            ]);

            //return back()->with(['status' =>'1']);
            try {
               Excel::import(new InsumosImport, request()->file('file'));
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                foreach ($failures as $failure) {
                    $failure->row(); // row that went wrong
                    $failure->attribute(); // either heading key (if using heading row concern) or column index
                }
                return response()->json(['data' => [
                    'code' => 417,
                    'message' => 'No fue posible importar el archivo'
                ]], 417); // Expectation Failed
            }
            return response()->json(['data' => [
                'code' => 201,
                'message' => 'Importación exitosa'
            ]], 201);   //200
        } else {
            //return back()->with(['error' => '0']);
            return response()->json(['data' => [
                'code' => 417,
                'message' => 'Seleccione un archivo excel para importar'
            ]], 417); // Expectation Failed
        }
    }

    public function onError(\Throwable $e)
    {
        throw $e;
    }
}
*/