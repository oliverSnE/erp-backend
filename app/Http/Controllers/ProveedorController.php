<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProveedorRequest;
use App\Services\ProveedorServiceInterface;

class ProveedorController extends Controller
{
    protected $proveedor;
    public function __construct(ProveedorServiceInterface $proveedor)
    {
        $this->proveedor = $proveedor;
    }
    public function index()
    {
        return $this->proveedor->index();
    }
    public function find($id)
    {
         return $this->proveedor->find($id);
    }
    public function store(ProveedorRequest $request)
    {
       return $this->proveedor->store($request);

    }
    public function update(ProveedorRequest $request)
    {
      return $this->proveedor->update($request);
    }
    public function destroy($id)
    {
        return $this->proveedor->destroy($id);
    }
}
