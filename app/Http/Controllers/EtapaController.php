<?php

namespace App\Http\Controllers;

use App\Http\Requests\EtapaRequest;
use App\Services\EtapaServiceInterface;


class EtapaController extends Controller
{
    protected $etapaService;

    public function __construct(EtapaServiceInterface $etapaService)
    {
        $this->etapaService = $etapaService;
    }

    public function create(EtapaRequest $request)
    {
        return $this->etapaService->create($request);
    }

    public function update(EtapaRequest $request)
    {
        return $this->etapaService->update($request);
    }

    public function find(int $id)
    {
        return $this->etapaService->find($id);
    }

    public function destroy(int $id)
    {
        return $this->etapaService->destroy($id);
    }

}
