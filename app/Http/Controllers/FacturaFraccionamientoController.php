<?php

namespace App\Http\Controllers;

use App\Http\Requests\FacturaFraccionamientoRequest;
use App\Services\FacturaFraccionamientoServiceInterface;
use Illuminate\Http\Request;

class FacturaFraccionamientoController extends Controller
{
    protected $fraccionamiento;

    public function __construct(FacturaFraccionamientoServiceInterface $fraccionamiento) {
        $this->fraccionamiento = $fraccionamiento;
    }

    public function fraccionamientoReport(FacturaFraccionamientoRequest $request)
    {
        return $this->fraccionamiento->fraccionamientoReport($request);
    }
}
