<?php

namespace App\Http\Controllers;

use App\Http\Requests\CuadrillaRequest;
use App\Http\Requests\CuadrillaImportRequest;
use App\Services\CuadrillaServiceInterface;

class CuadrillaController extends Controller
{
    protected $cuadrillaService;

    public function __construct(CuadrillaServiceInterface $cuadrillaService)
    {
        $this->cuadrillaService = $cuadrillaService;
    }

    public function create(CuadrillaRequest $request)
    {
        return $this->cuadrillaService->create($request);
    }

    public function update(CuadrillaRequest $request)
    {
        return $this->cuadrillaService->update($request);
    }

    public function find($id)
    {
        return $this->cuadrillaService->findById($id);
    }

    public function all()
    {
        return $this->cuadrillaService->Findall();
    }

    public function destroy($id)
    {
        return $this->cuadrillaService->destroy($id);
    }

    public function import(CuadrillaImportRequest $request) 
    {
        return $this->cuadrillaService->import($request);
    }
}
