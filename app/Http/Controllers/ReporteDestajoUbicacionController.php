<?php

namespace App\Http\Controllers;

use App\Http\Requests\AvanceUbicacionReportRequest;
use App\Services\ReporteDestajoUbicacionServiceInterface;
use Illuminate\Http\Request;

class ReporteDestajoUbicacionController extends Controller
{
    protected $ubicacion;

    public function __construct(ReporteDestajoUbicacionServiceInterface $ubicacion) {
        $this->ubicacion = $ubicacion;
    }

    public function destajoUbicacionReport(AvanceUbicacionReportRequest $request)
    {
        return $this->ubicacion->destajoUbicacionReport($request);
    }
}
