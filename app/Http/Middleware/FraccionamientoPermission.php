<?php

namespace App\Http\Middleware;

use App\Models\Etapa;
use App\Models\Ubicacion;
use App\Models\User;
use Closure;
use JWTAuth;

class FraccionamientoPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permision, $routeName = null)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if($user->rol === "admin"){
            return $next($request);
        }
        $dbUser = User::with('fraccionamientos')->where('id', $user->id)->first();;
        $isValid = true;
        switch($permision){
            case 'orden_compra':
                break;
            case 'ubicacion':
                $isValid = $this->handleUbicacion($dbUser, $request, $routeName);
                break;
            case 'etapa':
                $isValid = $this->handleEtapa($dbUser, $request, $routeName);
                break;
            case 'fraccionamiento':
                $isValid = $this->handleFraccionamiento($dbUser, $request);
                break;
        }

        if($isValid){
            return $next($request);
        }

        return response()->json(['error' => [
            'message' => "No se cuentan con los permisos necesarios para acceder a este modulo.",
            'code'    => 403
            ]
        ],   403);
    }

    private function handleFraccionamiento($user, $request): bool
    {
        foreach ($user->fraccionamientos as  $fracc) {
            if($fracc->id == $request->id){
                return true;
            }
        }

        return false;
    }

    private function handleEtapa($user, $request, $routeName): bool
    {
        switch($routeName){
            case 'create':
                foreach ($user->fraccionamientos as  $fracc) {
                    if($fracc->id == $request->get('fraccionamiento_id')){
                        return true;
                    }
                }
                break;
            case 'update':
                $etapa = Etapa::with('fraccionamiento')->where('id', $request->get('id'))->first();
                foreach ($user->fraccionamientos as  $fracc) {
                    if($etapa->fraccionamiento->id == $fracc->id){
                        return true;
                    }
                }
                break;
            case 'get':
                $etapa = Etapa::where('id',  $request->id)->first();
                foreach ($user->fraccionamientos as  $fracc) {
                    if($etapa->fraccionamiento_id == $fracc->id){
                        return true;
                    }
                }
                break;
        }



        return false;
    }

    private function handleUbicacion($user, $request, $routeName): bool
    {
        switch($routeName){
            case 'create':
                $etapa = Etapa::with('fraccionamiento')->where('id', $request->get('etapa_id'))->first();
                foreach ($user->fraccionamientos as  $fracc) {
                    if($fracc->id == $etapa->fraccionamiento->id){
                        return true;
                    }
                }
                break;
            case 'update':
                $ubicacion = Ubicacion::with('etapa.fraccionamiento')->where('id', $request->get('id'))->first();
                foreach ($user->fraccionamientos as  $fracc) {
                    if($ubicacion->etapa->fraccionamiento->id == $fracc->id){
                        return true;
                    }
                }
                break;
            case 'get':
                $ubicacion = Ubicacion::with('etapa.fraccionamiento')->where('id', $request->id)->first();
                foreach ($user->fraccionamientos as  $fracc) {
                    if($ubicacion->etapa->fraccionamiento->id == $fracc->id){
                        return true;
                    }
                }
                break;
        }

        return false;
    }
}
