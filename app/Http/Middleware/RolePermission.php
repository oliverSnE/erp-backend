<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class RolePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $user = JWTAuth::parseToken()->authenticate();

        foreach ($roles as $rol) {
            if($user->rol === $rol){
                return $next($request);
            }
        }

        return response()->json(['error' => [
            'message' => "El usuario no cuenta con permisos para realizar esta accion.",
            'code'    => 403
            ]
        ],   403);
    }
}
