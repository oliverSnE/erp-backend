<?php

namespace App\Exports;

use App\Models\Fraccionamiento;
use App\Models\Quema;
use App\Models\QuemaDestajos;
use App\Models\QuemaNomina;
use App\Models\Nomina;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithTitle;


class NominaExport  implements FromView, WithTitle
{
    protected $fracc_id;
    protected $nomina_id;

    public function __construct(int $nomina_id, int $fracc_id)
    {
        $this->nomina_id = $nomina_id;
        $this->fracc_id = $fracc_id;
    }


    public function view(): View
    {
        $fraccionamiento = Fraccionamiento::find($this->fracc_id);
        $nomina = Nomina::find($this->nomina_id);
        $quema_id = [];
        foreach($nomina->detalles as $detalle) {

            $quema_id[] = $detalle->quema_id;
        }

        $quemas = Quema::with('user')->whereIn('id', $quema_id)->get();
        $empleados = QuemaNomina::with('empleado')->whereIn('quema_id', $quema_id)->groupBy('empleado_id')->select(DB::raw('sum(pago) as pago, empleado_id'))->get();
        $destajos = QuemaDestajos::with('destajo', 'ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id', 'destajo_id')->select(DB::raw('sum(costo) as pago, ubicacion_id, destajo_id'))->orderBy('ubicacion_id', 'ASC')->get();
        $totalesUbicacion = QuemaDestajos::with('ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id')->select(DB::raw('sum(costo) as pago, ubicacion_id'))->orderBy('ubicacion_id', 'ASC')->get();

        $totalEmpleado = $empleados->sum('pago');

        return view('excel.nomina', [
            'fraccionamiento'   => $fraccionamiento,
            'nomina'            => $nomina,
            'quemas'            => $quemas,
            'empleados'         => $empleados,
            'totalEmpleados'    => $totalEmpleado,
            'totalesUbicacion'  => $totalesUbicacion,
            'destajos'          => $destajos
        ]);
    }

    public function title(): string
    {
        $nomina = Nomina::find($this->nomina_id);
        return $nomina->nombre;
    }
}

