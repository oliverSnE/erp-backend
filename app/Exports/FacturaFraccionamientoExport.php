<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class FacturaFraccionamientoExport implements FromView, WithTitle, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $fraccionamiento;

    public function __construct($fraccionamiento) {
        $this->fraccionamiento = $fraccionamiento;
    }

    public function view(): View
    {
        $frac = $this->fraccionamiento;

        $factura = DB::select(
            "SELECT
            orden_compras.id,
            orden_compra_facturas.fecha,
            orden_compras.folio,
            proveedores.nombre AS proveedor,
            orden_compra_facturas.importe,
            orden_compra_facturas.folio AS no_factura,
            orden_compra_facturas.importe /
                (SELECT count(*)
                FROM
                    orden_compra_facturas AS subtotal
                WHERE
                    subtotal.orden_compra_id = orden_compra_facturas.orden_compra_id
                GROUP BY
                    orden_compra_id) AS importe_ubicacion,
            (select string_agg(concat(ubicacion.manzana, '/', ubicacion.numero)::varchar, ', ') from orden_compra_ubicacion ocu
            inner join ubicaciones as ubicacion
            on ubicacion.id = ocu.ubicacion_id where ocu.orden_compra_id = orden_compra_facturas.orden_compra_id ) as ubicaciones
        FROM
            orden_compra_facturas
        inner JOIN
            orden_compras ON orden_compras.fraccionamiento_id =".$frac."
        INNER JOIN
            proveedores ON orden_compras.proveedor_id = proveedores.id
        WHERE
            orden_compra_facturas.orden_compra_id = orden_compras.id ORDER BY folio ASC");


        return view('excel.facturaFraccionamiento', [
            'facturas' => $factura
        ]);


    }
    public function title() : string
    {
        return 'Factura por fracciomamiento';
    }
}
