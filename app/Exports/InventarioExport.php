<?php

namespace App\Exports;

use App\Models\Fraccionamiento;
use App\Models\Insumo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithTitle;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class InventarioExport implements FromView, WithTitle /*, WithDrawings*/
{

    protected $fracc_id;

    public function __construct(int $fracc_id)
    {
        $this->fracc_id = $fracc_id;
    }

    public function view(): View
    {
        $fraccionamiento = Fraccionamiento::find($this->fracc_id);
        $insumos = Insumo::with('Unidades', 'Familias')
        ->from( 'insumos' )
        ->select(DB::raw("
            insumos.*,
                            coalesce((
                                select coalesce(SUM(doc.cantidad), 0) as cantidad_pedida
                                from insumos as i1
                                left join orden_compras as oc on oc.fraccionamiento_id = ".$this->fracc_id."
                                left join detalle_orden_compras as doc on doc.insumo_id = i1.id and doc.orden_compra_id = oc.id
                                where i1.tipo = 'normal' and i1.id = insumos.id
                                group by i1.id
                            ), 0) as cantidad_pedida,
                            coalesce((
                                select coalesce(SUM(de.cantidad), 0) as cantidad_entregada
                                from orden_compras as oc
                                join detalle_orden_compras as doc on doc.orden_compra_id = oc.id
                                left join detalle_entradas as de on de.detalle_orden_compra_id = doc.id
                                join insumos as i2 on i2.id = doc.insumo_id
                                where oc.fraccionamiento_id = ".$this->fracc_id." and i2.id = insumos.id
                                group by i2.id
                                order by i2.id
                            ),0) as cantidad_recibida,
                            coalesce((
                                select coalesce(SUM(dos.cantidad), 0) as cantidad_pendiente_de_entregada
                                from orden_salidas as os
                                join detalle_orden_salidas as dos on dos.orden_salida_id = os.id and dos.estatus = 'creada'
                                join detalle_destajos as dd on dd.id = dos.detalle_destajo_id
                                join insumos as i3 on i3.id = dd.insumo_id
                                where os.fraccionamiento_id = ".$this->fracc_id." and i3.id = insumos.id
                                group by i3.id
                                order by i3.id
                            ),0) as cantidad_pendiente_en_obra,
                                coalesce((
                                select coalesce(SUM(dos.cantidad), 0) as cantidad_entregada
                                from orden_salidas as os
                                join detalle_orden_salidas as dos on dos.orden_salida_id = os.id and dos.estatus = 'recibida'
                                join detalle_destajos as dd on dd.id = dos.detalle_destajo_id
                                join insumos as i4 on i4.id = dd.insumo_id
                                where os.fraccionamiento_id = ".$this->fracc_id." and i4.id = insumos.id
                                group by i4.id
                                order by i4.id
                            ),0) as cantidad_entregada
        "))
        ->where('insumos.tipo', '=', DB::raw("'normal'"))->orderby('insumos.id', 'ASC')->get();
        return view('excel.inventario', [
            'insumos'           => $insumos,
            'fraccionamiento'   => $fraccionamiento
        ]);
    }

    public function title(): string
    {
        return 'Inventario';
    }
}
