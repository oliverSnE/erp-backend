<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;


class FacturaUbicacionExport implements FromView, WithTitle, ShouldAutoSize
{
    use Exportable;
    protected $ubicacion_id;

    public function __construct($ubicacion_id) {
        $this->ubicacion_id = $ubicacion_id;
    }
    public function view(): View
    {
        $ubicaciones = $this->ubicacion_id;

        $arrayfacturas = [];

        foreach ($ubicaciones as $ubcicacion) {

            $facturas = DB::select(DB::raw(
                "SELECT
                    orden_compra_facturas.orden_compra_id,
                    orden_compra_ubicacion.ubicacion_id,
                    ubicaciones.numero,
                    ubicaciones.manzana,
                    orden_compra_facturas.fecha,
                    proveedores.nombre,
                    orden_compra_facturas.folio,
                    orden_compra_facturas.importe,
                    orden_compra_facturas.importe / (
                        SELECT count (*)
                        FROM orden_compra_facturas AS subtotal
                        WHERE subtotal.orden_compra_id = orden_compra_facturas.orden_compra_id
                        GROUP by orden_compra_id) * 1.16 AS subtotal,
                    orden_compras.id
                FROM
                    ubicaciones
                INNER JOIN orden_compra_ubicacion
                    ON ubicaciones.id = orden_compra_ubicacion.ubicacion_id
                INNER JOIN orden_compras
                    ON orden_compra_ubicacion.orden_compra_id = orden_compras.id
                INNER JOIN proveedores
                    ON orden_compras.proveedor_id = proveedores.id
                INNER JOIN orden_compra_facturas
                    ON orden_compras.id = orden_compra_facturas.orden_compra_id
                WHERE
                    orden_compra_ubicacion.ubicacion_id =".$ubcicacion));

             $arrayfacturas = array_merge($arrayfacturas, $facturas);

        }

        return view('excel.facturaUbicacion', [
            'arrayfacturas' => $arrayfacturas
        ]);
    }

    public function title() : string
    {
        return 'Factura por ubicacion';
    }
}
