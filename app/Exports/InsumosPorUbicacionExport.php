<?php

namespace App\Exports;

use App\Models\DetalleOrdenSalida;
use App\Models\Fraccionamiento;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithTitle;


class InsumosPorUbicacionExport implements FromView, WithTitle
{

    protected $ubicaciones;
    protected $fecha;
    protected $fracc_id;

    public function __construct(array $ubicaciones, string $fecha, int $fracc_id)
    {
        $this->ubicaciones = $ubicaciones;
        $this->fecha = $fecha;
        $this->fracc_id = $fracc_id;
    }


    public function view(): View
    {
        $dataUbicaicones = DetalleOrdenSalida::from('detalle_orden_salidas as dos')
                ->select(
                    'u.manzana',
                    'u.numero',
                    'd.nombre as destajo',
                    'i.nombre as insumo',
                    'un.nombre as unidad',
                    'dos.cantidad',
                    'os.folio',
                    'os.created_at as fecha')
                ->join('ubicaciones as u', 'u.id', '=', 'dos.ubicacion_id')
                ->join('orden_salidas as os', 'os.id', '=', 'dos.orden_salida_id')
                ->join('detalle_destajos as dd', 'dd.id', '=', 'dos.detalle_destajo_id')
                ->join('insumos as i', 'i.id', '=', 'dd.insumo_id')
                ->join('unidades as un', 'un.id', '=', 'i.unidad_id')
                ->join('destajos as d', 'd.id', '=', 'dd.destajo_id')
                ->whereIn('dos.ubicacion_id', $this->ubicaciones)
                ->where('dos.created_at', '<', $this->fecha.' 23:59:59')
                ->where('dos.estatus','=', 'recibida')
                ->orderBy('u.id', 'asc')
                ->orderBy('os.created_at', 'asc')
                ->get();
        $fraccionamiento = Fraccionamiento::find($this->fracc_id);
        return view('excel.insumos-ubicacion', [
            'dataUbicaciones' => $dataUbicaicones,
            'fraccionamiento' => $fraccionamiento,
            'fecha'           => $this->fecha
        ]);
    }

    public function title(): string
    {
        return 'Vales de  salida por ubicacion';
    }
}
