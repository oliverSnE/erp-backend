<?php

namespace App\Exports;

use App\Models\Fraccionamiento;
use App\Models\QuemaDestajos;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class DestajoPorUbicacionExport implements FromView, WithTitle, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $ubicaciones;
    protected $fracc_id;

    public function __construct($ubicaciones, $fracc_id) {
        $this->ubicaciones = $ubicaciones;
        $this->fracc_id = $fracc_id;
    }
    public function view(): View
    {



        $destajoUbicacion = QuemaDestajos::from('quemas_destajos as qd')
        ->select(
            DB::raw('MAX(u.id) as ubicacion_id'),
            DB::raw('MAX(u.manzana) as manzana'),
            DB::raw('MAX(u.numero) as numero'),
            DB::raw('MAX(d.id) as destajo_id'),
            DB::raw('MAX(d.nombre) as destajo'),
            DB::raw('MAX(i.nombre) as insumo'),
            'dd.cantidad as cantidad_presupuestada',
            DB::raw('(select coalesce(SUM(dos.cantidad), 0) from detalle_orden_salidas as dos where dos.detalle_destajo_id = dd.id and dos.ubicacion_id = qd.ubicacion_id) as cantidad_utilizada,
	            SUM(qd.costo) as mano_obra_pagada,
                MAX(d.costo * d.cantidad) as mano_obra_presupuestada'))
        ->join('ubicaciones as u','u.id','=','qd.ubicacion_id')
        ->leftJoin('detalle_destajos as dd', 'dd.destajo_id', '=', 'qd.destajo_id')
        ->leftJoin('insumos as i', 'i.id', '=', 'dd.insumo_id')
        ->join('destajos as d', 'd.id','=', 'qd.destajo_id')
        ->whereIn('qd.ubicacion_id', $this->ubicaciones)
        ->where('qd.estatus', '=', 'aprobada' )
        ->groupBy('qd.ubicacion_id', 'd.id', 'dd.id')
        ->get();

        $fraccionamiento = Fraccionamiento::where('id',$this->fracc_id)->first();

        return view('excel.destajoPorUbicacion', [
            'destajoUbicacion' => $destajoUbicacion,
            'fraccionamiento' => $fraccionamiento
        ]);
    }
    public function title(): string
    {
        return 'Reporte destajos-ubicaciones';
    }
}
