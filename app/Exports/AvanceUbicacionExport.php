<?php

namespace App\Exports;

use App\Models\Fraccionamiento;
use App\Models\QuemaDestajos;
use App\Models\Ubicacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithTitle;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AvanceUbicacionExport implements FromView, WithTitle
{

    protected $ubicaciones;
    protected $fecha;
    protected $fracc_id;

    public function __construct(array $ubicaciones, string $fecha, int $fracc_id)
    {
        $this->ubicaciones = $ubicaciones;
        $this->fecha = $fecha;
        $this->fracc_id = $fracc_id;
    }

    public function view(): View
    {
        $dataUbicacion = [];
        $fraccionamiento = Fraccionamiento::find($this->fracc_id);
        foreach ($this->ubicaciones as $ubicacion_id)
        {
            $ubicacion = Ubicacion::find($ubicacion_id);
            $materialUsado = DB::select(DB::raw("
                        select  coalesce(SUM(round((dos.cantidad *
                            (
                                select AVG(de.cantidad)
                                from detalle_entradas as de
                                join orden_compra_ubicacion as ocu on ocu.ubicacion_id = dos.ubicacion_id
                                join orden_compra_destajo as ocd on ocd.destajo_id = dd.destajo_id
                                join detalle_orden_compras as doc on doc.orden_compra_id = ocd.orden_compra_id and ocu.orden_compra_id = doc.orden_compra_id and doc.insumo_id = 635
                                where de.detalle_orden_compra_id = doc.id
                            ))::numeric, 2)),0) as costo_materiales
                        from detalle_orden_salidas as dos
                        join detalle_destajos as dd on dd.id = dos.detalle_destajo_id
                        where dos.ubicacion_id = ".$ubicacion_id." and dos.estatus = 'recibida' and dos.created_at < '".$this->fecha." 23:59:59'
                "));
            $totalMaterial = $ubicacion->destajos->sum('costo_insumos');

            $quemaDestajos = QuemaDestajos::with('destajo')->select(
                'destajo_id',
                DB::raw('SUM(quemas_destajos.costo) as pagado'),
                DB::raw('MAX(destajos.cantidad * destajos.costo) as total'),
                DB::raw('ROUND((SUM(quemas_destajos.costo)/MAX(destajos.cantidad * destajos.costo)*100)::numeric, 0) as porcentaje'),
                DB::raw("string_agg(quemas_destajos.costo::varchar, ', ') as pagos_parciales"))
            ->join('destajos', 'destajos.id', '=', 'quemas_destajos.destajo_id')
            ->where('quemas_destajos.ubicacion_id', '=', $ubicacion_id)
            ->where('quemas_destajos.created_at', '<', $this->fecha.' 23:59:59')
            ->groupBy('quemas_destajos.destajo_id')
            ->get();

            $destajosId = collect(DB::select(DB::raw("
                select dm.destajo_id
                from detalle_modelos as dm
                join ubicaciones as u on u.id = ".$ubicacion_id."
                where u.modelo_id = dm.modelo_id and dm.destajo_id not in (
                    select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
                    )
                group by dm.destajo_id
                union all
                select destajo_id from detalle_ubicaciones where ubicacion_id = ".$ubicacion_id." and tipo = 'agregado'
                group by destajo_id
            ")))->pluck('destajo_id');

            $costoTotalQuemas = $quemaDestajos->sum('pagado');
            $costoDestajo =  round($ubicacion->destajos->sum(function($t){
                return $t->costo * $t->cantidad;
            }),2);
            $dataUbicacion[] = [
                'ubicacion' => $ubicacion,
                'materialUsado' => $materialUsado[0]->costo_materiales,
                'totalMaterial' => $totalMaterial,
                'porcentajeMaterial' => round($materialUsado[0]->costo_materiales/($totalMaterial < 1 ? 1: $totalMaterial)*100, 2),
                'costoTotalQuemas' => $costoTotalQuemas,
                'totalQuemas' => $costoDestajo,
                'porcentajeQuemas' => round($costoTotalQuemas/($costoDestajo < 1 ? 1: $costoDestajo)*100, 2)

            ];

        }

        return view('excel.avance-ubicacion', [
            'data' => $dataUbicacion,
            'fraccionamiento' => $fraccionamiento,
            'fecha' =>  $this->fecha
        ]);
    }

    public function title(): string
    {
        return 'Avance Ubicacion';
    }
}
