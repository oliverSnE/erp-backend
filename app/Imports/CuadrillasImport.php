<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Http\Requests\CuadrillaImportRequest;
use Carbon\Carbon;
use App\Models\Empleado;
use App\Models\Cuadrilla;
use App\Models\CuadrillaEmpleado;

class CuadrillasImport implements ToCollection, WithHeadingRow, WithMultipleSheets, WithValidation
{
    private $rownumber = 0;
    public function sheets(): array
    {
        return [
            0 => $this
        ];
    }

    /**
     * @param array $row
     *
     * @return array
     */
    public function collection(Collection $rows)
    {
        $acciones = [];
        $id_cuadrilla = null;
        $id_empleado = null;
        foreach ($rows as $row) {
            $this->rownumber++;
            for ($i=0; $i < count($row->keys())-1; $i++) {
                switch($row->keys()[$i]) {
                    case "cuadrilla": break;
                    case "rfc": break;
                    case "empleado": break;
                    case "direccion": break;
                    case "puesto": break;
                    case "nss": break;
                    case "alias": break;
                    case "isjefe": break;
                    default:
                        throw new HttpResponseException(
                            response()->json([
                                'error' => [
                                    'code' => 406,
                                    'message' => "Hay valores invalidos en el archivo" . $this->rownumber
                                ]
                            ], JsonResponse::HTTP_NOT_ACCEPTABLE)
                        );
                }
            }
            if(is_null($row['nss'])
                || empty($row['nss'])
                || !isset($row['nss'])) {
                return null;
            }

            if(Empleado::where('nss', $row['nss'])->exists()) {
                $id_empleado = Empleado::where('nss', $row['nss'])->first()->id;
            } else {
                if(empty($row['empleado']) ||
                    empty($row['direccion']) ||
                    empty($row['puesto']) ||
                    empty($row['nss']) ||
                    is_null($row['alias']) ) {
                    throw new HttpResponseException(
                        response()->json([
                            'error' => [
                                'code' => 406,
                                'message' => "Hay valores no especificados en la fila " . $this->rownumber
                            ]
                        ], JsonResponse::HTTP_NOT_ACCEPTABLE)
                    );
                }
                $data_empleado = new Empleado([
                    'nombre' => $row['empleado'],
                    'rfc' => $row['rfc'],
                    'direccion' => $row['direccion'],
                    'puesto' => $row['puesto'],
                    'nss' => $row['nss'],
                    'alias' => $row['alias']
                ]);

                if($data_empleado->save()) {
                    $id_empleado = $data_empleado->id;
                    $acciones[] = [
                        'Accion' => 'Empleado agregado',
                        'Empleado' => $data_empleado
                    ];
                }
            }

            // Cuadrillas
            if(is_null($row['cuadrilla'])
                || empty($row['cuadrilla'])
                || !isset($row['cuadrilla'])) {
                throw new HttpResponseException(
                    response()->json([
                        'error' => [
                            'code' => 406,
                            'message' => "No hay cuadrilla especificada"
                        ]
                    ], JsonResponse::HTTP_NOT_ACCEPTABLE)
                );
            }

            if(Cuadrilla::where('nombre', $row['cuadrilla'])->exists()) {
                $id_cuadrilla = Cuadrilla::where('nombre', $row['cuadrilla'])->first()->id;
            } else {
                $data_cuadrilla = new Cuadrilla([
                    'nombre' => $row['cuadrilla']
                ]);
                if($data_cuadrilla->save()) {
                    $id_cuadrilla = $data_cuadrilla->id;
                    $acciones[] = [
                        'Accion' => 'Cuadrilla agregada',
                        'Cuadrilla' => $data_cuadrilla
                    ];
                }
            }

            if(is_null($row['isjefe'])
                || empty($row['isjefe'])
                || !isset($row['isjefe'])
                || $row['isjefe'] == 'no') {
                $is_jefe = false;
            } else {
                $is_jefe = true;
            }

            // Desmadre con las cuadrillas
            if(!CuadrillaEmpleado::where('empleado_id', $id_empleado)->where('cuadrilla_id', $id_cuadrilla)->exists()) {
                $data_CE = new CuadrillaEmpleado([
                    'cuadrilla_id' => $id_cuadrilla,
                    'empleado_id' => $id_empleado,
                    'is_jefe' => $is_jefe,
                    ]);
                if($data_CE->save()) {
                    $acciones[] = [
                        'Accion' => 'Cuadrilla empleado agregada',
                        'Cuadrilla_Empleado' => $data_CE
                    ];
                }
            }
        }
        return $acciones;
    }

    public function rules(): array {
        return [
            'rfc' => Rule::in(['required|max:13|min:12|unique:empleados,rfc']),
            'cuadrilla' => Rule::in(['required|string']),
            '*' => Rule::in(['required'])
        ];
    }

    public function customValidationMessages()
    {
        return [
            'rfc.in' => 'El RFC es requerido',
            'cuadrilla.in' => 'La cuadrilla es requerida',
            '*.in' => 'Hay valores invalidos en la hoja de excel'
        ];
    }
}
