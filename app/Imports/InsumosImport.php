<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
//use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\Insumo;
use App\Models\Familia;
use App\Models\Unidade;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class InsumosImport implements ToModel, WithHeadingRow, WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            0 => $this
        ];
    }
    /**
     * @return int
     */
    /*
    public function startRow(): int
    {
        return 2;
    }
*/
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function model(array $row)
    {
        /*
        if (!isset($row[0])) {
            return null;
        }
        */
        /*
        $id=DB::table('unidade')->insertGetId(
            ['nombre' => 'UN3']
        );
        */

        if ($row['unidad'] == null) {
            return null;
        }
        if (Unidade::where('nombre', '=', $row['unidad'])->exists()) {

            $id_unidad = Unidade::where('nombre', $row['unidad'])->first()->id;
        } else {
            $data = new Unidade;
            $data->nombre = $row['unidad'];
            $data->status = 1;
            if ($data->save()) {
                $id_unidad = $data->id;
            }
        }

        if ($row['cantidad']  == "" || $row['cantidad']  == " " || is_null($row['cantidad']) || !isset($row['cantidad'])) {
            $row['cantidad']  = 0;
        }


        if ($row['familia'] == null) {
            $id_Familia = Familia::where('nombre', '=', 'SIN FAMILIA')->first()->id;
        }
        else if (Familia::where('nombre', '=', $row['familia'])->exists()) {

            $id_Familia = Familia::where('nombre', $row['familia'])->first()->id;
        } else {
            $data2 = new Familia;
            $data2->nombre = $row['familia'];
            $data2->status = 1;
            if ($data2->save()) {
                $id_Familia = $data2->id;
            }
        }
        // var_dump($row);
        if ($row['descripcion_insumo'] == null) {
            return null;
        }
        if (!Insumo::where('nombre', '=', $row['descripcion_insumo'])->exists()) {
            return new Insumo([

                // 'descripcion'           => $row['descripcion'],             //descripcion         2
                'nombre'                => $row['descripcion_insumo'],     //descripcion_insumo  3
                'unidad_id'             => $id_unidad,                      //unidad              4
                'cantidad'              => $row['cantidad'],                //Cantidad            5
                'familia_id'            => $id_Familia,                     //familia             8
                'status'                =>  1
            ]);
        }

    }

    public function rules(): array
    {
        return [
            'descripcion_insumo'   => 'required|string|max:255|unique:insumos,nombre',
            'unidad_id'            => 'required|string',
            'cantidad'              => '',
            'familia_id'            => 'required|string',
        ];
    }
}
