<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Insumo extends Model
{
    protected $casts = [
        'data' => 'array'
    ];

    protected $fillable = [
        'nombre',
        'unidad_id',
        'familia_id',
        'cantidad',
        'status',
        'tipo',
        'data'
    ];

    public function Unidades() {
        return $this->belongsTo(Unidade::class, 'unidad_id');
    }

    public function Familias() {
        return $this->belongsTo(Familia::class, 'familia_id');
    }

    public function getCostoEnFraccionamiento($idFraccionamiento)
    {
        $costo = DB::select(DB::raw("
                select
                    MAX(CASE
                    WHEN COALESCE(dd.costo,0) > COALESCE(doc.costo,-1) THEN dd.costo else doc.costo end) as costo
                from detalle_destajos dd
                left join orden_compras oc on oc.fraccionamiento_id = ".$idFraccionamiento." and oc.estatus = 'aprovada'
                join modelos m on m.fraccionamiento_id = ".$idFraccionamiento."
                join detalle_modelos dm on dm.modelo_id = m.id
                left join detalle_orden_compras doc on doc.orden_compra_id = oc.id and doc.insumo_id = ".$this->id."
                where dd.insumo_id = ".$this->id." and dm.destajo_id = dd.destajo_id
        "));

        return $costo[0]->costo;
    }

    public function addOrdenData($detalleId)
    {
        $data = $this->data;
        $ordenCompra =[[
            'detalleId' => $detalleId
        ]];
        if($data !== null){
            if(array_key_exists('orden_compras', $data) && is_array($data['orden_compras'])){
                $ordenCompras = $data['orden_compras'];
                array_push( $ordenCompras, [
                    'detalleId' => $detalleId
                ]);
                $data['orden_compras'] = $ordenCompras;
            }else {
                $data['orden_compras'] = $ordenCompra;
            }
        }else {
            $data = [];
            $data['orden_compras'] = $ordenCompra;
        }

        return $this->update(
            [
                'data' => $data
            ]
        );
    }
}
