<?php

namespace App\Models;

use App\Models\Basic\Ubicacion;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    const ROL = [
        'ADMIN' => 'admin',
        'ADMINISTRADOR DE PROYECTOS' => "admin_proyectos",
        'GERENTE DE CONSTRUCCION' => "gerente_construccion",
        'SUPERVISOR DE EDIFICACION' => 'supervisor_edificacion',
        'SUPERVISOR DE URBANIZACION' => 'supervisor_urbanizacion',
        'CONTADOR' => 'contador',
        'RESIDENTE DE VIVIENDA' => 'residente_vivienda',
        'RESIDENTE DE URBANIZACION' => 'residente_urbanizacion',
        'CONTOL DE OBRA' => 'control_obra',
        'ALMACENISTA' => 'almacenista',
        'COMPRAS' => 'compras',
        'INVITADO' => 'invitado'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rol'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function fraccionamientos()
    {
        return $this->belongsToMany(Fraccionamiento::class, 'user_fraccionamiento', 'user_id', 'fraccionamiento_id');
    }

    public function ubicaciones()
    {
        return $this->belongsToMany(Ubicacion::class, 'user_ubicacion', 'user_id', 'ubicacion_id');
    }
}
