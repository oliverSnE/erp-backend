<?php

namespace App\Models;

use App\Models\Basic\Modelo;
use App\Models\Basic\Ubicacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use File;
class Etapa extends Model
{
    use SoftDeletes;
    protected $appends = ['modelos'];
    protected $fillable = [
        'nombre',
        'logo',
        'fraccionamiento_id'
   ];

    public function fraccionamiento() {
        return $this->belongsTo(Fraccionamiento::class, 'fraccionamiento_id');
    }

    public function getModelosAttribute() {
        return Modelo::where('fraccionamiento_id', $this->fraccionamiento_id)->get();
    }


    public function ubicaciones() {
        return $this->hasMany(Ubicacion::class, 'etapa_id');
    }

    public function setLogo($logo){
        $name = 'logo.'.$logo->getClientOriginalExtension();
        $destinationPath = public_path('/imagenes/etapa/'.$this->id);

        $logo->move($destinationPath, $name);
        $urlPath = '/imagenes/etapa/'.$this->id."/".$name;
        return $this->update(
            [
                'logo' => $urlPath,
            ]
        );
    }

    public function unsetLogo(){
        $imagePath = public_path($this->logo);
        if(File::exists($imagePath)) {
            File::delete($imagePath);
        }
        return $this->update(
            [
                'logo' => "",
            ]
        );
    }
}
