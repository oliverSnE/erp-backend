<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenCompra extends Model
{

    protected $casts = [
        'data' => 'array'
    ];

    protected $fillable = [
        'costo' ,
        'cantidad' ,
        'orden_compra_id' ,
        'insumo_id',
        'data'
   ];

   public function insumo()
   {
       return $this->belongsTo(Insumo::class, 'insumo_id');
   }

    public function ordenCompra() {
        return $this->belongsTo(OrdenCompra::class, 'orden_compra_id');
    }
}
