<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleEntrada extends Model
{
    protected $fillable = [
        'entrada_id',
        'detalle_orden_compra_id',
        'cantidad',
        'costo'
    ];

    public function detalleOrdenCompra() {
        return $this->belongsTo(DetalleOrdenCompra::class, 'detalle_orden_compra_id');
    }

    public function entrada() {
        return $this->belongsTo(Entrada::class, 'entrada_id');
    }

}
