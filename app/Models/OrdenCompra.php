<?php

namespace App\Models;

use App\Models\Basic\Destajo;
use App\Models\Basic\Ubicacion;
use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    protected $casts = [
        'data' => 'array'
    ];


    protected $fillable = [
        'folio' ,
        'estatus' ,
        'proveedor_id' ,
        'user_id' ,
        'fraccionamiento_id',
        'data'
   ];

    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        static::deleting(function($orden) { // before delete() method call this
            $orden->detalle()->delete();
            $orden->facturas()->delete();
            $orden->ubicaciones()->detach();
            $orden->destajos()->detach();
             // do the rest of the cleanup...
        });
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function proveedor() {
        return $this->belongsTo(Proveedores::class, 'proveedor_id');
    }

    public function destajos()
    {
        return $this->belongsToMany(Destajo::class, 'orden_compra_destajo', 'orden_compra_id', 'destajo_id');
    }

    public function ubicaciones()
    {
        return $this->belongsToMany(Ubicacion::class, 'orden_compra_ubicacion', 'orden_compra_id', 'ubicacion_id');
    }

    public function detalle() {
        return $this->hasMany(DetalleOrdenCompra::class, 'orden_compra_id');
    }

    public function facturas() {
        return $this->hasMany(OrdenCompraFactura::class, 'orden_compra_id');
    }
}
