<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuemaNomina extends Model
{
    protected $table = 'quemas_nomina';
    protected $fillable = [
        'quema_id',
        'empleado_id',
        'pago',
        'is_jefe'
    ];

    public function quema() {
        return $this->belongsTo(Quema::class, 'quema_id');
    }

    public function empleado() {
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }
}
