<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Destajo extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['costo_insumos', 'familias_id'];
    const TIPO = [
        'Mano de obra' => 'mano_de_obra',
        'Mano de obra con material' => 'mano_obra_materiales',
        'Subcontrato' => 'subcontrato'
    ];
    protected $fillable = [
        'nombre',
        'descripcion',
        'costo',
        'tipo',
        'mostrar',
        'unidad_id',
        'cantidad'
    ];

    public function getCostoInsumosAttribute(){
        return round($this->DetalleDestajo()->sum(DB::raw('detalle_destajos.costo * detalle_destajos.cantidad')), 2);
    }

    public function unidad() {
        return $this->belongsTo(Unidade::class, 'unidad_id');
    }

    public function DetalleDestajo()
    {
        return $this->hasMany(DetalleDestajo::class, 'destajo_id')->orderby('detalle_destajos.id');
    }

    public function getFamiliasIdAttribute()
    {
        return $this->DetalleDestajo->pluck('familia_id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($destajo) { // before delete() method call this
             $destajo->DetalleDestajo()->delete();
             // do the rest of the cleanup...
        });
    }

}
