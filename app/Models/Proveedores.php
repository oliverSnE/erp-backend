<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Familia;

class Proveedores extends Model
{
    protected $appends = ['familias_id'];
    protected $table = 'proveedores';
    protected $fillable = [
        'nombre',
        'direccion',
        'municipio',
        'estado',
        'cp',
        'tel',
        'email',
        'status'
    ];

    public function familias()
    {
        return $this->belongsToMany(Familia::class);
    }

    public function getFamiliasIdAttribute()
    {
        return $this->familias->pluck('id');
    }
}
