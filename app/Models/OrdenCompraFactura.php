<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenCompraFactura extends Model
{
    protected $fillable = [
        'folio' ,
        'orden_compra_id' ,
        'factura',
        'fecha',
        'importe'
    ];


    public function ordenCompra() {
        return $this->belongsTo(OrdenCompra::class, 'orden_compra_id');
    }

}
