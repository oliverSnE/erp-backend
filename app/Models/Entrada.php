<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entrada extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'orden_compra_id',
        'factura',
        'folio_factura',
        'user_id'
    ];

    public function ordenCompra() {
        return $this->belongsTo(OrdenCompra::class, 'orden_compra_id');
    }

    public function detalle() {
        return $this->hasMany(DetalleEntrada::class, 'entrada_id');
    }

}
