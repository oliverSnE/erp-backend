<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Proveedores;

class Familia extends Model
{
    protected $fillable = [
        'nombre'
    ];

    public function proveedores()
    {
        return $this->belongsToMany(Proveedores::class);
    }
}
