<?php
namespace App\Models\Basic;

use App\Models\Basic\Modelo;
use App\Models\Basic\Ubicacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use File;
class Etapa extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nombre',
        'logo',
        'fraccionamiento_id'
   ];

}
