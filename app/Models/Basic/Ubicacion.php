<?php

namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Ubicacion extends Model
{
    protected $table = 'ubicaciones';
    protected $appends = ['modelo','etapa']; 
    use SoftDeletes;
    protected $fillable = [
        'numero',
        'manzana',
        'etapa_id',
        'modelo_id'
    ];

    public function getModeloAttribute(){
        $modelo = Modelo::where('id', $this->modelo_id)->first();
        return $modelo;
    }

    public function getEtapaAttribute(){
        $etapa = Etapa::where('id', $this->etapa_id)->first();
        return $etapa;
    }
}
