<?php

namespace App\Models\Basic;

use App\Models\DetalleDestajo;
use App\Models\Unidade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Destajo extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    const TIPO = [
        'Mano de obra' => 'mano_de_obra',
        'Mano de obra con material' => 'mano_obra_materiales',
        'Subcontrato' => 'subcontrato'
    ];
    protected $fillable = [
        'nombre',
        'descripcion',
        'costo',
        'tipo',
        'mostrar',
        'unidad_id',
        'cantidad'
    ];

    public function unidad() {
        return $this->belongsTo(Unidade::class, 'unidad_id');
    }

    public function DetalleDestajo()
    {
        return $this->hasMany(DetalleDestajo::class, 'destajo_id')->orderby('detalle_destajos.id');
    }



}
