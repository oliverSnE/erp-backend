<?php

namespace App\Models;

use App\Models\Destajo;
use App\Models\Ubicacion;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class DetalleUbicacion extends Model
{
    protected $table = 'detalle_ubicaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'tipo',
        'ubicacion_id',
        'destajo_id'
    ];

    public function destajo() {
        return $this->belongsTo(Destajo::class, 'destajo_id');
    }

    public function ubicacion() {
        return $this->belongsTo(Ubicacion::class, 'ubicacion_id');
    }
}
