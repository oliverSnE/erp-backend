<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nomina extends Model
{
    protected $fillable = [
        'fecha',
        'nombre',
        'pagado',
        'descuento'
    ];

    public function detalles() {
        return $this->hasMany(DetalleNomina::class, 'nomina_id');
    }
}
