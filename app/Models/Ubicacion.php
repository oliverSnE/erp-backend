<?php

namespace App\Models;

use App\Models\DetalleUbicacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Ubicacion extends Model
{

    protected $table = 'ubicaciones';
    protected $appends = ['costo_calculado', 'destajos'];
    use SoftDeletes;
    protected $fillable = [
        'numero',
        'manzana',
        'etapa_id',
        'modelo_id'
   ];

    public function etapa() {
        return $this->belongsTo(Etapa::class, 'etapa_id');
    }

    public function modelo() {
        return $this->belongsTo(Modelo::class, 'modelo_id');
    }

    public function detalle() {
        return $this->hasMany(DetalleUbicacion::class, 'ubicacion_id');
    }

    public function getDestajosAttribute(){
        $destajosId = collect(DB::select(DB::raw("
            select dm.destajo_id
            from detalle_modelos as dm
            join ubicaciones as u on u.id = (".$this->id.")
            where u.modelo_id = dm.modelo_id and dm.destajo_id not in (
                select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
                )
            group by dm.destajo_id having count(*) = "."1"."
            union all
            select destajo_id from detalle_ubicaciones where ubicacion_id in (".$this->id.") and tipo = 'agregado'
            group by destajo_id having count(*) =  "."1"."
        ")))->pluck('destajo_id');
        $allDestajos = Destajo::whereIn('id', $destajosId)->get();
        return $allDestajos;
    }

    public function getCostoCalculadoAttribute() {
        $costoModelo =  round($this->destajos->sum(function($t){
            return $t->costo * $t->cantidad;
        }),2) + $this->destajos->sum('costo_insumos');
        return  $costoModelo;
    }

    public function ordenCompras()
    {
        return $this->belongsToMany(OrdenCompra::class, 'orden_compra_ubicacion', 'ubicacion_id', 'orden_compra_id');
    }

    public function ordenSalidas()
    {
        return $this->belongsToMany(OrdenSalida::class, 'detalle_orden_salidas', 'ubicacion_id', 'orden_salida_id');
    }

    public function quemas()
    {
        return $this->belongsToMany(Quema::class, 'quemas_destajos', 'ubicacion_id', 'quema_id');
    }



}
