<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuadrilla extends Model
{
    protected $fillable = [
        'nombre'
    ];

    public function detalle()
    {
        return $this->hasMany(CuadrillaEmpleado::class, 'cuadrilla_id');
    }


}
