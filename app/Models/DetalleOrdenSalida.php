<?php

namespace App\Models;

use App\Models\Basic\Ubicacion;
use Illuminate\Database\Eloquent\Model;

class DetalleOrdenSalida extends Model
{
    protected $fillable = [
        'cantidad' ,
        'orden_salida_id' ,
        'detalle_destajo_id',
        'ubicacion_id',
        'estatus'
    ];


    public function detalleDestajos() {
        return $this->belongsTo(DetalleDestajo::class, 'detalle_destajo_id');
    }

    public function ubicacion() {
        return $this->belongsTo(Ubicacion::class, 'ubicacion_id');
    }

    public function ordenSalida() {
        return $this->belongsTo(OrdenSalida::class, 'orden_salida_id');
    }
}
