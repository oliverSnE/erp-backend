<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modelo extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['costo_mano_de_obra', 'costo_insumos'];
    protected $fillable = [
        'nombre',
        'descripcion',
        'fraccionamiento_id'
    ];

    public function fraccionamiento() {
        return $this->belongsTo(Fraccionamiento::class, 'fraccionamiento_id');
    }

    public function getCostoManoDeObraAttribute(){
        return round($this->destajos->sum(function($t){
            return $t->costo * $t->cantidad;
        }),2);
    }

    public function getCostoInsumosAttribute(){
        return $this->destajos->sum('costo_insumos');
    }

    public function destajos()
    {
        return $this->belongsToMany(Destajo::class, 'detalle_modelos', 'modelo_id', 'destajo_id');
    }

    public function DetalleModelo()
    {
        return $this->hasMany(DetalleModelo::class, 'modelo_id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($modelo) { // before delete() method call this
             $modelo->DetalleModelo()->delete();
             // do the rest of the cleanup...
        });
    }
}
