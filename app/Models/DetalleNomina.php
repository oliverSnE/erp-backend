<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleNomina extends Model
{
    protected $fillable = [
       'quema_id',
       'nomina_id'
    ];

    public function nomina()
    {
        return $this->belongsTo(Nomina::class, 'nomina_id');
    }

}
