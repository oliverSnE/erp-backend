<?php

namespace App\Models;

use App\DetalleNomina;
use Illuminate\Database\Eloquent\Model;

class Quema extends Model
{
    protected $fillable = [
        'fraccionamiento_id',
        'user_id',
        'estatus',
        'tipo'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function detalleDestajos()
    {
        return $this->hasMany(QuemaDestajos::class, 'quema_id');
    }

    public function pendientesDetalleDestajos()
    {
        return $this->hasMany(QuemaDestajos::class, 'quema_id')->where('estatus', '=', 'creada');
    }
    public function aprobadaDetalleDestajos()
    {
        return $this->hasMany(QuemaDestajos::class, 'quema_id')->where('estatus','aprobada');
    }

    public function detalleNomina()
    {
        return $this->hasMany(QuemaNomina::class, 'quema_id');
    }
    public function nominaDetalle()
    {
        return $this->belongsTo(DetalleNomina::class, 'quema_id');
    }
}
