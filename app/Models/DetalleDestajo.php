<?php

namespace App\Models;

use App\Models\Basic\Destajo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetalleDestajo extends Model
{
    protected $appends = ['familia_id'];
    protected $fillable = [
        'cantidad',
        'costo',
        'insumo_id',
        'destajo_id',
        'estatus',
        'data'
    ];
    protected $casts = [
        'data' => 'array'
    ];


    public function getCostoAttribute($value)
    {
        // $costo =  DB::select(DB::raw("
        //                 select MAX(doc.costo) as costo
        //                 from detalle_orden_compras as doc
        //                 join detalle_destajos as dd on dd.id = ".$this->id."
        //                 join detalle_modelos as dm on dm.destajo_id = dd.destajo_id
        //                 join modelos as m on m.id = dm.modelo_id
        //                 join orden_compras as oc on oc.fraccionamiento_id = m.fraccionamiento_id and oc.estatus in ('aprobada')
        //                 where doc.orden_compra_id = oc.id  and doc.detalle_destajo_id = dd.id
        //                 group by doc.detalle_destajo_id
        //             "));
        // if(sizeof($costo) > 0 && is_numeric($costo[0]->costo)){
        //     return $costo[0]->costo;
        // }
        return ucfirst($value);
    }

    public function insumo()
    {
        return $this->belongsTo(Insumo::class, 'insumo_id');
    }
    public function destajo()
    {
        return $this->belongsTo(Destajo::class, 'destajo_id');
    }
    public function getFamiliaIdAttribute(){
        return $this->insumo->familia_id;
    }

}
