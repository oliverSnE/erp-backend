<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleModelo extends Model
{
    protected $fillable = [
        'modelo_id',
        'destajo_id'
    ];

    public function modelo()
    {
        return $this->belongsTo(Modelo::class, 'modelo_id');
    }
    public function destajo()
    {
        return $this->belongsTo(Destajo::class, 'destajo_id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($detalle) { // before delete() method call this
             $detalle->destajo()->forceDelete();
             // do the rest of the cleanup...
        });
    }
}
