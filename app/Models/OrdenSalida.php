<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenSalida extends Model
{

    protected $fillable = [
        'folio' ,
        'estatus',
        'user_id',
        'fraccionamiento_id',
        'user_id'
   ];

   public function user()
   {
       return $this->belongsTo(User::class, 'user_id');
   }

   public function fraccionamiento()
   {
       return $this->belongsTo(Fraccionamiento::class, 'fraccionamiento_id');
   }

   public function detalle() {
       return $this->hasMany(DetalleOrdenSalida::class, 'orden_salida_id');
   }

}
