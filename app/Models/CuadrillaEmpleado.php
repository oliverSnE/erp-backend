<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuadrillaEmpleado extends Model
{
    protected $fillable = [
        'cuadrilla_id',
        'empleado_id',
        'is_jefe'
    ];

    public function cuadrilla()
    {
        return $this->belongsTo(Cuadrilla::class, 'cuadrilla_id');
    }
    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }

}
