<?php

namespace App\Models;

use App\Models\Basic\Destajo;
use App\Models\Basic\Ubicacion;
use Illuminate\Database\Eloquent\Model;

class QuemaDestajos extends Model
{

    protected $table = 'quemas_destajos';
    protected $casts = [
        'data' => 'array'
    ];

    protected $fillable = [
        'quema_id',
        'ubicacion_id',
        'destajo_id',
        'estatus',
        'costo',
        'data'
    ];

    public function quema() {
        return $this->belongsTo(Quema::class, 'quema_id');
    }

    public function destajo() {
        return $this->belongsTo(Destajo::class, 'destajo_id');
    }

    public function ubicacion() {
        return $this->belongsTo(Ubicacion::class, 'ubicacion_id');
    }


}
