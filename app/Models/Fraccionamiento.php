<?php

namespace App\Models;

use App\Models\Basic\Etapa;
use App\Models\Basic\Modelo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use File;

class Fraccionamiento extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'direccion',
        'nombre',
        'logo',
        'razon_social_factura',
        'rfc_factura',
        'cp_factura',
        'direccion_factura'
    ];

    public function setLogo($logo){
        $name = 'logo.'.$logo->getClientOriginalExtension();
        $destinationPath = public_path('/imagenes/fraccionamientos/'.$this->id);

        $logo->move($destinationPath, $name);
        $urlPath = '/imagenes/fraccionamientos/'.$this->id."/".$name;
        return $this->update(
            [
                'logo' => $urlPath,
            ]
        );
    }

    public function unsetLogo(){
        $imagePath = public_path($this->logo);
        if(File::exists($imagePath)) {
            File::delete($imagePath);
        }
        return $this->update(
            [
                'logo' => "",
            ]
        );
    }

    public function etapas() {
        return $this->hasMany(Etapa::class, 'fraccionamiento_id');
    }

    public function modelos() {
        return $this->hasMany(Modelo::class, 'fraccionamiento_id');
    }

}
