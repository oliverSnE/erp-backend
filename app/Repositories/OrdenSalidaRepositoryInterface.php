<?php
namespace App\Repositories;

use App\Http\Requests\OrdenSalidaDestajosEstatusRequest;
use App\Models\User;
use App\Models\OrdenSalida;
use App\Models\Basic\Ubicacion;
use App\Http\Requests\OrdenSalidaRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;

interface OrdenSalidaRepositoryInterface
{
    /**
     * Funcion para obtener las ubicaciones de la Orden de Salida.
     *
     * @param Request $request
     * @return Ubicacion
     */
    public function ubicaciones(Request $request);

    /**
     * Funcion para obtener los destajos de una Orden de Salida y estos son
     * puestos dentro de las ubicaciones relacionadas.
     *
     * @param Request $request
     * @return Ubicacion
     */
    public function destajos(Request $request);

    /**
     * Funcion para crear una Orden de Salida en la base de datos.
     *
     * @param OrdenSalidaRequest $request
     * @return OrdenSalida
     */
    public function create(OrdenSalidaRequest $request);

    /**
     * Funcion para obtener una Orden de Salida usando su id.
     *
     * @param integer $id
     * @return OrdenSlaida
     */
    public function find(int $id);

    /**
     * Funcion para obtener una Orden de Salida usando su folio.
     *
     * @param string $folio
     * @return OrdenSalida
     * @throws HttpResponseException
     */
    public function findByFolio(string $folio);

    /**
     * Funcion para obtener todas las Ordenes de Salidas relacionadas
     * con un fraccionamiento en especifico.
     *
     * @param integer $fraccionamientoId
     * @return OrdenSalida
     */
    public function findAllByFraccionamiento(int $fraccionamientoId);

    /**
     * Funcion para marcar la Orden de Salida como procesando.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenSalida
     */
    public function processing(int $id, $user);

    /**
     * Funcion para marcar la Orden de Salida como lista.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenSalida
     */
    public function ready(int $id, $user);

    /**
     * Funcion para marcar la Orden de Salida como cancelada.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenSalida
     */
    public function cancel(int $id, $user);

    /**
     * Funcion para marcar la Orden de Salida como completada.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenSalida
     */
    public function completed(int $id, $user);

    public function changeEstatus(OrdenSalidaDestajosEstatusRequest $request);

    public function getDestajoUbicacion(int $destajo_id, int $ubicacion_id, int $orden_salida_id);

}
