<?php
namespace App\Repositories;

use App\Http\Requests\EtapaRequest;
use App\Models\Basic\Ubicacion;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use App\Models\Etapa;

class EtapaRepository implements EtapaRepositoryInterface
{
    public function all()
    {
        $data = Etapa::all();

        return $data;
    }

    public function create(EtapaRequest $request)
    {
        $etapa = Etapa::create([
            'nombre' =>  $request->get('nombre'),
            'fraccionamiento_id'=>$request->get('fraccionamiento_id'),
        ]);

        if($etapa->setLogo($request->file('logo'))){
            return $etapa;
        }

        throw new HttpResponseException(
            response()->json( ["error" => [
                "code"=> 422,
                "message" => "Ocurrio un error en el servidor al tratar de crear la etapa."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function update(EtapaRequest $request)
    {
        $etapa = Etapa::find($request->get('id'));
        if($etapa) {
            Etapa::where('id', $request->get('id'))->update([
                'nombre' =>  $request->get('nombre')
            ]);

            if($request->file('logo')) {

                if($etapa->unsetLogo() && $etapa->setLogo($request->file('logo'))){
                    return $etapa->fresh();
                }
            }

            return Etapa::find($request->get('id'));

        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "La etapa no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function find(int $id)
    {
        $etapa = Etapa::with(array('ubicaciones' => function($query) {
            $query->orderByRaw('length(manzana) ASC');
            $query->orderBy('manzana', 'ASC');
            $query->orderByRaw('numero::INTEGER asc');
        }))->where('id', $id)->first();
        if($etapa){
            return $etapa;
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "La etapa no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function destroy(int $id)
    {
        $ubicaciones = Ubicacion::where('etapa_id', $id)->exists();

        if ($ubicaciones) {

            throw new HttpResponseException(
                response()->json(['error' => [
                    "code"=> 422,
                    "message" => "No se puede eliminar la etapa."
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        else {
            $etapa = Etapa::where('id', $id)->exists();
            if ($etapa) {
                Etapa::find($id)->delete($id);
                return $etapa;
            }
            throw new HttpResponseException(
                response()->json(['error' => [
                    "code"=> 404,
                    "message" => "La etapa no existe."
                    ]
                ], JsonResponse::HTTP_NOT_FOUND)
            );
        }

    }

}
