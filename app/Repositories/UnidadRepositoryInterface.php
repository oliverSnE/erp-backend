<?php
namespace App\Repositories;

interface UnidadRepositoryInterface
{
    /**
     * Funcion para obtener todas las unidades
     *
     * @return object
     */
    public function all();
}

