<?php

namespace App\Repositories;

use App\Http\Requests\CostoDestajoRequest;
use App\Http\Requests\DestajoRequest;
use App\Http\Requests\DetalleDestajoRequest;
use App\Models\Basic\Destajo as BasicDestajo;
use App\Models\Destajo;
use App\Models\DetalleDestajo;
use App\Models\QuemaDestajos;
use App\Models\User;
use App\Repositories\DestajoRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class DestajoRepository implements DestajoRepositoryInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destajos = Destajo::with('DetalleDestajo')->where('mostrar', '=', 'todos')->get();


        if ($destajos) {
            return $destajos;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(DestajoRequest $request)
    {
        $request->validated();

        $destajo = Destajo::create([
            'nombre' => $request['nombre'],
            'descripcion' => $request['descripcion'],
            'costo' => $request['costo'],
            'tipo' => $request['tipo'],
            'cantidad' => $request['cantidad'],
            'unidad_id' => $request['unidad_id'],
        ]);

        if ($destajo) {
            if($request['tipo'] === 'mano_obra_materiales'){
                $detalles = $request->get('detalle');
                foreach ($detalles as $value) {
                    $this->addDetalle($value, $destajo);
                }
            }

            $response = Destajo::with('DetalleDestajo', 'DetalleDestajo.insumo')->where('id', $destajo->id)->first();
            return  $response;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function getDetalleById($detalle_id) {
        $detalle = DetalleDestajo::with('destajo.unidad', 'insumo.Unidades')->where('id', $detalle_id)->first();

        if($detalle) {
            return $detalle;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Destajo  $destajo
     * @return \Illuminate\Http\Response
     */
    public function show($id): Destajo
    {
        $destajos = Destajo::with('DetalleDestajo', 'DetalleDestajo.insumo','unidad','DetalleDestajo.insumo.Unidades')->where('id', $id)->first();

        if ($destajos) {
            return $destajos;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function editDetalle(DetalleDestajoRequest $request)
    {
        /** @var DetalleDestajo $detalle */
        $user = JWTAuth::parseToken()->authenticate();
        $detalle = DetalleDestajo::find($request->get('detalle_id'));
        $data = [
            "user_id" => $user->id,
            "cantida_anterior" => $detalle->cantidad,
            "update_at" => Carbon::now()
        ];
        if($detalle->data){
            $dataDetalle = $detalle->data;
            if(array_key_exists('modificacion', $dataDetalle)){
                array_push($dataDetalle['modificacion'],$data);
            }else {
                $dataDetalle = array_push($dataDetalle, ['modificacion' => [[$data]]]);
            }
            $detalle->data = $dataDetalle;
        }else {
            $detalle->data = ['modificacion' => [$data]];
        }
        $detalle->cantidad = $request->get('cantidad');
        $detalle->estatus = 'modificado';
        if($detalle->save()){
            return $detalle;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Al tratar de actalizar el detalle'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Destajo  $destajo
     * @return \Illuminate\Http\Response
     */

     /************      METODO UPDATE NO SE ESTA UTILIZANDO       ************* */
    // public function update(DestajoRequest $request)
    // {
    //     $request->validated();
    //     $destajo = Destajo::with('DetalleDestajo', 'DetalleDestajo.insumo')->where('id', $request->id)->first();
    //     $detalleDes = $destajo->detalleDestajo()->get();
    //     $detalleRq = $request->get('detalle');
    //     $excludeFromdelete = [];

    //     $destajoUpdate = Destajo::where('id', $request->id)->update([
    //         'nombre' => $request['nombre'],
    //         'descripcion' => $request['descripcion'],
    //         'costo' => $request['costo'],
    //         'tipo' => $request['tipo'],
    //         'cantidad' => $request['cantidad'],
    //         'unidad_id' => $request['unidad_id'],
    //     ]);
    //     if ($destajoUpdate) {

    //         foreach ($detalleRq as $value) {
    //             $isNew = true;
    //             //return $value;
    //             foreach ($detalleDes as $val) {
    //                 //return $val->destajo_id;
    //                 if ($val->insumo_id === (int) $value['insumo_id']) {

    //                     DetalleDestajo::where('destajo_id', $request->id)->delete();
    //                 }
    //                 $val->update([
    //                     'cantidad' => $value['cantidad']
    //                 ]);
    //                 $isnew = false;
    //             }
    //             if ($isNew) {
    //                 $this->addDetalle($value, $destajo);
    //             }
    //         }
    //         foreach ($detalleDes as $val) {
    //             $willDelete = true;
    //             foreach ($excludeFromdelete as $id) {
    //                 $willDelete = false;
    //             }
    //             if ($willDelete) {
    //                 $val->delete();
    //             }
    //         }
    //         return $destajo = Destajo::with('DetalleDestajo', 'DetalleDestajo.insumo')->where('id', $request->id)->first();
    //     }
    //     throw new HttpResponseException(
    //         response()->json([
    //             'error' => [
    //                 "code" => 422,
    //                 "message" => 'Ocurrió un error en el servidor'
    //             ]
    //         ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
    //     );
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Destajo  $destajo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $destajo = Destajo::withTrashed('DetalleDestajo', 'DetalleDestajo.insumo')->where('id', $id)->first();

       if($destajo->delete()) {
            DetalleDestajo::where('destajo_id', $id)->delete();

            return response()->json([
                'data' => [
                    "code" => 200,
                    "message" => 'Eliminado correctamente.'
                ]
            ], 200);
       }
       throw new HttpResponseException(
        response()->json([
            'error' => [
                "code" => 422,
                "message" => 'Ocurrió un error en el servidor'
            ]
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
    );

       //return $destajo;
    }
    private function addDetalle($detail, $destajo)
    {
        $destajo->DetalleDestajo()->create([
            'cantidad' => $detail['cantidad'],
            'costo' => $detail['costo'],
            'insumo_id' => $detail['insumo_id'],
        ]);
    }

    public function getTipos(){
        return Destajo::TIPO;
    }


    public function destajosByUbicacion(Request $request, $user)
    {

        $ordenes = collect(DB::select(DB::raw("
            select
                d.*,
                string_agg(DISTINCT(select CONCAT(SUM(costo),'-',qd.ubicacion_id) from quemas_destajos as qd where qd.ubicacion_id = u.id and qd.destajo_id = d.id group by qd.ubicacion_id)::varchar,',') as pagado,
                (select nombre from unidades where unidades.id = d.unidad_id) as unidad_nombre,
                string_agg(DISTINCT u.id::varchar,',') as ubicaciones_id,
                string_agg(DISTINCT (select CONCAT(ubicaciones.manzana, '/', ubicaciones.numero) from ubicaciones where ubicaciones.id = u.id)::varchar,',') as ubicaciones
            from destajos as d
            join user_ubicacion as ub on ub.user_id = ".$user->id."
            join ubicaciones as u on u.id = ub.ubicacion_id  and u.etapa_id = ".$request->get('etapa_id')." and u.modelo_id = ".$request->get('modelo_id')."
            LEFT join detalle_modelos as dm on dm.modelo_id = u.modelo_id and dm.destajo_id not in ((
                        select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
                        ))
            left join detalle_ubicaciones as du on du.ubicacion_id = u.id and du.tipo = 'agregado'
            where (d.id = dm.destajo_id or d.id = du.destajo_id) and (d.tipo = 'mano_de_obra' or d.tipo = 'mano_obra_materiales' )
            group by d.id
            order by d.id
        ")));

        return $ordenes;
    }
    public function destajosBySubcontrato(Request $request, $user)
    {

        $ordenes = collect(DB::select(DB::raw("
            select
                d.*,
                string_agg(DISTINCT(select CONCAT(SUM(costo),'-',qd.ubicacion_id) from quemas_destajos as qd where qd.ubicacion_id = u.id and qd.destajo_id = d.id group by qd.ubicacion_id)::varchar,',') as pagado,
                (select nombre from unidades where unidades.id = d.unidad_id) as unidad_nombre,
                string_agg(DISTINCT u.id::varchar,',') as ubicaciones_id,
                string_agg(DISTINCT (select CONCAT(ubicaciones.manzana, '/', ubicaciones.numero) from ubicaciones where ubicaciones.id = u.id)::varchar,',') as ubicaciones
            from destajos as d
            join user_ubicacion as ub on ub.user_id = ".$user->id."
            join ubicaciones as u on u.id = ub.ubicacion_id  and u.etapa_id = ".$request['etapa_id']." and u.modelo_id = ".$request['modelo_id']."
            LEFT join detalle_modelos as dm on dm.modelo_id = u.modelo_id and dm.destajo_id not in ((
                        select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
                        ))
            left join detalle_ubicaciones as du on du.ubicacion_id = u.id and du.tipo = 'agregado'
            where (d.id = dm.destajo_id or d.id = du.destajo_id) and (d.tipo = 'subcontrato' )
            group by d.id
        ")));

        return $ordenes;
    }


    public function costoDestajoByUbicacion(CostoDestajoRequest $request)
    {
        $request->validated();
        $destajo = BasicDestajo::find($request->get('destajo_id'));
        $pagoActual = QuemaDestajos::
                            where('ubicacion_id', '=', $request["ubicacion_id"])
                            ->where('destajo_id', '=', $request['destajo_id'])
                            ->get()->sum('costo');
        $diferencia = round(($destajo->costo * $destajo->cantidad), 3) - $pagoActual;
        if($diferencia < 1)
        {
            $quema = QuemaDestajos::where('ubicacion_id', '=', $request["ubicacion_id"])
                ->where('destajo_id', '=', $request['destajo_id'])
                ->orderBy('id','desc')
                ->first();
            $quema->costo += $diferencia;
            if($quema->save())
            {
                $pagoActual = QuemaDestajos::
                            where('ubicacion_id', '=', $request["ubicacion_id"])
                            ->where('destajo_id', '=', $request['destajo_id'])
                            ->get()->sum('costo');
            }
        }

        return round(($destajo->costo * $destajo->cantidad), 3) - $pagoActual;
    }

    public function searchByName(Request $request)
    {
        $destajo =  Destajo::with('DetalleDestajo', 'DetalleDestajo.insumo','unidad','DetalleDestajo.insumo.Unidades')->where('nombre', $request['nombre'])->first();

        if ($destajo) {
            return $destajo;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

}
