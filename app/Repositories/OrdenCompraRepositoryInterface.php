<?php
namespace App\Repositories;

use App\Http\Requests\OrdenCompraAddFacturaRequest;
use App\Http\Requests\OrdenCompraCreateInsumosRequest;
use App\Http\Requests\OrdenCompraCreateRequest;
use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\OrdenCompraReOrdenRequest;
use App\Http\Requests\OrdenCompraSubcontratosInsumosRequest;
use App\Http\Requests\OrdenCompraUpdateRequest;
use App\Http\Requests\UbicacionByModeloEtapaRequest;
use App\Models\User;
use App\Models\Destajo;
use App\Models\OrdenCompra;
use App\Models\OrdenCompraFactura;
use Illuminate\Http\Request;

interface OrdenCompraRepositoryInterface
{
    /**
     * Funcion para crear una Orden de Compra en la base de datos.
     *
     * @param Request $request
     * @param User $user
     * @return OrdenCompra
     */
    public function create(OrdenCompraCreateRequest $request, $user);

    /**
     * Funcion para crear una Orden de Compra en la base de datos
     * solo con insumos.
     *
     * @param Request $request
     * @param User $user
     * @return OrdenCompra
     */
    public function createOnlyInsumos(OrdenCompraCreateInsumosRequest $request, $user);

    /**
     * Funcion para actualizar el detalle de una Orden de Compra.
     * en la base de datos.
     *
     * @param Request $request
     * @return OrdenCompra
     */
    public function updateDetalle(OrdenCompraUpdateRequest $request);

    /**
     * Funcion para marcar como aprobada una Orden de Compra.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenCompra
     */
    public function aprove(int $id, $user);

    /**
     * Funcion para marcar cono cancelada una Orden de Compra.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenCompra
     */
    public function cancel(int $id, $user);

    /**
     * Funcion para marcar como completada una Orden de Compra.
     *
     * @param integer $id
     * @param User $user
     * @return OrdenCompra
     */
    public function completed(int $id, $user);

    /**
     * Funcion para obtener una Orden de Compra usando su id.
     *
     * @param integer $id
     * @return OrdenCompra
     */
    public function find(int $id);

    /**
     * Funcion para obtener una Orden de Compra usando su numero de folio.
     *
     * @param String $folio
     * @return OrdenCompra
     * @throws HttpResponseException
     */
    public function findByFolio(String $folio);

    /**
     * Funcion para obtener todas las ordenes de compra.
     *
     * @return object
     * @throws HttpResponseException
     */
    public function findAll();

    /**
     * Funcion para obtener todas las ordenes de compra usando el id
     * del fraccionamiento relacionado con el mismo.
     *
     * @param integer $fraccionamientoId
     * @return OrdenCompra
     */
    public function findAllByFraccionamiento(int $fraccionamientoId);

    /**
     * Funcion para obtener las ubicaciones relacionadas con la Orden
     * de Compra
     *
     * @param Request $request
     * @return object
     */
    public function ubicacion(UbicacionByModeloEtapaRequest $request);

    /**
     * Funcion para obtener los destajos de la Orden de Compra
     *
     * @param Request $request
     * @return Destajo
     */
    public function destajos(OrdenCompraDestajosRequest $request);

    /**
     * Funcion para obtener los subcontratos de la Orden de Compra.
     *
     * @param Request $request
     * @return Destajo
     */
    public function subcontratos(OrdenCompraDestajosRequest $request);

    /**
     * Funcion para obtener los insumos de los subcontratos de la
     * Orden de compra.
     *
     * @param Request $request
     * @return object
     */
    public function subcontratosInsumos(OrdenCompraSubcontratosInsumosRequest $request);

    /**
     * Funcion para obtener la verificacion de una Orden de Compra.
     *
     * @param Request $request
     * @return object
     */
    public function verificacion(int $id);

    /**
     * Funcion para añadir una factura a una Orden de Compra en especifico.
     *
     * @param Request $request
     * @param User $user
     * @return OrdenCompraFactura
     */
    public function addFactura(OrdenCompraAddFacturaRequest $request, $user);

    /**
     * Funcion para obtener las Ordenes Especiales de la base de datos.
     *
     * @param integer $model_id
     * @return object
     */
    public function getSpecialOrders(int $model_id);

    public function getFacturas(int $orden_id);

    public function getFactura(int $factura_id);

    public function reOrden(OrdenCompraReOrdenRequest $request, $user);
}

