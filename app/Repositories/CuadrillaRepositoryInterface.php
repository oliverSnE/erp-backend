<?php
namespace App\Repositories;
use App\Models\Cuadrilla;
use App\Http\Requests\CuadrillaRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

interface CuadrillaRepositoryInterface
{
    /**
     * Funcion para guardar un nuevo Cuadrilla en la base de datos
     *
     * @param CuadrillaRequest $request
     * @return Cuadrilla
     */
    public function create(CuadrillaRequest $request): Cuadrilla;

    /**
     * Funcion para actualizar un Cuadrilla en la base de datos
     *
     * @param CuadrillaRequest $request
     * @return Cuadrilla
     * @throws HttpResponseException
     */
    public function update(CuadrillaRequest $request);

    /**
     * Funcion para obtener un Cuadrilla por medio de su id
     *
     * @param integer $id
     * @return Cuadrilla
     * @throws HttpResponseException
     */
    public function findById(int $id);

    /**
     * Funcion para obtener todos los Cuadrillas
     *
     * @return object
     * @throws HttpResponseException
     */
    public function findAll(): object;

    /**
     * Funcion para eliminar un Cuadrilla por medio de su id
     *
     * @param integer $id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy(int $id);
}

