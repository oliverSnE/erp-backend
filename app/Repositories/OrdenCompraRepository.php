<?php
namespace App\Repositories;

use App\Http\Requests\MailRequest;
use App\Http\Requests\OrdenCompraAddFacturaRequest;
use App\Http\Requests\OrdenCompraCreateInsumosRequest;
use App\Http\Requests\OrdenCompraCreateRequest;
use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\OrdenCompraReOrdenRequest;
use App\Http\Requests\OrdenCompraSubcontratosInsumosRequest;
use App\Http\Requests\OrdenCompraUpdateRequest;
use App\Http\Requests\UbicacionByModeloEtapaRequest;
use App\Models\OrdenCompra;
use App\Models\Destajo;
use App\Models\User;
use App\Services\MailServiceInterface;
use App\Models\DetalleOrdenCompra;
use App\Models\Insumo;
use Illuminate\Support\Str;
use App\Models\OrdenCompraFactura;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrdenCompraRepository implements OrdenCompraRepositoryInterface
{
    protected $mail;
    public function __construct(MailServiceInterface $mail)
    {
        $this->mail = $mail;
    }

    public function create(OrdenCompraCreateRequest $request, $user)
    {
        $detalles = $request->get('detalle');
        $provedoresId = [];
        $statusOrden = false;
        foreach ($detalles as $value) {
            if(array_key_exists($value['proveedor_id'], $provedoresId) && $provedoresId[$value['proveedor_id']] === true){
                continue;
            }
            $provedoresId[$value['proveedor_id']] = $value['cambios'];

        }
        $ordenesCreadas = [];
        foreach ($provedoresId as $key => $value) {
            $folio = OrdenCompra::max('folio');
            $orden = OrdenCompra::create([
                'folio' =>   $folio ? ($folio + 1) : 10001,
                'proveedor_id' => $key,
                'fraccionamiento_id' => $request['fraccionamiento_id'],
                'user_id' => $user->id,
                'estatus' => $value == "true" ? "pendiente" : "creada"
            ]);
            $statusOrden = $value;


            if($orden){
                $orden->ubicaciones()->attach($request['ubicaciones']);
                $orden->destajos()->attach($request['destajos']);
                $detalles = $request->get('detalle');
                foreach ($detalles as $value) {
                    $this->addDetalle($value, $orden);
                }
            }
            $ordenesCreadas[] = $orden;
        }

        if($ordenesCreadas && $statusOrden) {
            $lastOrder = OrdenCompra::latest()->first();
            $this->notificacionCorreo($lastOrder['id'], "pendiente");
        }

        return $ordenesCreadas;
    }

    public function reOrden(OrdenCompraReOrdenRequest $request, $user)
    {
        OrdenCompra::find($request->get('orden_id'))->delete();

        $detalles = $request->get('detalle');
        $provedoresId = [];
        $statusOrden = false;
        foreach ($detalles as $value) {
            if(array_key_exists($value['proveedor_id'], $provedoresId) && $provedoresId[$value['proveedor_id']] === true){
                continue;
            }
            $provedoresId[$value['proveedor_id']] = $value['cambios'];

        }
        $ordenesCreadas = [];
        foreach ($provedoresId as $key => $value) {
            $lastOrder = OrdenCompra::latest()->first();
            $orden = OrdenCompra::create([
                'folio' =>   $lastOrder ? ($lastOrder->folio + 1) : 10000,
                'proveedor_id' => $key,
                'fraccionamiento_id' => $request['fraccionamiento_id'],
                'user_id' => $user->id,
                'estatus' => $value == "true" ? "pendiente" : "creada"
            ]);
            $statusOrden = $value;


            if($orden){
                $orden->ubicaciones()->attach($request['ubicaciones']);
                $orden->destajos()->attach($request['destajos']);
                $detalles = $request->get('detalle');
                foreach ($detalles as $value) {
                    $this->addDetalle($value, $orden);
                }
            }
            $ordenesCreadas[] = $orden;
        }

        if($ordenesCreadas && $statusOrden) {
            $lastOrder = OrdenCompra::latest()->first();
            $this->notificacionCorreo($lastOrder['id'], "pendiente");
        }

        return $ordenesCreadas;
    }

    public function createOnlyInsumos(OrdenCompraCreateInsumosRequest $request, $user)
    {

        $detalles = $request->get('detalle');
        $provedoresId = [];
        foreach ($detalles as $value) {
            if(array_key_exists($value['proveedor_id'], $provedoresId) && $provedoresId[$value['proveedor_id']] === true){
                continue;
            }
            $provedoresId[$value['proveedor_id']] = $value['cambios'];

        }
        $ordenesCreadas = [];
        foreach ($provedoresId as $key => $value) {
            $lastOrder = OrdenCompra::latest()->first();
            $orden = OrdenCompra::create([
                'folio' =>   $lastOrder ? ($lastOrder->folio + 1) : 10000,
                'proveedor_id' => $key,
                'fraccionamiento_id' => $request['fraccionamiento_id'],
                'user_id' => $user->id,
                'estatus' => $value == "true" ? "pendiente" : "creada"
            ]);
            if($orden){
                $detalles = $request->get('detalle');
                foreach ($detalles as $value) {
                    $this->addDetalle($value, $orden);
                }
            }
            $ordenesCreadas[] = $orden;
        }

        return $ordenesCreadas;
    }

    public function updateDetalle(OrdenCompraUpdateRequest $request)
    {
        OrdenCompra::find($request->get('orden_id'))->update([
            'proveedor_id' => $request->get('proveedor_id')
        ]);
        $detalleRq = $request->get('detalle');
        $changeState = false;
        foreach ($detalleRq as $value) {
            $doc = DetalleOrdenCompra::find($value['detalle_orden_id']);
            if(!$changeState && (float)$doc->costo < (float)$value['costo']){
                $changeState = true;
            }
            $doc->update([
                'costo' => $value['costo']
            ]);
        }

        if($changeState){
            OrdenCompra::find($request->get('orden_id'))->update([
                'estatus' => "pendiente"
            ]);
        }

        return OrdenCompra::with('user','proveedor', 'detalle', 'detalle.insumo')->where('id', $request->get('orden_id'))->first();
    }

    public function aprove(int $id, $user)
    {
        $ordenCompra =  OrdenCompra::where('id', $id)->first();
        if($ordenCompra)
        {
            if($ordenCompra->estatus !== 'pendiente'){
                throw new HttpResponseException(
                    response()->json(['error' =>
                            [
                                "code" =>  401,
                                "message" => "No es necesario validar la orden de compra"
                            ]
                        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
                );
            }

            if($ordenCompra->data === null){
                $ordenCompra->update([
                    'estatus' => 'aprobada',
                    'data'    => [
                        'estatus_aprobada' => [
                            "user_id" => $user->id,
                            "update_at" => Carbon::now()
                        ]
                    ]
                ]);
                $this->notificacionCorreo($id, "aprobada");
                return $ordenCompra->refresh();
            }
            $data = $ordenCompra->data;
            $data['estatus_aprobada'] = [
                "user_id" => $user->id,
                "update_at" => Carbon::now()
            ];
            $ordenCompra->update([
                'estatus' => 'aprobada',
                'data'    => $data
                ]);
            $this->notificacionCorreo($id, "aprobada");

            return $ordenCompra->refresh();
        }

        throw new HttpResponseException(
            response()->json(['error' =>
                    [
                        "code" =>  422,
                        "message" => "La orden de compra no existe"
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function cancel(int $id, $user)
    {
        $ordenCompra =  OrdenCompra::where('id', $id)->first();
        if($ordenCompra)
        {
            if($ordenCompra->estatus !== 'pendiente'){
                throw new HttpResponseException(
                    response()->json(['error' =>
                            [
                                "code" =>  401,
                                "message" => "No es posible cancelar esta orden de compra"
                            ]
                        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
                );
            }

            if($ordenCompra->data === null){
                $ordenCompra->update([
                    'estatus' => 'cancelada',
                    'data'    => [
                        'estatus_cancelada' => [
                            "user_id" => $user->id,
                            "update_at" => Carbon::now()
                        ]
                    ]
                ]);
                $this->notificacionCorreo($id, "cancelada");
                return $ordenCompra->refresh();
            }
            $data = $ordenCompra->data;
            $data['estatus_cancelada'] = [
                    "user_id" => $user->id,
                    "update_at" => Carbon::now()
                ];
            $ordenCompra->update([
                'estatus' => 'cancelada',
                'data'    => $data
            ]);
            $this->notificacionCorreo($id, "cancelada");
            return $ordenCompra->refresh();
        }

        throw new HttpResponseException(
            response()->json(['error' =>
                    [
                        "code" =>  422,
                        "message" => "La orden de compra no existe"
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function completed(int $id, $user)
    {
        $ordenCompra =  OrdenCompra::where('id', $id)->first();
        if($ordenCompra)
        {
            if($ordenCompra->estatus !== 'en proceso'){
                throw new HttpResponseException(
                    response()->json(['error' =>
                            [
                                "code" =>  401,
                                "message" => "No es posible completar esta orden de compra"
                            ]
                        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
                );
            }

            if($ordenCompra->data === null){
                $ordenCompra->update([
                    'estatus' => 'completada',
                    'data'    => [
                        'estatus_completada' => [
                            "user_id" => $user->id,
                            "update_at" => Carbon::now()
                        ]
                    ]
                ]);
                return $ordenCompra->refresh();
            }
            $data = $ordenCompra->data;
            $data['estatus_completada'] = [
                    "user_id" => $user->id,
                    "update_at" => Carbon::now()
                ];
            $ordenCompra->update([
                'estatus' => 'completada',
                'data'    => $data
            ]);

            return $ordenCompra->refresh();
        }

        throw new HttpResponseException(
            response()->json(['error' =>
                    [
                        "code" =>  422,
                        "message" => "La orden de compra no existe"
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function find(int $id)
    {

        $orden = OrdenCompra::with(
            'user',
            'proveedor',
            'ubicaciones',
            'destajos',
            'detalle.insumo.Unidades'
            )->where('id', $id)->first();
        if($orden){
            return $orden;
        }

         throw new HttpResponseException(
            response()->json(['error' =>
                    [
                        "code" =>  422,
                        "message" => "La orden de compra no existe"
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function findByFolio(String $folio)
    {
        $orden = OrdenCompra::with(
            'user',
            'proveedor',
            'ubicaciones',
            'destajos',
            'detalle.insumo.Unidades')
            ->where('folio','=',$folio)->first();
        if($orden){
            return $orden;
        }

        throw new HttpResponseException(
            response()->json(['error' =>
                    [
                        "code" =>  422,
                        "message" => "La orden de compra no existe"
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function findAll()
    {
        return OrdenCompra::with('user','proveedor')->get();
    }

    public function findAllByFraccionamiento(int $fraccionamientoId){
        return OrdenCompra::with('user','proveedor', 'ubicaciones')->where('fraccionamiento_id', $fraccionamientoId)->get();
    }

    public function verificacion(int $id)
    {
        $orden = $this->find($id);
        if($orden->estatus !== 'pendiente'){
            throw new HttpResponseException(
                response()->json(['error' =>
                        [
                            "code" =>  401,
                            "message" => "No es necesario verificar esta orden de compra"
                        ]
                    ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        $verficacion = [];
        $ubicaciones = [];
        foreach ($orden->detalle as $detalle) {
            $verficacion[] = [
                'insumo'                => $detalle->insumo->nombre,
                'unidad'                => $detalle->insumo->unidades->nombre,
                'costo_destajo'         => $detalle->insumo->getCostoEnFraccionamiento($orden->fraccionamiento_id),
                'costo_orden_compra'    => $detalle->costo
            ];
        }

        foreach ($orden->ubicaciones as $ubicacion) {
            $ubicaciones[] = [
                'nombre_manzana'                => $ubicacion->manzana."/".$ubicacion->numero,
            ];
        }

        $response = [
            'verificacion'      => $verficacion,
            'ubicaciones'       => $ubicaciones,
            'folio'             => $orden->folio
        ];


        return $response;
    }

    private function addDetalle($detail, $ordenCompra)
    {
        if($detail['proveedor_id'] == $ordenCompra->proveedor_id){
            $ordenCompra->detalle()->create([
                'insumo_id' => $detail['insumo_id'],
                'costo' => $detail['costo'],
                'cantidad' => $detail['cantidad']
            ]);
        }

    }

    public function ubicacion(UbicacionByModeloEtapaRequest $request)
    {
        $ubicaciones = DB::table('ubicaciones as t')
        ->select('t.*')
        ->where('etapa_id', $request['etapa_id'])
        ->where('modelo_id', $request['modelo_id'])
        ->get();

        if($ubicaciones) {
            return $ubicaciones;
        }

    }

    public function destajos(OrdenCompraDestajosRequest $request)
    {
        return Destajo::with('DetalleDestajo.insumo.Unidades')->whereIn('id', $this->getDestajosValidos($request))->where('tipo', '=', 'mano_obra_materiales')->get();
    }

    public function subcontratos(OrdenCompraDestajosRequest $request)
    {
        return Destajo::whereIn('id', $this->getDestajosValidos($request))->where('tipo', '=', 'subcontrato')->get();
    }

    public function subcontratosInsumos(OrdenCompraSubcontratosInsumosRequest $request)
    {
        $insumos = [];
        foreach ($request->get('destajos_id') as $id) {
            $destajo = Destajo::where('id', $id)->first();
            $insumo = Insumo::create([
                'nombre' => $destajo->nombre,
                'unidad_id' => $destajo->unidad_id,
                'familia_id' => 5,
                'cantidad' => 0,
                'status' => 1,
                'tipo'  => 'subcontrato'
            ]);

            $insumos[] = [
                'cantidad' => $destajo->cantidad,
                'costo' => $destajo->costo,
                'insumo' => Insumo::with('unidades', 'familias')->find($insumo->id)
            ];
        }

        return $insumos;
    }

    private function getDestajosValidos(OrdenCompraDestajosRequest $request)
    {
        $destajosId = collect(DB::select(DB::raw("
        select *
        from (
        --Obtener destajos de los modelos
        select dm.destajo_id
        from detalle_modelos as dm
        join ubicaciones as u on u.id in  (".implode(',',$request['ubicacion_id']).")
        where u.modelo_id = dm.modelo_id and dm.destajo_id not in (
            --Quitar destajos que fueron eliminados
            select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
            )
        group by dm.destajo_id having count(*) = ".sizeof($request['ubicacion_id'])."
        union all
        --Obtener destajos agregados posteriormente a las ubicaciones
        select destajo_id from detalle_ubicaciones where ubicacion_id in  (".implode(',',$request['ubicacion_id']).") and tipo = 'agregado'
        group by destajo_id having count(*) =  ".sizeof($request['ubicacion_id']).") as destajos
        where destajos.destajo_id not in(
            --Quitar destajos anteriormente comprados
            select d.id
            from destajos as d
            join orden_compra_ubicacion as ocu on ocu.ubicacion_id in  (".implode(',',$request['ubicacion_id']).")
            join orden_compra_destajo as ocd  on ocd.orden_compra_id = ocu.orden_compra_id
            where d.id = ocd.destajo_id
            group by d.id
            order by d.id
            )
            ")))->pluck('destajo_id')->toArray();


        return $destajosId;
    }

    public function notificacionCorreo(string $ordenId, string $status)
    {
        if ($status == "pendiente") {
            $users = User::where('rol','supervisor_obra')->get()->pluck('email');
        }
        else {
            $ordenCompra = OrdenCompra::where('id', $ordenId)->first();
            $user = User::where('id', $ordenCompra->user_id)->first();
            $proveedor = $ordenCompra->proveedor;
            $users = [$user->email, $proveedor->email];
        }

        $request = new MailRequest();
        $request->merge([
            'subject' => 'Orden de compra',
            'status' => $status,
            'link' =>   url('/')."/purchaseOrder/".$ordenId,
            'to' => $users,
            'content' => 'Existe una orden de compra que necesita de su atención.'
        ]);
        $this->mail->notificacion($request);
    }

    public function addFactura(OrdenCompraAddFacturaRequest $request, $user)
    {
        $orden = OrdenCompra::find($request->get('orden_compra_id'));
        $path = $request->file('factura')->storeAs('facturas', Str::uuid()->toString());
        $factura = OrdenCompraFactura::create(
            [
                'orden_compra_id'   => $request->get('orden_compra_id'),
                'folio'             => $request->get('folio'),
                'fecha'             => $request->get('fecha'),
                'importe'           => $request->get('importe'),
                'factura'           => $path
            ]
        );
        if($orden->estatus != 'en proceso' && $orden->estatus != 'completada')
        {
            $data =   $orden->data;
            $data['estatus_en_proceso'] = [
                "user_id" => $user->id,
                "update_at" => Carbon::now()
            ];
            $orden->update([
                'estatus' => 'en proceso',
                'data'    =>  $data
            ]);
        }

        return $factura;
    }

    public function getSpecialOrders(int $model_id)
    {
        $ordenes = collect(DB::select(DB::raw("
            select
                string_agg(DISTINCT ocu.ubicacion_id::varchar, ',') as ubicaciones_id,
                string_agg(distinct dd.destajo_id::varchar, ',') as destajos,
                string_agg(distinct CONCAT(u.manzana, '/', u.numero), ',') as ubicaciones,
                string_agg(distinct (select nombre from destajos where destajos.id = dd.destajo_id)::varchar, ',') as destajo_nombre,
                dd.insumo_id,
                (select nombre from insumos where insumos.id = dd.insumo_id) as nombre,
                (select familia_id from insumos where insumos.id = dd.insumo_id) as familia_id,
                min(un.nombre) as unidad,
                string_agg(distinct oc.folio::varchar, ',') as folio_orden_compra,
                AVG(doc.costo) as costo,
                avg(dd.cantidad) as cantidad_por_ubicacion,
                ROUND(MIN(doc.cantidad/(select COUNT(*) from orden_compra_ubicacion as ocu2 where ocu2.orden_compra_id = doc.orden_compra_id group by ocu.id))::numeric, 2) * COUNT(DISTINCT ocu.ubicacion_id) as cantidad_comprada,
                ROUND(MAX(dd.cantidad - doc.cantidad/(select COUNT(*) from orden_compra_ubicacion as ocu2 where ocu2.orden_compra_id = doc.orden_compra_id group by ocu.id))::numeric, 2) * COUNT(DISTINCT ocu.ubicacion_id)  as cantidad_a_comprar
            from detalle_destajos as dd
            join orden_compra_destajo as ocd on ocd.destajo_id = dd.destajo_id
            join orden_compras as oc on oc.id = ocd.orden_compra_id and oc.created_at < dd.updated_at
            join orden_compra_ubicacion as ocu on ocu.orden_compra_id = oc.id
            join ubicaciones as u on u.modelo_id = ".$model_id." and u.id = ocu.ubicacion_id
            join detalle_orden_compras as doc on doc.insumo_id = dd.insumo_id and doc.orden_compra_id = oc.id
            join insumos as i on i.id = dd.insumo_id
            join unidades as un on i.unidad_id = un.id
            where dd.estatus = 'modificado'
            group by dd.id
            ")));
        return $ordenes;
    }

    public function getFacturas(int $orden_id)
    {
        $facturas = OrdenCompraFactura::where('orden_compra_id', '=', $orden_id)->get();

        return $facturas;
    }

    public function getFactura(int $factura_id) {
        $factura = OrdenCompraFactura::find($factura_id);

        return $factura;
    }

}
