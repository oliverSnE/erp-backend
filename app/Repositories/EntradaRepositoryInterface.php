<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Entrada;

interface EntradaRepositoryInterface {
    /**
     * Funcion para obtener todas las Ordenes de Entrada por el
     * id de la compra de entrada.
     *
     * @param integer $ordenCompraId
     * @return object
     * @throws HttpResponseException
     */
    public function allByOrdenCompra(int $ordenCompraId);

    /**
     * Funcion para obtener una Orden de Entrada usando su id.
     *
     * @param integer $id
     * @return Entrada
     * @throws HttpResponseException
     */
    public function find(int $id);

    /**
     * Funcion para crear una nueva Orden de Entrada a la base de datos.
     *
     * @param Request $request
     * @return Entrada
     * @throws HttpResponseException
     */
    public function create(Request $request);

    /**
     * Funcion para añadir una factura relacionada a una Orden de Entrada.
     *
     * @param Request $request
     * @return Entrada
     * @throws HttpResponseException
     */
    public function addFactura(Request $request);

    /**
     * Funcion para obtener una factura usando el id de una Orden de Entrada.
     *
     * @param integer $id
     * @return Entrada
     * @throws HttpResponseException
     */
    public function getFactura(int $id);


    public function getInsumosRestantes(int $orden_compra_id);
}
