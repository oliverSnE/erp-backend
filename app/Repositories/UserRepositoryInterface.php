<?php
namespace App\Repositories;

use App\Http\Requests\OrdenCompraDestajosRequest;
use App\Http\Requests\UserChangeUbicacionesRequest;
use App\Models\User;
use App\Http\Requests\UserRegisterRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    /**
     * Funcion para guardar un nuevo usuario en la base de datos
     *
     * @param UserRegisterRequest $request
     * @return User
     */
    public function create(UserRegisterRequest $request): User;

    /**
     * Funcion para actualizar un usuario en la base de datos
     *
     * @param UserRegisterRequest $request
     * @return User
     * @throws HttpResponseException
     */
    public function update(UserRegisterRequest $request): User;

    /**
     * Funcion para regresar los roles de usuario existentes
     *
     * @return array
     */
    public function getRoles(): array;

    /**
     * Funcion para obtener un usuario por medio de su id
     *
     * @param integer $id
     * @return User
     * @throws HttpResponseException
     */
    public function findById(int $id): User;

    /**
     * Funcion para obtener todos los usuarios
     *
     * @return object
     * @throws HttpResponseException
     */
    public function findAll(): object;

    /**
     * Funcion para eliminar un usuario por medio de su id
     *
     * @param integer $id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy(int $id);

    /**
     * Funcion para asignar ubicaciones a un usuario en particular
     *
     * @param Request $request
     * @return void
     */
    public function changeUbicaciones(UserChangeUbicacionesRequest $request);

    /**
     * Funcion para obtener las ubicaciones que pueden ser utilizadas por el usuario
     *
     * @param integer $user_id
     * @return void
     */
    public function getUbicacionesDisponibles(int $user_id);
}

