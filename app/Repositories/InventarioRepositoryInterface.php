<?php
namespace App\Repositories;

interface InventarioRepositoryInterface
{
    /**
     * Funcion para obtener la cantidad de insumos que estan en una
     * orden de compra
     *
     * @param integer $idInsumo
     * @return integer
     */
    public function getCantidadInsumo(int $idInsumo);

    public function getInventarioActual(int $fracc_id);
}

