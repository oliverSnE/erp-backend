<?php
namespace App\Repositories;

interface FamiliaRepositoryInterface
{
    /**
     * Funcion para obtener todas las familias de la base de datos.
     *
     * @return object
     */
    public function all();
}

