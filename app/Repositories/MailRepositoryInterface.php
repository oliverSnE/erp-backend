<?php
namespace App\Repositories;

use App\Http\Requests\MailRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

interface MailRepositoryInterface
{
    /**
     * Funcion para mandar un correo como notificacion
     *
     * @param MailRequest $request
     * @return object
     * @throws HttpResponseException
     */
    public function notificacion(MailRequest $request);
}
