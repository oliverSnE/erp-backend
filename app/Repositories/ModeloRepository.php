<?php

namespace App\Repositories;

use App\Http\Requests\ModeloImportRequest;
use App\Http\Requests\ModeloRequest;
use App\Models\DetalleModelo;
use App\Models\Modelo;
use App\Imports\DestajosInsumosImport;
use App\Models\Basic\Ubicacion;
use App\Models\Destajo;
use App\Models\DetalleDestajo;
use App\Models\Familia;
use App\Models\Insumo;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Unidade;
use App\Repositories\ModeloRepositoryInterface;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class ModeloRepository implements ModeloRepositoryInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = Modelo::with('DetalleModelo', 'fraccionamiento')->get();
        if ($modelos) {

            return $modelos;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModeloRequest $request)
    {
        $request->validated();

        $modelos = Modelo::create([
            'nombre' => $request['nombre'],
            'descripcion' => $request['descripcion'],
            'fraccionamiento_id' => $request['fraccionamiento_id']
        ]);
        if ($modelos) {
            $detalle = $request->get('detalle');
            foreach ($detalle as $value) {
                $this->addDetalle($value['destajo_id'], $modelos);
            }


            $response = Modelo::with('DetalleModelo', 'fraccionamiento')->where('id', $modelos->id)->first();
            return $response;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelos = Modelo::with('DetalleModelo.destajo.DetalleDestajo.insumo.unidades', 'DetalleModelo.destajo.DetalleDestajo.insumo.familias', 'fraccionamiento')->where('id', $id)->first();
        if ($modelos) {
            return $modelos;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModeloRequest $request)
    {
        $request->validated();
        $modelos = Modelo::where('id', $request->id)->update([
            'nombre' => $request['nombre'],
            'descripcion' => $request['descripcion'],
            'fraccionamiento_id' => $request['fraccionamiento_id']
        ]);
        $modelos = Modelo::where('id', $request->id)->first();
        if ($modelos) {
            DetalleModelo::where('modelo_id', $request->id)->delete();
            $detalle = $request->get('detalle');
            foreach ($detalle as $value) {
                $this->addDetalle($value['destajo_id'], $modelos);
            }
            $response = Modelo::with('DetalleModelo', 'fraccionamiento')->where('id', $modelos->id)->first();
            return $response;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ubicaciones = Ubicacion::where('modelo_id', $id)->get();
        if (count($ubicaciones) < 1) {

            $modelo = Modelo::find($id);
            if ($modelo->DetalleModelo()) {

                foreach ($modelo->DetalleModelo as $detalle) {

                    DetalleModelo::where('modelo_id',$id)->where('destajo_id', $detalle->destajo_id)->delete();
                    $destajo =  Destajo::find($detalle['destajo_id']);
                    $destajo->DetalleDestajo()->delete();
                    $destajo->forcedelete();
                }


                $modelo->forceDelete();
            }
            return $modelo;
        } else {
            throw new HttpResponseException(
                response()->json([
                    'error' => [
                        "code" => 422,
                        "message" => 'El modelo que intenta eliminar tiene ubicaciones asignadas.'
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }



        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    private function addDetalle($detail, $modelo)
    {
        $modelo->DetalleModelo()->create([
            'destajo_id' => $detail,
            'modelo_id' => $modelo->id
        ]);
    }

    public function import(ModeloImportRequest $request)
    {
        $collection = Excel::toArray(new DestajosInsumosImport, request()->file('file'));
        $lastDestajo = null;
        $totalDestajos = 0;
        $totalInsumos = 0;
        $modelo = Modelo::create([
            'nombre' => $request['nombre'],
            'descripcion' => $request['descripcion'],
            'fraccionamiento_id' => $request['fraccionamiento_id']
        ]);
        $total = 0;
        $fails = [];
        foreach ($collection[0] as $destajo) {
            if ($destajo[1] !== null || !empty($destajo[1]) )
            {
                $total++;
            }
            if ($destajo[1] === 'DESCRIPCIÓN') {
                continue;
            }
            if (is_numeric($destajo[0])) {
                $uni = Unidade::firstOrCreate(['nombre' => $destajo[2]]);
                $lastDestajo = Destajo::create([
                    'nombre' => $destajo[0],
                    'descripcion' => $destajo[1],
                    'costo' => is_numeric($destajo[4]) ? $destajo[4] : 0,
                    'unidad_id' => $uni->id,
                    'tipo' => $destajo[5] == '1' ? 'mano_de_obra' : 'subcontrato',
                    'mostrar' => 'solo_modelo',
                    'cantidad' => is_numeric($destajo[3]) ? $destajo[3] : 0,
                ]);
                $totalDestajos++;
                $modelo->DetalleModelo()->create([
                    'destajo_id' => $lastDestajo->id,
                    'modelo_id' => $modelo->id
                ]);
                continue;
            }
            if ($destajo[5] == 2) {
                if ($lastDestajo->tipo === 'mano_de_obra') {
                    $lastDestajo->update([
                        'tipo' => 'mano_obra_materiales',
                    ]);
                }

                $familia = Familia::firstOrCreate(['nombre' => $destajo[6]]);

                $unidad = Unidade::firstOrCreate(['nombre' => $destajo[2]]);
                $insumo = Insumo::where('nombre', 'like', "%" . $destajo[1] . "%")->first();
                if (!$insumo) {
                    $insumo = Insumo::create([
                        'nombre' => $destajo[1],
                        'unidad_id' => $unidad->id,
                        'familia_id' => $familia->id,
                        'cantidad' => 0,
                        'status' => 1,
                    ]);
                }else {
                    $insumo->update([
                        'nombre' => $destajo[1],
                        'unidad_id' => $unidad->id,
                        'familia_id' => $familia->id,
                        'cantidad' => 0,
                        'status' => 1,
                    ]);
                }

                DetalleDestajo::create([
                    'cantidad' => is_numeric($destajo[3]) ? $destajo[3] : 0,
                    'costo' => is_numeric($destajo[4]) ? $destajo[4] : 0,
                    'insumo_id' => $insumo->id,
                    'destajo_id' => $lastDestajo->id,
                ]);

                $totalInsumos++;
            }

        }

        return $modelo;
    }
}
