<?php

namespace App\Repositories;

use App\Http\Requests\ProveedorRequest;
use App\Models\Familia;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use App\Models\Proveedores;


class ProveedoresRepository implements ProveedoresRepositoryInterface
{
    public function index()
    {
        //return $this->Proveedores->index();

        $proveedores = Proveedores::with('familias')->where('status', '=', '1')->get();

        if($proveedores) {
            return $proveedores;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
    public function find($id)
    {
         $proveedor = Proveedores::with('familias')->where('id', $id)->first();
         if($proveedor) {
             return $proveedor;
         }
         throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un error con el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
    public function store(ProveedorRequest $request)
    {
        $request->validated();
        $proveedor = Proveedores::create([
            'nombre' => $request['nombre'],
            'direccion' => $request['direccion'],
            'municipio' => $request['municipio'],
            'estado' => $request['estado'],
            'estado' => $request['estado'],
            'cp' => $request['cp'],
            'tel' => $request['tel'],
            'email' => $request['email']
        ]);

        $integerIDs = $request['familias'];

        if ( $proveedor )
        {
           $Familias_arr = Familia::find($integerIDs);
            $proveedor->familias()->attach($Familias_arr);
            $response = Proveedores::with('familias')->where('id', $proveedor->id)->first();
           return $response;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }
    public function update(ProveedorRequest $request)
    {
        $request->validated();

        $proveedores = Proveedores::where('id',$request->id)->update([
            'nombre' => $request['nombre'],
            'direccion' => $request['direccion'],
            'municipio' => $request['municipio'],
            'estado' => $request['estado'],
            'estado' => $request['estado'],
            'cp' => $request['cp'],
            'tel' => $request['tel'],
            'email' => $request['email']
        ]);
        $integerIDs = $request['familias'];
        $proveedores = Proveedores::where('id', $request->id)->first();

        if ($proveedores) {

            $proveedores->familias()->detach();
            $familias_arr = Familia::find($integerIDs);
            $proveedores->familias()->attach($familias_arr);

            $response = Proveedores::with('familias')->where('id', $proveedores->id)->get();
            return $response;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
    public function destroy($id)
    {
        //TODO: PENDIENTE EL DESTROY
        $proveedor = Proveedores::find($id);
        $message = 'El provedor no existe';


        if ($proveedor) {
            $proveedor->status = 0;
            if($proveedor->save()) {
                $proveedor->familias()->detach();
                return $proveedor;
            }
            $message = 'Ocurrió un problema con el servidor';
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => $message
                ]
             ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
         );

    }
}
