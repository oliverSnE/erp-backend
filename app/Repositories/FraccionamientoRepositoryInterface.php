<?php
namespace App\Repositories;

use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\FraccionamientoRequest;
use App\Models\Fraccionamiento;

interface FraccionamientoRepositoryInterface
{
    /**
     * Funcion para obtener todos los fraccionamientos de la base de datos.
     *
     * @return object
     */
    public function all();

    /**
     * Funcion para crear un Fraccionamiento dentro de la base de datos.
     *
     * @param FraccionamientoRequest $request
     * @return Fraccionamiento
     * @throws HttpResponseException
     */
    public function create(FraccionamientoRequest $request);

    /**
     * Funcion para actualizar un Fraccionamiento usando su id como 
     * referencia.
     *
     * @param FraccionamientoRequest $request
     * @return Fraccionamiento
     * @throws HttpResponseException
     */
    public function update(FraccionamientoRequest $request);

    /**
     * Funcion para obtener un Fraccionamiento usando su id.
     *
     * @param integer $id
     * @return Fraccionamiento
     * @throws HttpResponseException
     */
    public function find(int $id);

    /**
     * Funcion para obtener las etapas y los modelos de relacionados
     * a un fraccionamiento usando su id
     *
     * @param integer $id
     * @return Array
     */
    public function getEtapasModelos(int $id);

}

