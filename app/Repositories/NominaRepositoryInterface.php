<?php
namespace App\Repositories;

use App\Http\Requests\NominaRequest;

interface NominaRepositoryInterface
{
    public function index();
    public function create(NominaRequest $request);
    public function find($id);
    public function destroy($id);
    public function getNomina(int $fracc_id);
}
