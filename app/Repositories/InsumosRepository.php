<?php
namespace App\Repositories;

use App\Http\Requests\InsumoRegisterRequest;
use App\Models\Familia;
use App\Models\Insumo;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class InsumosRepository implements InsumosRepositoryInterface
{
    public function allInsumos()
    {
        $insumos = Insumo::with('unidades', 'familias')->orderBy('insumos.id', 'ASC')->where('insumos.status',"=",'1')->get();

        if($insumos) {
          return $insumos;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    "code" => 422,
                    "message" => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
    public function insumo($id)
    {
        $insumos = Insumo::with('unidades', 'familias')->where('insumos.id', "=", $id)->where('insumos.status', '=', '1')->first();

        if($insumos) {
           return $insumos;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un error con el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
    public function store(InsumoRegisterRequest $request)
    {

        $request->validated();
        $familia = Familia::firstOrCreate(['nombre' => $request->get('familia')]);
        $insumo = Insumo::create([
            'nombre' => $request->get('nombre'),
            'unidad_id' => $request->get('unidad_id'),
            'familia_id' => $familia->id,
            'cantidad' => 0,
            'status' => 1,
        ]);

        if ($insumo) {
            $response =  Insumo::with('unidades', 'familias')->where('insumos.id', '=', $insumo['id'])->first();
            return $response;
        }

        throw new HttpResponseException(
           response()->json([
               'error' => [
                   'code' => 422,
                   'message' => 'Ocurrió un problema en el servidor'
               ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }
    public function update(InsumoRegisterRequest $request)
    {
        $request->validated();
        $familia = Familia::firstOrCreate(['nombre' => $request->get('familia')]);
        $insumo = Insumo::where('id', $request->id)->update([
            'nombre' => $request->get('nombre'),
            'unidad_id' => $request->get('unidad_id'),
            'familia_id' => $familia->id,
        ]);
        if($insumo)
        {
            $response = Insumo::with('unidades', 'familias')->where('insumos.id', '=', $request->id)->first();
            return $response;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un problema en el servidor'
                ]
             ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
         );
    }
    public function destroy($id)
    {
       // $this->events->destroy($id);
       $insumos = Insumo::find($id);
       $message = 'No existe el insumo';
       if ($insumos) {

           $insumos->status =  0;

           if ($insumos->save()){
                return $insumos;   //200
            }
            $message = 'Ocurrió un problema en el servidor';


       }
       throw new HttpResponseException(
        response()->json([
            'error' => [
                'code' => 422,
                'message' => $message
            ]
         ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
     );

   }
}
