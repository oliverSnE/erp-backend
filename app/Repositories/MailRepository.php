<?php
namespace App\Repositories;

use App\Http\Requests\MailRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use App\Mail\NotificacionEmail;
use Illuminate\Support\Facades\Mail;

class MailRepository implements MailRepositoryInterface
{
    public function notificacion(MailRequest $request)
    {
       // $request->validated();

        $data = [
            'subject' => $request['subject'],
            'link' => $request['link'],
            'to' => $request['to'],
            'content' => $request['content'],
            'status' => $request['status']
        ];

        if ($data) {

                foreach ($data['to'] as $para) {
                    Mail::to($para)->queue(new NotificacionEmail($data));
            }


            return $data;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                "code" => 422,
                "message" => 'Error al enviar el correo'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

}
