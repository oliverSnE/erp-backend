<?php
namespace App\Repositories;

use App\Http\Requests\UserChangeUbicacionesRequest;
use App\Models\User;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Basic\Ubicacion;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    public function create(UserRegisterRequest $request): User
    {
        $request->validated();

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'rol' => $request->get('rol'),
        ]);

        if($user){
            $user->fraccionamientos()->detach();
            $user->fraccionamientos()->attach($request->get('fraccionamientos'));
        }

        return $user;
    }

    public function update(UserRegisterRequest $request): User
    {
        $request->validated();

        $updated = User::where('id', $request->get('id'))->update(
            [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'rol' => $request->get('rol'),
            ]
        );

        if($request->get('password'))
        {
            $updated = User::where('id', $request->get('id'))->update([
                'password' => Hash::make($request->get('password')),
            ]);
        }

        if($updated){
            $user =  User::find($request->get('id'));
            if($user){
                $user->fraccionamientos()->detach();
                $user->fraccionamientos()->attach($request->get('fraccionamientos'));
                return $user->fresh();
            }
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code" => 422,
                "message" => "No fue posible actualizar el usuario"]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function getRoles(): array
    {
        return User::ROL;
    }

    public function findById(int $id): User
    {
        $user = User::with('ubicaciones', 'fraccionamientos')->find($id);

        if($user){
            return $user;
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code" => 404,
                "message" => "El usuario no existe"]
            ], JsonResponse::HTTP_NOT_FOUND)
        );

    }

    public function findAll(): object
    {
        $users = User::all();

        if($users){
            return  $users;
        }

        throw new HttpResponseException(
            response()->json(['errors' => "Hubo un problema al tratar de obtener los usuarios"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function destroy(int $id)
    {
        $destroyed = User::destroy($id);

        if(!$destroyed){
            throw new HttpResponseException(
                response()->json(['error' => [
                    "code" => 422,
                    "message" => "No fue posible eliminar al usuario"]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
    }

    public function changeUbicaciones(UserChangeUbicacionesRequest $request)
    {
        $user = User::find($request->get('user_id'));
        $user->ubicaciones()->detach();
        $user->ubicaciones()->attach($request->get('ubicaciones'));

        return User::with('ubicaciones')->find($request->get('user_id'));
    }

    public function getUbicacionesDisponibles(int $user_id)
    {
        $ubicaciones = Ubicacion::select(
                    'ubicaciones.id',
                    'ubicaciones.manzana',
                    'ubicaciones.numero',
                    'ubicaciones.modelo_id',
                    'ubicaciones.etapa_id',
                    DB::Raw('MAX("etapas"."nombre") as etapa_nombre'),
                    DB::Raw('MAX("fraccionamientos"."nombre") as fracc_nombre'))
            ->join('user_fraccionamiento', 'user_fraccionamiento.user_id', '=', DB::Raw($user_id))
            ->join('etapas', 'etapas.fraccionamiento_id', '=', 'user_fraccionamiento.fraccionamiento_id')
            ->join('fraccionamientos', 'fraccionamientos.id', '=', 'user_fraccionamiento.fraccionamiento_id')
            // ->where('ubicaciones.etapa_id', '=', DB::Raw('"etapas"."id"'))
            ->where('ubicaciones.etapa_id', '=', DB::Raw('"etapas"."id"'))
            ->groupBy('ubicaciones.id')
            ->get();

        return $ubicaciones;
    }
}
