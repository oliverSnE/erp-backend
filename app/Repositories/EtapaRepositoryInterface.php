<?php
namespace App\Repositories;

use App\Http\Requests\EtapaRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Etapa;

interface EtapaRepositoryInterface
{
    /**
     * Funcion para obtener todas las Etapas guardadas en la base de datos.
     *
     * @return object
     */
    public function all();

    /**
     * Funcion para crear una Etapa en la base de datos.
     *
     * @param EtapaRequest $request
     * @return Etapa
     * @throws HttpResponseException
     */
    public function create(EtapaRequest $request);

    /**
     * Funcion para actualizar una Etapa en la base de datos.
     *
     * @param EtapaRequest $request
     * @return Etapa
     * @throws HttpResponseException
     */
    public function update(EtapaRequest $request);

    /**
     * Funcion para obtener una Etapa usando su id.
     *
     * @param integer $id
     * @return Etapa
     * @throws HttpResponseException
     */
    public function find(int $id);

    public function destroy(int $id);

}

