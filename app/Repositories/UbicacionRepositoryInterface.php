<?php
namespace App\Repositories;

use App\Models\Ubicacion;
use App\Http\Requests\UbicacionDestajosRequest;
use App\Http\Requests\UbicacionEmpleadoDestajoRequest;
use App\Http\Requests\UbicacionMultipleRequest;
use App\Http\Requests\UbicacionRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

interface UbicacionRepositoryInterface
{
    /**
     * Funcion para obtener todas las ubicaciones de la base de datos.
     *
     * @return object
     */
    public function all();

    /**
     * Fucion para crear una ubicacion en la base de datos.
     *
     * @param UbicacionRequest $request
     * @return Ubicacion
     */
    public function create(UbicacionRequest $request);

    /**
     * Funcion para crear varias ubicaciones en la base de datos.
     *
     * @param UbicacionMultipleRequest $request
     * @return object
     */
    public function createMultiple(UbicacionMultipleRequest $request);

    /**
     * Funcion para actualizar una ubicacion en la base de datos.
     *
     * @param UbicacionRequest $request
     * @return Ubicacion
     */
    public function update(UbicacionRequest $request);

    /**
     * Funcion para obtener una ubicacion usado su id.
     *
     * @param integer $id
     * @return Ubicacion
     */
    public function find(int $id);

    /**
     * Funcion para agregar un destajo a una ubicacion de la base de datos.
     *
     * @param UbicacionDestajosRequest $request
     * @return Ubicacion
     */
    public function agregarDestajo(UbicacionDestajosRequest $request);

    /**
     * Funcion para eliminar un destajo a una ubicacion de la base de datos.
     *
     * @param UbicacionDestajosRequest $request
     * @return Ubicacion
     */
    public function eliminarDestajo(UbicacionDestajosRequest $request);


    public function getProgresoDeObra(int $ubicacion_id);

    public function getCompras(int $ubicacion_id);

    public function delete(int $ubicacion_id);

    public function ubicacionesByFracc(int $fracc_id);

    public function getEmpeladosDestajo(UbicacionEmpleadoDestajoRequest $request);

}

