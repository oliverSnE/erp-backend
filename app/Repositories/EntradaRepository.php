<?php

namespace App\Repositories;

use App\Models\DetalleOrdenCompra;
use Illuminate\Http\Request;
use App\Models\Entrada;
use App\Models\OrdenCompra;
use App\Repositories\EntradaRepositoryInterface;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonRequest;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class EntradaRepository implements EntradaRepositoryInterface {
    public function allByOrdenCompra(int $ordenCompraId) {
        $entradas = Entrada::with(
            'detalle.detalleOrdenCompra.insumo.Unidades'
        )->where('orden_compra_id', $ordenCompraId)->get();

        if($entradas) {
            return $entradas;
        }
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function find(int $id) {
        $entrada = Entrada::with(
            'detalle.detalleOrdenCompra.insumo.Unidades'
        )->where('id', $id)->first();

        if($entrada) {
            return $entrada;
        }

        throw new HttpResponseException(response()->json([
            'error' => [
                'code' => 422,
                'message' => 'Ocurrió un error en el servidor'
            ]
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    public function create(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $folio = $request->get('folio_factura');
        $ordenCompraId = $request->get('orden_compra_id');
        $entrada = Entrada::create([
            'orden_compra_id'   => $ordenCompraId,
            'folio_factura'     => $folio,
            'user_id'           => $user->id
        ]);

        if($entrada){
            $detalles = $request->get('detalle');
            foreach ($detalles as $detalle) {
                $this->addDetalle($detalle, $entrada);
            }

            $insumosRestantes = $this->getInsumosRestantes($ordenCompraId);
            $data =  $entrada->ordenCompra->data;
            $data['estatus_en_proceso'] = [
                "user_id" => $user->id,
                "update_at" => Carbon::now()
            ];
            $estatus = 'en proceso';
            if(sizeOf($insumosRestantes) < 1){
                $data['estatus_completada'] = [
                    "user_id" => $user->id,
                    "update_at" => Carbon::now()
                ];
                $estatus = 'completada';
            }


            $entrada->ordenCompra->update([
                'estatus' => $estatus,
                'data'    =>  $data
            ]);

            return $entrada;
        }

        throw new HttpResponseException(response()->json([
            'error' => [
                'code' => 422,
                'message' => 'Ocurrió un error en el servidor'
            ]
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    public function addFactura(Request $request) {
        $path = $request->file('factura')->storeAs('facturas', Str::uuid()->toString());
        Entrada::where('id', $request->get('id'))->update(
            [
                'factura' => $path
            ]
        );

        $entrada = Entrada::where('id', $request->get('id'))->first();
        if($entrada->save()) {
            return $entrada;
        }

        throw new HttpResponseException(response()->json([
            'error' => [
                'code' => 422,
                'message' => 'Ocurrió un error en el servidor'
            ]
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    public function getFactura(int $id) {
        $entrada = Entrada::where('id', $id)->first();

        return response()->download(storage_path('app/'.$entrada->factura), $entrada->folio_factura);
    }

    private function addDetalle($detail, $entrada)
    {
        if($detail['costo'] == 0 && $detail['cantidad'] == 0){
            return;
        }
        $entrada->detalle()->create([
            'detalle_orden_compra_id' => $detail['detalle_orden_compra_id'],
            'costo' => $detail['costo'],
            'cantidad' => $detail['cantidad']
        ]);
    }

    public function getInsumosRestantes(int $orden_compra_id)
    {
        $detalle = DetalleOrdenCompra::with('insumo.Unidades')
                ->select( DB::raw('MAX(orden_compras.folio) as orden_folio'),'detalle_orden_compras.*', DB::raw('ROUND((detalle_orden_compras.cantidad -  coalesce(SUM(detalle_entradas.cantidad), 0))::numeric, 2) as cantidad_restante'))
                ->leftJoin('detalle_entradas', 'detalle_entradas.detalle_orden_compra_id', '=','detalle_orden_compras.id')
                ->join('orden_compras', 'orden_compras.id', '=', 'detalle_orden_compras.orden_compra_id')
                ->where('detalle_orden_compras.orden_compra_id','=',$orden_compra_id)
                ->groupBy('detalle_orden_compras.id')
                ->havingRaw('ROUND((detalle_orden_compras.cantidad -  coalesce(SUM(detalle_entradas.cantidad), 0))::numeric, 2) <> 0')
                ->get();
        return $detalle;
    }
}
