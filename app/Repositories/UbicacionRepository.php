<?php
namespace App\Repositories;

use App\Http\Requests\UbicacionDestajosRequest;
use App\Http\Requests\UbicacionEmpleadoDestajoRequest;
use App\Models\DetalleUbicacion;
use App\Http\Requests\UbicacionMultipleRequest;
use App\Http\Requests\UbicacionRequest;
use App\Models\Basic\Destajo;
use App\Models\Basic\Ubicacion as BasicUbicacion;
use App\Models\DetalleDestajo;
use App\Models\DetalleEntrada;
use App\Models\DetalleOrdenCompra;
use App\Models\Empleado;
use App\Models\Modelo;
use App\Models\OrdenCompra;
use App\Models\OrdenCompraFactura;
use App\Models\QuemaDestajos;
use App\Models\QuemaNomina;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use App\Models\Ubicacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UbicacionRepository implements UbicacionRepositoryInterface
{
    public function all()
    {
        $data = Ubicacion::all();

        return $data;
    }

    public function create(UbicacionRequest $request)
    {
        $ubicacion = Ubicacion::create([
            'numero' =>  $request->get('numero'),
            'manzana'=>$request->get('manzana'),
            'etapa_id'=>$request->get('etapa_id'),
            'modelo_id'=>$request->get('modelo_id')
        ]);

        return $ubicacion;
    }

    public function createMultiple(UbicacionMultipleRequest $request)
    {
        $ubicaciones = [];
        foreach ($request->get('numeros') as $key => $numero) {
            $ubicacion = Ubicacion::create([
                'numero' =>  $numero,
                'manzana'=>$request->get('manzana'),
                'etapa_id'=>$request->get('etapa_id'),
                'modelo_id'=>$request->get('modelo_id')
            ]);
            $ubicaciones[] = $ubicacion;
        }

        return $ubicaciones;
    }

    public function agregarDestajo(UbicacionDestajosRequest $request)
    {
        DetalleUbicacion::where([
            'ubicacion_id'=>$request->get('ubicacion_id'),
            'destajo_id'=>$request->get('destajo_id'),])->forceDelete();

        $detalleUbicacion = DetalleUbicacion::create([
            'ubicacion_id'=>$request->get('ubicacion_id'),
            'destajo_id'=>$request->get('destajo_id'),
            'tipo'=> 'agregado'
        ]);



        return $this->find($request->get('ubicacion_id'));
    }

    public function eliminarDestajo(UbicacionDestajosRequest $request)
    {
        DetalleUbicacion::where([
            'ubicacion_id'=>$request->get('ubicacion_id'),
            'destajo_id'=>$request->get('destajo_id'),])->forceDelete();

        $detalleUbicacion = DetalleUbicacion::create([
            'ubicacion_id'=>$request->get('ubicacion_id'),
            'destajo_id'=>$request->get('destajo_id'),
            'tipo'=> 'eliminado'
        ]);

        return $this->find($request->get('ubicacion_id'));
    }

    public function update(UbicacionRequest $request)
    {
        $ubicacion = Ubicacion::find($request->get('id'));
        if($ubicacion) {
            Ubicacion::where('id', $request->get('id'))->update([
                'numero' =>  $request->get('numero'),
                'manzana' =>  $request->get('manzana'),
                'etapa_id' => $request->get('etapa_id')
            ]);

            return $ubicacion->fresh();
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "La ubicacion no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function find(int $id)
    {
        $ubicacion = Ubicacion::where('id', $id)->first();
        if($ubicacion){
            return $ubicacion;
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "La ubicacion no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function getProgresoDeObra(int $ubicacion_id)
    {
        $quemaDestajos = QuemaDestajos::with('destajo')->select(
                    'destajo_id',
                    DB::raw('SUM(quemas_destajos.costo) as pagado'),
                    DB::raw('MAX(destajos.cantidad * destajos.costo) as total'),
                    DB::raw('ROUND((SUM(quemas_destajos.costo)/MAX(destajos.cantidad * destajos.costo)*100)::numeric, 0) as porcentaje'),
                    DB::raw("string_agg(quemas_destajos.costo::varchar, ', ') as pagos_parciales"))
                ->join('destajos', 'destajos.id', '=', 'quemas_destajos.destajo_id')
                ->where('quemas_destajos.ubicacion_id', '=', $ubicacion_id)
                ->groupBy('quemas_destajos.destajo_id')
                ->get();
        $destajosId = collect(DB::select(DB::raw("
            select dm.destajo_id
            from detalle_modelos as dm
            join ubicaciones as u on u.id = ".$ubicacion_id."
            where u.modelo_id = dm.modelo_id and dm.destajo_id not in (
                select destajo_id from detalle_ubicaciones where ubicacion_id = u.id and tipo = 'eliminado'
                )
            group by dm.destajo_id
            union all
            select destajo_id from detalle_ubicaciones where ubicacion_id = ".$ubicacion_id." and tipo = 'agregado'
            group by destajo_id
        ")))->pluck('destajo_id');
        $ubicacionDestajos = Destajo::select( DB::raw('SUM(costo * cantidad) as costo_total'), DB::raw('COUNT(id) as destajos_total'))
                            ->whereIn('id', $destajosId)
                            ->where('tipo', '<>', 'subcontrato')
                            ->get();

        $costoTotalQuemas = $quemaDestajos->sum('pagado');
        $destajosTotalQuemados = $quemaDestajos->where('porcentaje', '>=', '100')->count();

        $progresoTotal = [
            'costo_pagado' => $costoTotalQuemas,
            'costo_total'  => (float)$ubicacionDestajos[0]->costo_total,
            'destajos_terminados' => $destajosTotalQuemados,
            'destajos_total'  => (int)$ubicacionDestajos[0]->destajos_total,
            'progreso_costo' => round($costoTotalQuemas/((float)$ubicacionDestajos[0]->costo_total > 0 ? (float)$ubicacionDestajos[0]->costo_total : 1)*100, 0),
            'progreso_destajos' => round($destajosTotalQuemados/((int)$ubicacionDestajos[0]->destajos_total > 0 ? (float)$ubicacionDestajos[0]->destajos_total : 1)*100, 0),
            'quema_destajos' => $quemaDestajos
        ];
        return $progresoTotal;
    }

    public function getCompras(int $ubicacion_id)
    {
        $ordenes = OrdenCompra::with('facturas')->select('orden_compras.*')
                ->join('orden_compra_ubicacion',"orden_compra_ubicacion.ubicacion_id", '=', DB::raw($ubicacion_id))
                ->where('orden_compras.id', '=', DB::raw('"orden_compra_ubicacion"."orden_compra_id"'))->get();
        $totalFacturado = OrdenCompraFactura::select(DB::raw('SUM(orden_compra_facturas.importe) as facturado'))
                ->join('orden_compra_ubicacion',"orden_compra_ubicacion.ubicacion_id", '=', DB::raw($ubicacion_id))
                ->where('orden_compra_facturas.orden_compra_id', '=', DB::raw('"orden_compra_ubicacion"."orden_compra_id"'))->get()[0]->facturado;
        $totalPedido = DetalleOrdenCompra::select(DB::raw('round(SUM(detalle_orden_compras.costo * detalle_orden_compras.cantidad)::numeric,2) as pedido'))
            ->join('orden_compra_ubicacion',"orden_compra_ubicacion.ubicacion_id", '=', DB::raw($ubicacion_id))
            ->where('detalle_orden_compras.orden_compra_id', '=', DB::raw('"orden_compra_ubicacion"."orden_compra_id"'))->get()[0]->pedido;
        $totalEntregado = DetalleEntrada::select(DB::raw('round(SUM(detalle_entradas.costo * detalle_entradas.cantidad)::numeric,2) as entregado'))
            ->join('orden_compra_ubicacion',"orden_compra_ubicacion.ubicacion_id", '=', DB::raw($ubicacion_id))
            ->join('entradas',"entradas.orden_compra_id", '=', "orden_compra_ubicacion.orden_compra_id")
            ->where('detalle_entradas.entrada_id', '=', DB::raw('"entradas"."id"'))->get()[0]->entregado;

        $compras = [
            "ordenes" => $ordenes,
            "total_facturado"                   => $totalFacturado === null ? 0 : $totalFacturado,
            "total_pedido"                      => $totalPedido === null ? 0 : $totalPedido,
            "total_entregado"                   => $totalEntregado === null ? 0 : $totalEntregado,
            "porcentaje_entregado_facturado"    => round($totalFacturado/($totalEntregado > 0 ? $totalEntregado : 1)*100, 0),
            "porcentaje_entregado_pedido"       => round($totalEntregado/($totalPedido > 0 ? $totalPedido : 1)*100, 0)
        ];

        return $compras;
    }

    public function delete(int $ubicacion_id)
    {
        $ubicacion = Ubicacion::find($ubicacion_id);
        if($ubicacion->ordenCompras()->exists()  || $ubicacion->ordenSalidas()->exists() || $ubicacion->quemas()->exists())
        {
            throw new HttpResponseException(
                response()->json(['error' => [
                    "code"=> 422,
                    "message" => "La ubicacion cuenta con relaciones y no es posible eliminar."
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        $ubicacion->detalle()->forcedelete();
        $ubicacion->forcedelete();
    }

    public function ubicacionesByFracc(int $fracc_id)
    {
        return  BasicUbicacion::select('ubicaciones.*')->join('etapas', 'etapas.fraccionamiento_id', '=', DB::raw($fracc_id))->where('etapa_id', DB::Raw('etapas.id'))->get();
    }

    public function getEmpeladosDestajo(UbicacionEmpleadoDestajoRequest $request)
    { 
        $quemas_id = QuemaDestajos::where('ubicacion_id', '=', $request->get('ubicacion_id'))->where('destajo_id', '=', $request->get('destajo_id'))->get()->pluck('quema_id');
        $empleados_id = QuemaNomina::whereIn('quema_id', $quemas_id)->get()->pluck('empleado_id');
        $empleados = Empleado::whereIn('id', $empleados_id)->get();

        return $empleados;
    }

}

