<?php

namespace App\Repositories;

use App\Http\Requests\OrdenSalidaDestajosEstatusRequest;
use App\Http\Requests\OrdenSalidaRequest;
use App\Models\Basic\Destajo;
use App\Models\Basic\Ubicacion;
use App\Models\DetalleDestajo;
use App\Models\DetalleOrdenSalida;
use App\Models\OrdenSalida;
use App\Models\QuemaDestajos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Maatwebsite\Excel\Concerns\ToArray;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use PHPUnit\Util\Json;

class OrdenSalidaRepository implements OrdenSalidaRepositoryInterface
{

    protected $inventario;

    public function __construct(InventarioRepositoryInterface $inventario)
    {
        $this->inventario = $inventario;
    }

    public function ubicaciones(Request $request)
    {
        $ubicaciones = Ubicacion::select('ubicaciones.*')
            ->join('orden_compra_ubicacion as ocu', 'ocu.ubicacion_id', '=', 'ubicaciones.id')
            ->where('ubicaciones.id', '=', DB::raw("ocu.ubicacion_id"))
            ->where('ubicaciones.etapa_id', '=', $request->get('etapa_id'))
            ->where('ubicaciones.modelo_id', '=', $request->get('modelo_id'))
            ->groupBy('ubicaciones.id')->get();

        return $ubicaciones;
    }

    public function destajos(Request $request)
    {
        $ubicaciones = collect();

        foreach ($request->get('ubicaciones') as $ubicacion_id) {
            $destajos = Destajo::with('DetalleDestajo.insumo.Unidades')->select('destajos.*')
                ->join('orden_compra_ubicacion', function ($join) use ($ubicacion_id) {
                    $join->on('orden_compra_ubicacion.ubicacion_id', "=", DB::raw($ubicacion_id));
                })->join('orden_compra_destajo', function ($join) use ($request) {
                    $join->on('orden_compra_destajo.orden_compra_id', '=', 'orden_compra_ubicacion.orden_compra_id');
                })->where('destajos.id', '=', DB::RAW('"orden_compra_destajo"."destajo_id"'))
                ->groupby('destajos.id')->orderby('destajos.id')->get();
            $destajosArray = [];

            foreach ($destajos as  $keyDestajo => $destajo) {
                $tieneMaterial = false;
                $pagoActual = QuemaDestajos::where('ubicacion_id', '=', $ubicacion_id)
                    ->where('destajo_id', '=', $destajo->id)
                    ->get()->sum('costo');
                $diferencia = round(($destajo->costo * $destajo->cantidad), 3) - $pagoActual;
                foreach ($destajo->DetalleDestajo as $key => $value) {
                    $cantidad = $this->inventario->getCantidadInsumo($value->insumo_id);
                    $sumCantidad = DetalleOrdenSalida::where('detalle_destajo_id', $value->id)->where('ubicacion_id', $ubicacion_id)->groupby('detalle_destajo_id')->sum('cantidad');

                    $value->cantidad -= $sumCantidad;

                    $value->cantidad_inventario = $cantidad;

                    $tieneMaterial = true;
                }
                if (!$tieneMaterial || $diferencia < 1) {
                    $destajos->forget($keyDestajo);
                } else {
                    $destajosArray[] = $destajo;
                }
            }
            $ubicacion = Ubicacion::find($ubicacion_id);
            $ubicacion->setAttribute('destajos', $destajosArray);
            $ubicaciones->push($ubicacion);
        }

        return $ubicaciones;
    }

    public function create(OrdenSalidaRequest $request)
    {
        $request->validated();

        $user = JWTAuth::parseToken()->authenticate();
        $lastOrder = OrdenSalida::latest()->first();
        $orden = OrdenSalida::create([
            'folio' =>   $lastOrder ? ($lastOrder->folio + 1) : 10000,
            'fraccionamiento_id' => $request['fraccionamiento_id'],
            'user_id' => $user->id,
            'estatus' => "creada"
        ]);

        if ($orden) {
            $detalles = $request->get('detalle');
            foreach ($detalles as $value) {
                $this->addDetalle($value, $orden);
            }
        }
        $ordenSalida = OrdenSalida::with('user', 'detalle', 'detalle.detalleDestajos', 'detalle.ubicacion')->where('id', $orden->id)->first();

        return $ordenSalida;
    }

    private function addDetalle($detail, $ordenSalida)
    {
        $ordenSalida->detalle()->create([
            'detalle_destajo_id' => $detail['detalle_destajo_id'],
            'cantidad' => $detail['cantidad'],
            'ubicacion_id' => $detail['ubicacion_id']
        ]);
    }

    public function findAllByFraccionamiento(int $fraccionamientoId)
    {
        return OrdenSalida::with('user','detalle.ubicacion')->where('fraccionamiento_id', $fraccionamientoId)->get();
    }

    public function find(int $id)
    {
        $ordenSalida = OrdenSalida::with('user', 'detalle', 'detalle.detalleDestajos.insumo.Unidades', 'detalle.detalleDestajos.destajo', 'detalle.ubicacion', 'fraccionamiento')->where('id', $id)->first();
        // return $ordenSalida;

        $ordenSalidaFinal = $this->setCustomDetalle($ordenSalida->detalle); // CHECAR ESTO
        $orden = array_merge($ordenSalida->makeHidden('detalle')->toArray(), ['detalle' => $ordenSalidaFinal]);

        return $orden;
    }
    public function findByFolio(string $folio)
    {
        $ordenSalida = OrdenSalida::with('user', 'detalle', 'detalle.detalleDestajos.insumo.Unidades', 'detalle.detalleDestajos.destajo', 'detalle.ubicacion', 'fraccionamiento')->where('folio', $folio)->first();


        if ($ordenSalida) {
            $ordenSalidaFinal = $this->setCustomDetalle($ordenSalida->detalle); // CHECAR ESTO
            $orden = array_merge($ordenSalida->makeHidden('detalle')->toArray(), ['detalle' => $ordenSalidaFinal]);
            return $orden;
        }
        throw new HttpResponseException(
            response()->json(['errors' => "La orden de salida con ese folio no existe"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Cambia la estructura de los detalles de la orden de salida
     */
    private function setCustomDetalle($detalleInput): array
    {
        // Primer foreach obtiene las ubicaciones dentro del atributo Detalle, dejando fuera los atributos Modelo y Etapa.
        $ubicaciones = [];
        foreach ($detalleInput as $value) {
            $value->ubicacion->makeHidden('modelo');
            $ubicaciones[$value->ubicacion_id] = $value->ubicacion->makeHidden('etapa');
        }

        // Segundo foreach obtiene las ubicaciones del arreglo hecho en el primer foreach.
        foreach ($ubicaciones as $key => $ubicacion) {

            // Se crea un arreglo donde se almacenaran los detalles.
            $detalles = [];

            // Tercer foreach obtiene los valores de los detalles, comparando
            foreach ($detalleInput as $value) {

                // Se verifica si existe la ubicacion xd
                if ($value->ubicacion_id == $ubicacion->id) {
                    // $detalle = $value->ubicacion_id;
                    $detalles[] = $value->makeHidden('ubicacion')->toArray();
                }
            }

            $destajos = [];


            foreach ($detalles as $value) {
                if (!array_key_exists($value['detalle_destajos']['destajo_id'], $destajos)) {
                    $destajos[$value['detalle_destajos']['destajo_id']] = $value['detalle_destajos']['destajo'];
                    $destajos[$value['detalle_destajos']['destajo_id']]['estatus'] = $value['estatus'];
                    unset($value['detalle_destajos']['destajo']);
                    $destajos[$value['detalle_destajos']['destajo_id']]['detalle'] = [$value];
                } else {
                    $destajos[$value['detalle_destajos']['destajo_id']]['detalle'][] = $value;
                }
            }

            $ubicaciones[$key] = array_merge($ubicacion->toArray(), ['detalle' => array_values($destajos)]);
        }

        return array_values($ubicaciones);
    }

    public function completed(int $id, $user)
    {
        $ordenSalida =  OrdenSalida::where('id', $id)->first();
        if ($ordenSalida->data === null) {
            $ordenSalida->update([
                'estatus' => 'completada',
                'data'    => [
                    'estatus_completada' => [
                        "user_id" => $user->id,
                        "update_at" => Carbon::now()
                    ]
                ]
            ]);
            return $ordenSalida;
        }
        $data = $ordenSalida->data;
        $data['estatus_completada'] = [
            "user_id" => $user->id,
            "update_at" => Carbon::now()
        ];
        $ordenSalida->update([
            'estatus' => 'completada',
            'data'    => $data
        ]);

        return $ordenSalida;
    }

    public function cancel(int $id, $user)
    {
        $ordenSalida =  OrdenSalida::where('id', $id)->first();
        if ($ordenSalida->data === null) {
            $ordenSalida->update([
                'estatus' => 'cancelada',
                'data'    => [
                    'estatus_cancelada' => [
                        "user_id" => $user->id,
                        "update_at" => Carbon::now()
                    ]
                ]
            ]);
            return $ordenSalida->refresh();
        }
        $data = $ordenSalida->data;
        $data['estatus_cancelada'] = [
            "user_id" => $user->id,
            "update_at" => Carbon::now()
        ];
        $ordenSalida->update([
            'estatus' => 'cancelada',
            'data'    => $data
        ]);

        return $ordenSalida->refresh();
    }

    public function ready(int $id, $user)
    {
        $ordenSalida =  OrdenSalida::where('id', $id)->first();
        if ($ordenSalida->data === null) {
            $ordenSalida->update([
                'estatus' => 'lista',
                'data'    => [
                    'estatus_lista' => [
                        "user_id" => $user->id,
                        "update_at" => Carbon::now()
                    ]
                ]
            ]);
            return $ordenSalida;
        }
        $data = $ordenSalida->data;
        $data['estatus_lista'] = [
            "user_id" => $user->id,
            "update_at" => Carbon::now()
        ];
        $ordenSalida->update([
            'estatus' => 'lista',
            'data'    => $data
        ]);
        $ordenSalida =  OrdenSalida::where('id', $id)->first();

        return $ordenSalida;
    }

    public function processing(int $id, $user)
    {
        $ordenSalida =  OrdenSalida::where('id', $id)->first();
        if ($ordenSalida->data === null) {
            $ordenSalida->update([
                'estatus' => 'en preparacion',
                'data'    => [
                    'estatus_preparacion' => [
                        "user_id" => $user->id,
                        "update_at" => Carbon::now()
                    ]
                ]
            ]);
            return $ordenSalida;
        }
        $data = $ordenSalida->data;
        $data['estatus_preparacion'] = [
            "user_id" => $user->id,
            "update_at" => Carbon::now()
        ];
        $ordenSalida->update([
            'estatus' => 'lista',
            'data'    => $data
        ]);
        $ordenSalida =  OrdenSalida::where('id', $id)->first();

        return $ordenSalida;
    }

    public function getDestajoUbicacion(int $destajo_id, int $ubicacion_id, int $orden_salida_id)
    {
        $detalle = DetalleOrdenSalida::with('ubicacion', 'detalleDestajos.insumo.Unidades', 'detalleDestajos.destajo')
            ->join('detalle_destajos as dd', 'dd.destajo_id', '=', DB::raw($destajo_id))
            ->where('ubicacion_id', '=', $ubicacion_id)
            ->where('detalle_destajo_id', '=', DB::raw('dd.id'))
            ->where('orden_salida_id', '=',  $orden_salida_id)->get();

        return $detalle;
    }

    public function changeEstatus(OrdenSalidaDestajosEstatusRequest $request)
    {
        DetalleOrdenSalida::join('detalle_destajos as dd', 'dd.destajo_id', '=', DB::raw($request->get('destajo_id')))
            ->where('ubicacion_id', '=', $request->get('ubicacion_id'))
            ->where('detalle_destajo_id', '=', DB::raw('dd.id'))
            ->where('orden_salida_id', '=',  $request->get('orden_salida_id'))
            ->update(['estatus' => 'recibida']);

        $orden = $this->find($request->get('orden_salida_id'));
        if (!DetalleOrdenSalida::where('orden_salida_id', '=', $request->get('orden_salida_id'))->where('estatus', '=', 'creada')->exists()) {
            $user = JWTAuth::parseToken()->authenticate();
            $this->completed((int) $request->get('orden_salida_id'),  $user);
        }
        return $orden;
    }
}
