<?php
namespace App\Repositories;

use Illuminate\Http\Request;

interface ImportRepositoryInterface {
    /**
     * Funcion para importar un archivo Excel para los insumos.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function import(Request $request);

    /**
     * Funcion para mostrar informacion sobre un error al importar.
     *
     * @param \Throwable $e
     * @throws Throwable
     */
    public function onError(\Throwable $e);
}