<?php
namespace App\Repositories;

use App\Models\Insumo;
use App\Http\Requests\InsumoRegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;

interface InsumosRepositoryInterface
{
    /**
     * Funcion para obtener todos los insumos de la base de datos.
     *
     * @return object
     */
    public function allInsumos();

    /**
     * Funcion para obtener un Insumo por usado su id.
     *
     * @param integer $id
     * @return Insumo
     * @throws HttpResponseException
     */
    public function insumo($id);

    /**
     * Funcion para almacenar un nuevo Insumo en la base de datos.
     *
     * @param InsumoRegisterRequest $request
     * @return Insumo
     * @throws HttpResponseException
     */
    public function store(InsumoRegisterRequest $request);

    /**
     * Funcion para actualizar el Insumo mandando nuevos datos como
     * request al metodo.
     *
     * @param InsumoRegisterRequest $request
     * @return Insumo
     * @throws HttpResponseException
     */
    public function update(InsumoRegisterRequest $request);

    /**
     * Funcion para eliminar un Insumo dentro de la base de datos.
     *
     * @param integer $id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy($id);
}

