<?php

namespace App\Repositories;

use App\Models\DetalleNomina;
use App\Http\Requests\NominaRequest;
use App\Models\Quema;
use App\Models\QuemaDestajos;
use App\Models\QuemaNomina;
use App\Models\Nomina;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class NominaRepository implements NominaRepositoryInterface
{

    public function find($id)
    {
        $nomina = Nomina::find($id);

        $quema_id = [];

        foreach($nomina->detalles as $detalle) {

            $quema_id[] = $detalle->quema_id;
        }

        $quemas = Quema::whereIn('id', $quema_id)->get();
        $empleados = QuemaNomina::with('empleado')->whereIn('quema_id', $quema_id)->groupBy('empleado_id')->select(DB::raw('sum(pago) as pago, empleado_id'))->get();
        $destajos = QuemaDestajos::with('destajo', 'ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id', 'destajo_id')->select(DB::raw('sum(costo) as pago, ubicacion_id, destajo_id'))->orderBy('ubicacion_id', 'ASC')->get();
        $totalEmpleado = $empleados->sum('pago');
        $totalesUbicacion = QuemaDestajos::with('ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id')->select(DB::raw('sum(costo) as pago, ubicacion_id'))->orderBy('ubicacion_id', 'ASC')->get();
        $quemEmpDes = ['nomina' => $nomina, 'quemas' => $quemas, 'empleados' => $empleados,'totalesUbicacion' => $totalesUbicacion, 'totalEmpleados' => $totalEmpleado, 'destajos' => $destajos];
        return $quemEmpDes;
    }

    public function getNomina(int $fracc_id)
    {
        $quemas = DB::select(DB::raw(
            "select quemas.*
            from quemas
            inner join
                quemas_destajos on quemas.id = quemas_destajos.quema_id
            where not exists
                (select * from quemas_destajos where quemas.id = quemas_destajos.quema_id and estatus = 'creada')
            and
                quemas.fraccionamiento_id = ".$fracc_id."
            and
                quemas.estatus = 'creada' group by quemas.id"));


        if($quemas) {

            $quema_id = [];

            foreach($quemas as $quema) {

                $quema_id[] = $quema->id;
            }

            $quemas = Quema::whereIn('id', $quema_id)->get();
            $empleados = QuemaNomina::with('empleado')->whereIn('quema_id', $quema_id)->groupBy('empleado_id')->select(DB::raw('sum(pago) as pago, empleado_id'))->get();
            $destajos = QuemaDestajos::with('destajo', 'ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id', 'destajo_id')->select(DB::raw('sum(costo) as pago, ubicacion_id, destajo_id'))->orderBy('ubicacion_id', 'ASC')->get();
            $totalEmpleado = $empleados->sum('pago');
            $totalesUbicacion = QuemaDestajos::with('ubicacion')->whereIn('quema_id', $quema_id)->groupBy('ubicacion_id')->select(DB::raw('sum(costo) as pago, ubicacion_id'))->orderBy('ubicacion_id', 'ASC')->get();
            $quemEmpDes = ['quemas' => $quemas, 'empleados' => $empleados,'totalesUbicacion' => $totalesUbicacion, 'totalEmpleados' => $totalEmpleado, 'destajos' => $destajos];

            return $quemEmpDes;
        }

        return [];
    }

    public function index()
    {
        $nominas = Nomina::all();
        return $nominas;
    }

    public function create(NominaRequest $request)
    {
        $request->validated();
        $nomina = Nomina::create([
            'nombre' => $request['nombre'],
            'fecha' => $request['fecha'],
            'pagado' => $request['pagado'],
            'descuento' => $request['descuento']

        ]);
        if($nomina) {

            $quemas = $request->get('quemas');

            foreach ($quemas as $quema) {

                $quemasCreadas = Quema::where('id', $quema)->where('estatus','creada')->first();

                if($quemasCreadas) {

                    $this->addDetalle($quema, $nomina);
                    Quema::where('id', $quemasCreadas->id)->update([
                        'estatus' => 'pagada'
                    ]);
                }
            }
            $response = Nomina::with('detalles')->where('id', $nomina->id)->first();
            return $response;
        }
        throw new HttpResponseException(
            response()->json(['errors' => "Ocurrio un error en el servidor"],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function destroy($id)
    {

        $detalleNomina = DetalleNomina::where('nomina_id', '=', $id)->get();

        if(count($detalleNomina) >= 1) {


           foreach($detalleNomina as $dn) {
               DetalleNomina::where('nomina_id', $dn->nomina_id)->delete();
           }
           $nomina = Nomina::where('id', $id)->delete();

           if($nomina) {
               foreach($detalleNomina as $quema)
                Quema::where('id', $quema->quema_id)->update([
                    'estatus' => 'creada'
                ]);
                return;
           }

        }
        throw new HttpResponseException(
            response()->json(['data' => [
                'code' => 404,
                'message' => 'La nomina no existe.'
            ]],
            JsonResponse::HTTP_NOT_FOUND)
        );


    }

    private function addDetalle($detalle, $nomina) {

        DetalleNomina::create([
            'quema_id' => $detalle,
            'nomina_id' => $nomina->id
        ]);
    }
}
