<?php
namespace App\Repositories;

use App\Http\Requests\FraccionamientoRequest;
use App\Models\Basic\Etapa;
use App\Models\Basic\Modelo;
use App\Models\Fraccionamiento;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


class FraccionamientoRepository implements FraccionamientoRepositoryInterface
{
    public function all()
    {
        $data = Fraccionamiento::all();

        return $data;
    }

    public function create(FraccionamientoRequest $request)
    {
        $request->validated();

        $fraccionamiento = Fraccionamiento::create([
            'nombre' =>  $request->get('nombre'),
            'direccion' => $request->get('direccion'),
            'razon_social_factura'=>$request->get('razon_social_factura'),
            'rfc_factura'=>$request->get('rfc_factura'),
            'cp_factura'=>$request->get('cp_factura'),
            'direccion_factura'=>$request->get('direccion_factura'),
        ]);

        if($fraccionamiento->setLogo($request->file('logo'))){
            return $fraccionamiento;
        }

        throw new HttpResponseException(
            response()->json( ["error" => [
                "code"=> 422,
                "message" => "Ocurrio un error en el servidor al tratar de crear el fraccionamiento."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function update(FraccionamientoRequest $request)
    {
        $request->validated();

        $fraccionamiento = Fraccionamiento::find($request->get('id'));
        if($fraccionamiento) {
            Fraccionamiento::where('id', $request->get('id'))->update([
                'nombre' =>  $request->get('nombre'),
                'direccion' => $request->get('direccion'),
                'razon_social_factura'=>$request->get('razon_social_factura'),
                'rfc_factura'=>$request->get('rfc_factura'),
                'cp_factura'=>$request->get('cp_factura'),
                'direccion_factura'=>$request->get('direccion_factura'),
            ]);

            if(!$request->hasFile('logo')){
                return $fraccionamiento->fresh();
            }
            if($fraccionamiento->unsetLogo() && $fraccionamiento->setLogo($request->file('logo'))){
                return $fraccionamiento->fresh();
            }

            throw new HttpResponseException(
                response()->json(['error' => [
                    "code"=> 422,
                    "message" => "Ocurrio un error en el servidor al tratar de actualizar el fraccionamiento."
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "El fraccionamiento no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function find(int $id)
    {
        $fraccionamiento = Fraccionamiento::with('etapas','modelos')->where('id', $id)->first();
        if($fraccionamiento){
            return $fraccionamiento;
        }

        throw new HttpResponseException(
            response()->json(['error' => [
                "code"=> 422,
                "message" => "El fraccionamiento no existe."
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function getEtapasModelos(int $id){
        $etapas = Etapa::where('fraccionamiento_id', $id)->get();
        $modelos = Modelo::where('fraccionamiento_id', $id)->get();

        return ['etapas' => $etapas, 'modelos' => $modelos];
    }

}
