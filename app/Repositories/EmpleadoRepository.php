<?php
namespace App\Repositories;

use App\Models\Empleado;
use App\Http\Requests\EmpleadoRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class EmpleadoRepository implements EmpleadoRepositoryInterface
{
    public function create(EmpleadoRequest $request): Empleado
    {
        $request->validated();

        $empleado = Empleado::create([
            'nombre' => $request->get('nombre'),
            'rfc' => $request->get('rfc'),
            'direccion' => $request->get('direccion'),
            'puesto' => $request->get('puesto'),
            'nss' => $request->get('nss'),
            'alias' => $request->get('alias'),
        ]);

        return $empleado;
    }

    public function update(EmpleadoRequest $request): Empleado
    {
        $request->validated();

        $updated = Empleado::where('id', $request->get('id'))->update(
            [
                'nombre' => $request->get('nombre'),
                'rfc' => $request->get('rfc'),
                'direccion' => $request->get('direccion'),
                'puesto' => $request->get('puesto'),
                'nss' => $request->get('nss'),
                'alias' => $request->get('alias'),
            ]
        );

        if($updated){
            return Empleado::find($request->get('id'));
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function findById(int $id): Empleado
    {
        $empleado = Empleado::find($id);

        if($empleado){
            return $empleado;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'El empleado no existe'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function findAll(): object
    {
        $empleado = Empleado::all();

        if($empleado){
            return  $empleado;
        }

        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'code' => 422,
                    'message' => 'Ocurrió un error en el servidor'
                ]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function destroy(int $id)
    {
        $destroyed = Empleado::find($id)->delete($id);

        if(!$destroyed){
            throw new HttpResponseException(
                response()->json([
                    'error' => [
                        'code' => 422,
                        'message' => 'Ocurrió un error en el servidor'
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
    }

    public function puestos(): object
    {
        $puestos = Empleado::distinct()->get(['puesto'])->pluck('puesto');
        return  $puestos;
    }
}
