<?php
namespace App\Repositories;
use App\Models\Empleado;
use App\Http\Requests\EmpleadoRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

interface EmpleadoRepositoryInterface
{
    /**
     * Funcion para guardar un nuevo empleado en la base de datos
     *
     * @param EmpleadoRequest $request
     * @return Empleado
     */
    public function create(EmpleadoRequest $request): Empleado;

    /**
     * Funcion para actualizar un empleado en la base de datos
     *
     * @param EmpleadoRequest $request
     * @return Empleado
     * @throws HttpResponseException
     */
    public function update(EmpleadoRequest $request): Empleado;

    /**
     * Funcion para obtener un empleado por medio de su id
     *
     * @param integer $id
     * @return Empleado
     * @throws HttpResponseException
     */
    public function findById(int $id): Empleado;

    /**
     * Funcion para obtener todos los empleados
     *
     * @return object
     * @throws HttpResponseException
     */
    public function findAll(): object;

    /**
     * Funcion para eliminar un empleado por medio de su id
     *
     * @param integer $id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy(int $id);


    /**
     * Regresa los puestos que han sido creado
     *
     * @return object
     */
    public function puestos(): object;
}

