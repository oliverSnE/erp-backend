<?php

namespace App\Repositories;

use App\Models\Proveedores;
use App\Http\Requests\ProveedorRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

interface ProveedoresRepositoryInterface
{
    /**
     * Funcion para obtener todos los proveedores de la base de datos.
     *
     * @return object
     * @throws HttpResponseException
     */
    public function index();

    /**
     * Funcion para obtener un proveedor usando su id.
     *
     * @param integer $id
     * @return Proveedores
     */
    public function find($id);

    /**
     * Funcion para almacenar un proveedor en la base de datos.
     *
     * @param ProveedorRequest $request
     * @return Proveedores
     */
    public function store(ProveedorRequest $request);

    /**
     * Funcion para actualizar un proveedor en la base de datos.
     *
     * @param ProveedorRequest $request
     * @return Proveedores
     */
    public function update(ProveedorRequest $request);

    /**
     * Funcion para eliminar un proveedor de la base de datos usando su id.
     *
     * @param integer $id
     * @return void
     */
    public function destroy($id);



}
