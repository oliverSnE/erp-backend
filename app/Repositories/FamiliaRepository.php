<?php
namespace App\Repositories;
use App\Models\Familia;

class FamiliaRepository implements FamiliaRepositoryInterface
{
    public function all()
    {
        $data = Familia::all();

        return $data;
    }

}
