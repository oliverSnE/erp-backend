<?php
namespace App\Repositories;

use App\Http\Requests\CostoDestajoRequest;
use App\Http\Requests\DestajoRequest;
use App\Http\Requests\DetalleDestajoRequest;
use App\Models\Destajo;
use App\Models\DetalleDestajo;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;

interface DestajoRepositoryInterface
{
    /**
     * Funcion para obtener todos los destajos.
     *
     * @return object
     * @throws HttpResponseException
     */
    public function index();

    /**
     * Funcion para obtener un Destajo usando su id.
     *
     * @param integer $id
     * @return Destajo
     * @throws HttpResponseException
     */
    public function show($id): Destajo;

    /**
     * Funcion para obtener el detalle de un destajo.
     *
     * @param integer $detalle_id
     * @return DetalleDestajo
     * @throws HttpResponseException
     */
    public function getDetalleById($detalle_id);

    /**
     * Funcion para guardar un Destajo en la base de datos.
     *
     * @param DestajoRequest $request
     * @return Destajo
     * @throws HttpResponseException
     */
    public function create(DestajoRequest $request);

    /**
     * Funcion para actualizar un Destajo en la base de datos.
     *
     * @param DestajoRequest $request
     * @return Destajo
     * @throws HttpResponseException
     */
    //public function update(DestajoRequest $request);

    /**
     * Funcion para editar un detalle de un Destajo de la base de datos.
     *
     * @param Request $request
     * @return DetalleDestajo
     * @throws HttpResponseException
     */
    public function editDetalle(DetalleDestajoRequest $request);

    public function destajosByUbicacion(Request $request, $user);
    public function destajosBySubcontrato(Request $request, $user);
    public function costoDestajoByUbicacion(CostoDestajoRequest $request);

    /**
     * Funcion para eliminar de la base de datos un Destajo usando su id.
     *
     * @param integer $id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy($id);

    /**
     * Funcion para obtener los tipos de un Destajo.
     *
     * @return tipo
     */
    public function getTipos();

    public function searchByName(Request $request);

}
