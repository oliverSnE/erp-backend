<?php
namespace App\Repositories;

use App\Models\Insumo;
use Illuminate\Support\Facades\DB;

class InventarioRepository implements InventarioRepositoryInterface
{
    public function getCantidadInsumo(int $idInsumo)
    {
        $cantidad = collect(DB::select(DB::raw("
            select SUM(de.cantidad) - coalesce((
                            select SUM(dos.cantidad)
                            from detalle_orden_salidas dos
                            join detalle_destajos dd on dd.id = dos.detalle_destajo_id and dd.insumo_id = ".$idInsumo."
                            group by dd.insumo_id
                        ), 0) as cantidad
            from detalle_entradas as de
            join detalle_orden_compras as doc on doc.id = de.detalle_orden_compra_id and doc.insumo_id =  ".$idInsumo."
            group by doc.insumo_id
        ")))->pluck('cantidad')->toArray();
        if(!$cantidad){
            $cantidad = [0];
        }

        return $cantidad[0];
    }

    public function getInventarioActual(int $fracc_id)
    {
        $insumosEnInventario = Insumo::with('Unidades', 'Familias')
                    ->from( 'insumos as i' )
                    ->select(DB::raw("
                        i.*,
                                    coalesce((
                                        select coalesce(SUM(doc.cantidad), 0) as cantidad_pedida
                                        from insumos as i1
                                        left join orden_compras as oc on oc.fraccionamiento_id = ".$fracc_id."
                                        left join detalle_orden_compras as doc on doc.insumo_id = i1.id and doc.orden_compra_id = oc.id
                                        where i1.tipo = 'normal' and i1.id = i.id
                                        group by i1.id
                                    ), 0) as cantidad_pedida,
                                    coalesce((
                                        select coalesce(SUM(de.cantidad), 0) as cantidad_entregada
                                        from orden_compras as oc
                                        join detalle_orden_compras as doc on doc.orden_compra_id = oc.id
                                        left join detalle_entradas as de on de.detalle_orden_compra_id = doc.id
                                        join insumos as i2 on i2.id = doc.insumo_id
                                        where oc.fraccionamiento_id = ".$fracc_id." and i2.id = i.id
                                        group by i2.id
                                        order by i2.id
                                    ),0) as cantidad_recibida,
                                    coalesce((
                                        select coalesce(SUM(dos.cantidad), 0) as cantidad_pendiente_de_entregada
                                        from orden_salidas as os
                                        join detalle_orden_salidas as dos on dos.orden_salida_id = os.id and dos.estatus = 'creada'
                                        join detalle_destajos as dd on dd.id = dos.detalle_destajo_id
                                        join insumos as i3 on i3.id = dd.insumo_id
                                        where os.fraccionamiento_id = ".$fracc_id." and i3.id = i.id
                                        group by i3.id
                                        order by i3.id
                                    ),0) as cantidad_pendiente_en_obra,
                                        coalesce((
                                        select coalesce(SUM(dos.cantidad), 0) as cantidad_entregada
                                        from orden_salidas as os
                                        join detalle_orden_salidas as dos on dos.orden_salida_id = os.id and dos.estatus = 'recibida'
                                        join detalle_destajos as dd on dd.id = dos.detalle_destajo_id
                                        join insumos as i4 on i4.id = dd.insumo_id
                                        where os.fraccionamiento_id = ".$fracc_id." and i4.id = i.id
                                        group by i4.id
                                        order by i4.id
                                    ),0) as cantidad_entregada
                "))
                ->where('i.tipo', '=', DB::raw("'normal'"))->get();

        return $insumosEnInventario;
    }
}
