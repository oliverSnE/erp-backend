<?php
namespace App\Repositories;

use App\Models\Modelo;
use App\Http\Requests\ModeloImportRequest;
use App\Http\Requests\ModeloRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

interface ModeloRepositoryInterface
{
    /**
     * Funcion para obtener todos los modelos de la base de datos.
     *
     * @return object
     */
    public function index();

    /**
     * Funcion para obtener un Modelo usando su id.
     *
     * @param integer $id
     * @return Modelo
     * @throws HttpResponseException
     */
    public function show($id);

    /**
     * Funcion para almacenar un nuevo Modelo en la base de datos.
     *
     * @param ModeloRequest $request
     * @return Modelo
     * @throws HttpResponseException
     */
    public function store(ModeloRequest $request);

    /**
     * Funcion para actualizar un Modelo mandando data como request.
     *
     * @param ModeloRequest $request
     * @return Modelo
     * @throws HttpResponseException
     */
    public function update(ModeloRequest $request);

    /**
     * Funcion para eliminar un Modelo de la base de datos usando su id.
     *
     * @param integer id
     * @return void
     * @throws HttpResponseException
     */
    public function destroy($id);

    /**
     * Funcion para importar un archivo de Excel con modelos.
     *
     * @param ModeloImportRequest $request
     * @return Modelo
     * @throws HttpResponseException
     */
    public function import(ModeloImportRequest $request);
}
