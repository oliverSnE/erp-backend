<?php
namespace App\Repositories;

use App\Models\Cuadrilla;
use App\Http\Requests\CuadrillaRequest;
use App\Models\CuadrillaEmpleado;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class CuadrillaRepository implements CuadrillaRepositoryInterface
{
    public function create(CuadrillaRequest $request): Cuadrilla
    {
        $request->validated();

        $cuadrilla = Cuadrilla::create([
            'nombre' => $request['nombre']
        ]);
        if ($cuadrilla) {
            $detalles = $request->get('detalle');
            foreach ($detalles as $detalle) {
                $this->addDetalle($detalle, $cuadrilla);
            }

            $response = Cuadrilla::with('detalle')->where('id', $cuadrilla->id)->first();
            return $response;
        }
        throw new HttpResponseException(
            response()->json(['errors' => "Ocurrio un error en el servidor"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function update(CuadrillaRequest $request)
    {
        $request->validated();

        $updated = Cuadrilla::where('id', $request->id)->update([
            'nombre' => $request['nombre']
        ]
        );

        if($updated){

            CuadrillaEmpleado::where('cuadrilla_id', $request->id)->delete();

            $detalles = $request->get('detalle');
            $cuadrilla = Cuadrilla::find($request->id);
            foreach ($detalles as $detalle) {

                $this->addDetalle($detalle, $cuadrilla);
            }
            $response = Cuadrilla::with('detalle')->where('id', $request->id)->first();

            return $response;
        }

        throw new HttpResponseException(
            response()->json(['errors' => "Ocurrio un error en el servidor"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function findById(int $id)
    {
        //$Cuadrilla = Cuadrilla::find($id);
        $cuadrilla = Cuadrilla::with('detalle.empleado')->where('id', $id)->first();

        if($cuadrilla){
            return $cuadrilla;
        }

        throw new HttpResponseException(
            response()->json(['errors' => "La Cuadrilla no existe"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }

    public function findAll(): object
    {
        $cuadrilla = Cuadrilla::with('detalle.empleado')->get();

        if($cuadrilla){
            return  $cuadrilla;
        }

        throw new HttpResponseException(
            response()->json(['errors' => "Hubo un problema al tratar de obtener los Cuadrillas"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    public function destroy(int $id)
    {
        $cuadrillaDeleted = CuadrillaEmpleado::where('cuadrilla_id', $id)->delete();
        if ($cuadrillaDeleted) {

            $destroyed = Cuadrilla::find($id)->delete($id);
        }


        if(!$destroyed){
            throw new HttpResponseException(
                response()->json(['errors' => "Hubo un problema al tratar de borrar la cuadrilla"], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
    }

    private function addDetalle($detalle, $cuadrilla)
    {
        $cuadrilla->detalle()->create([
            'cuadrilla_id' => $cuadrilla->id,
            'empleado_id' => $detalle['empleado_id'],
            'is_jefe' => (bool)$detalle['is_jefe']
        ]);
    }

}
