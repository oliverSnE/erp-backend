<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="7">ORDENES DE COMPRA (FACTURA POR UBICACIÓN)</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">FECHA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">ORDEN DE COMPRA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">PROVEEDOR</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">IMPORTE (IVA INCLUIDO)</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">NO. FACTURA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">IMPORTE (IVA INCLUIDO)</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">UBICACIONES</th>
        </tr>
    </thead>
    <tbody style="color: black; padding: 5px;">
        <table>
            <thead>
                <tr>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Destajos por ubicaciones y materiales</th>
                </tr>
                <tr>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Manzana</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Número</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Destajo</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Insumo</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Cantidad presupuestada</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Cantidad utilizada</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Mano de obra pagada</th>
                    <th style="text-align: center;border: 1px solid #000; font-weight: bold">Mano de obra presupuestada</th>
                </tr>
            </thead>
            <tbody>

        @foreach ($facturas as $factura)
            <?php $factura->fecha = date('d/m/Y'); ?>
                <tr>
                    <td>{{$factura->fecha}}</td>
                    <td style="text-align: center;">{{$factura->folio}}</td>
                    <td>{{$factura->proveedor}}</td>
                    <td style="text-align: center;">{{$factura->importe * 1.16}}</td>
                    <td style="text-align: center;">{{$factura->no_factura}}</td>
                    <td style="text-align: center;">{{$factura->importe_ubicacion * 1.16}}</td>
                    <td>{{$factura->ubicaciones}}</td>
                </tr>
        @endforeach
    </tbody>
</table>
