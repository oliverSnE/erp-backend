<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">Fraccionamiento</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">Direccion</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">{{$fraccionamiento->nombre}}</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">{{$fraccionamiento->direccion}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="8">AVANCES POR UBICACION</th>
        </tr>
        <tr>
            <th style="text-align: left;border: 1px solid #000;font-weight: bold;" colspan="8">Fecha: {{$fecha}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2" >Ubicación</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">Material</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">Mano de obra</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Manzana</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Lote</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Comprado</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Total</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Porcentaje</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Pagado</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Total</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Porcentaje</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $ubicacion)
        <tr>
            <td style="width: 15px;text-align: center;">{{$ubicacion["ubicacion"]->manzana}}</td>
            <td style="width: 15px;text-align: center;">{{$ubicacion["ubicacion"]->numero}}</td>
            <td style="width: 15px;text-align: center;">${{number_format($ubicacion["materialUsado"], 2)}}</td>
            <td style="width: 15px;text-align: center;">${{number_format($ubicacion["totalMaterial"], 2)}}</td>
            <td style="width: 15px;text-align: center;">{{$ubicacion["porcentajeMaterial"]}}%</td>
            <td style="width: 15px;text-align: center;">${{number_format($ubicacion["costoTotalQuemas"], 2)}}</td>
            <td style="width: 15px;text-align: center;">${{number_format($ubicacion["totalQuemas"], 2)}}</td>
            <td style="width: 15px;text-align: center;">{{$ubicacion["porcentajeQuemas"]}}%</td>
        </tr>
    @endforeach
    </tbody>
</table>
