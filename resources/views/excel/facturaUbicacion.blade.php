<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="7">FACTURA POR UBICACIÓN</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="2">UBICACIÓN </th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">FECHA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">PROVEEDOR</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">No. FACTURA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">IMPORTE (IVA INCLUIDO)</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">IMPORTE TOTAL (IVA INCLUIDO)</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" >MANZANA</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">LOTE</th>
        </tr>
    </thead>
    <tbody style="color: black; padding: 5px;">


        @foreach ($arrayfacturas as $factura)
            <?php $factura->fecha = date('d/m/Y'); ?>
                <tr>
                    <td style="text-align: center;">{{$factura->manzana}}</td>
                    <td style="text-align: center;">{{$factura->numero}}</td>
                    <td style="text-align: center;">{{$factura->fecha}}</td>
                    <td>{{$factura->nombre}}</td>
                    <td style="text-align: center;">{{$factura->folio}}</td>
                    <td style="text-align: center;">{{$factura->subtotal}}</td>
                    <td style="text-align: center;">{{$factura->importe * 1.16}}</td>
                </tr>
                <?php $manzana = $factura->manzana; $numero = $factura->numero;    ?>
        @endforeach
    </tbody>
</table>
