<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9">Fraccionamiento</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">{{$fraccionamiento->nombre}}</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="6">{{$fraccionamiento->direccion}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9">NOMINA</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9">Fecha: {{date('Y-m-d')}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9" >UBICACIONES TOTALES</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">MANZANA</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">LOTE</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="5">PAGO</td>
        </tr>
        @foreach($totalesUbicacion as $utotales)
            <tr>
                <td style="text-align: center;" colspan="2">{{$utotales->ubicacion->manzana}}</td>
                <td style="text-align: center;" colspan="2">{{$utotales->ubicacion->numero}}</td>
                <td style="text-align: center;" colspan="5">{{ $utotales->pago }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9">EMPLEADOS</td>
        </tr>
        <tr>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" >NOMBRE</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" >PUESTO</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" >RFC</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" >NSS</td>
            <td style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">PAGO</td>
        </tr>
    </thead>
    <tbody>
        @foreach($empleados as $empleado)
            <tr>
                <td style="text-align: center; width: 50px;">{{$empleado->empleado->nombre}}</td>
                <td style="text-align: center; width: 25px;">{{$empleado->empleado->puesto}}</td>
                <td style="text-align: center; width: 35px;">{{$empleado->empleado->rfc}}</td>
                <td style="text-align: center; width: 35px;">{{$empleado->empleado->nss}}</td>
                <td style="text-align: center;" colspan="2">{{$empleado->pago}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
