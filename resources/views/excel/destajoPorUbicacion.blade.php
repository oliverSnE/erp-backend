<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold;" colspan="8">{{ $fraccionamiento->nombre}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="2">Ubicacion</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="4">Destajo</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold" colspan="3">Insumo</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Manzana</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Número</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Nombre</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Mano de obra pagada</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Mano de obra presupuestada</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Porcentaje</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Nombre</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Cantidad presupuestada</th>
            <th style="text-align: center;border: 1px solid #000; font-weight: bold">Cantidad utilizada</th>
        </tr>
    </thead>
    <tbody>
        @php
            $manzana = "";
            $lote  = "";
            $destajo = "";
        @endphp
        @foreach ($destajoUbicacion as $du)
            @if ($manzana != $du->manzana || $lote != $du->numero)
                <tr>
                    <td style="text-align: center;">{{$du->manzana}}</td>
                    <td style="text-align: center;">{{$du->numero}}</td>
                </tr>
                @php
                    $manzana = $du->manzana;
                    $lote = $du->numero;
                @endphp
            @endif
            @if ($destajo != $du->destajo )
                <tr>
                    <td colspan="2"></td>
                    <td style="text-align: center;">{{$du->destajo}}</td>
                    <td style="text-align: center;">{{$du->mano_obra_pagada}}</td>
                    <td style="text-align: center;">{{$du->mano_obra_presupuestada}}</td>
                    <td style="text-align: center;">{{($du->mano_obra_pagada/$du->mano_obra_presupuestada)*100}}</td>
                </tr>
                @php
                    $destajo = $du->destajo;
                @endphp
            @endif
            @if ($du->insumo != null)
                <tr>
                    <td colspan="6"></td>
                    <td style="text-align: center;">{{$du->insumo}}</td>
                    <td style="text-align: center;">{{$du->cantidad_presupuestada}}</td>
                    <td style="text-align: center;">{{$du->cantidad_utilizada}}</td>
                </tr>
            @endif
        @endforeach
    </tbody>
</table>
