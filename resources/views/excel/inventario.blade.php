<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">Fraccionamiento</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="6">Almacen</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">{{$fraccionamiento->nombre}}</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="6">{{$fraccionamiento->direccion}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="9">INVENTARIO</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">Fecha: {{date('Y-m-d')}}</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">Orden de compra</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">Obra</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;">Almacen</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >ID</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Nombre</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Unidad</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Familia</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Comprada</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Recibida</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Pedida</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Entregada</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Cantidad</th>
        </tr>
    </thead>
    <tbody>
    @foreach($insumos as $insumo)
        <tr>
            <td style="width: 5px;">{{ $insumo->id }}</td>
            <td style="width: 70px;">{{ $insumo->nombre }}</td>
            <td>{{ $insumo->unidades->nombre }}</td>
            <td style="width: 20px;">{{ $insumo->familias->nombre }}</td>
            <td style="width: 15px;">{{ round($insumo->cantidad_pedida, 2)  }}</td>
            <td style="width: 15px;">{{ round($insumo->cantidad_recibida, 2)  }}</td>
            <td style="width: 15px;">{{ round($insumo->cantidad_pendiente_en_obra, 2)  }}</td>
            <td style="width: 15px;">{{ round($insumo->cantidad_entregada, 2)  }}</td>
            <td style="width: 15px;">{{ round($insumo->cantidad_recibida -  $insumo->cantidad_entregada, 2) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
