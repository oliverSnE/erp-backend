<table>
    <thead>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">Fraccionamiento</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">Direccion</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">{{$fraccionamiento->nombre}}</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="4">{{$fraccionamiento->direccion}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="8">VALES DE SALIDA POR DESTAJO POR UBICACION</th>
        </tr>
        <tr>
            <th style="text-align: left;border: 1px solid #000;font-weight: bold;" colspan="8">Fecha: {{$fecha}}</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="2">Ubicación</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">Material</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" colspan="3">Salida</th>
        </tr>
        <tr>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Manzana</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Lote</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Destajo</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Nombre</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Unidad</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Cantidad</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Folio</th>
            <th style="text-align: center;border: 1px solid #000;font-weight: bold;" >Fecha</th>
        </tr>
    </thead>
    <tbody>
    @php
        $manzana = "";
        $numero = "";
    @endphp
    @foreach($dataUbicaciones as $material)
        <tr>
            @if ($manzana !== $material->manzana || $numero !== $material->numero)
                <td style="width: 15px;text-align: center;">{{$material->manzana}}</td>
                <td style="width: 15px;text-align: center;">{{$material->numero}}</td>
                @php
                    $manzana = $material->manzana;
                    $numero  = $material->numero;
                @endphp
            @else
                <td style="width: 15px;text-align: center;">{{" "}}</td>
                <td style="width: 15px;text-align: center;">{{" "}}</td>
            @endif
            <td style="width: 70px;text-align: center;">{{$material->destajo}}</td>
            <td style="width: 70px;text-align: center;">{{$material->insumo}}</td>
            <td style="width: 15px;text-align: center;">{{$material->unidad}}</td>
            <td style="width: 15px;text-align: center;">{{$material->cantidad}}</td>
            <td style="width: 15px;text-align: center;">{{$material->folio}}</td>
            <td style="width: 15px;text-align: center;">{{date_format(date_create($material->fecha), "m/d/Y")}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
