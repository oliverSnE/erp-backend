<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{'Orden de Compra '.$orden->folio}}</title>
    <style>
    </style>
    </head>
    <body style="font-family:Lato;font-size: 12px;color: #4A4A4A;">
        <div style="width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 400px;display: table-cell;">
                    <img style="width:250px;" src={{public_path().$fraccionamiento->logo}} alt="">
                </div>
                <div style="margin-left: 400px;display: table-cell;">
                    <span style="margin-left: 80px;font-weight: 400;font-size: 23px;color: #7A1613;"> ORDEN DE COMPRA</span>
                    <div style="height: 20px;">
                        <span style="margin-left: 180px;">FECHA</span>
                    <span style="margin-left: 5px;border: 1xp solid #7A1613;width: 100px;padding: 3px,2px;">{{date('d-m-Y', strtotime($orden->created_at))}}</span>
                    </div>
                    <div style="margin-top: 5px;height:20px;margin-bottom: 5px;">
                        <span style="margin-left: 180px;">FOLIO</span>
                        <span style="margin-left: 9px;border: 1xp solid #7A1613;width: 100px;padding: 3px,2px;">{{$orden->folio}}</span>
                    </div>
                    <div style="margin-top: 5px;height:20px;margin-bottom: 40px;width:100%;text-align: center">
                        @php
                            echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($orden->folio, "C39",1,50) . '" alt="barcode"   />';
                        @endphp
                    </div>
                </div>
            </div>
            <div style="display: table-row;">
                <div style="display: table-cell;">
                    <div style="width: 95%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">FRACCIONAMIENTO:</span></div>
                    <div>{{$fraccionamiento->nombre}}</div>
                    <div style="width: 95%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">ENTREGA:</span></div>
                    <div>Dirección: {{$fraccionamiento->direccion}}</div>
                    <div>Responsable: {{$orden->user->name}}</div>
                    <div style="margin-top: 10px; width: 95%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">PROVEEDOR</span></div>
                    <div>Nombre: {{$orden->proveedor->nombre}}</div>
                    <div>Dirección: {{$orden->proveedor->direccion}} </div>
                    <div>Municipio y Estado: {{$orden->proveedor->municipio.", ".$orden->proveedor->estado}}</div>
                    <div>Código Postal: {{$orden->proveedor->cp}}</div>
                    <div>Telefono: {{$orden->proveedor->tel}}</div>
                    <div>Correo: {{$orden->proveedor->email}}</div>
                </div>
                <div style="margin-left: 400px;display: table-cell;">
                    <div style="width: 300px;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">FACTURACION:</span></div>
                    <div style="width: 300px;">Razon Social: {{$fraccionamiento->razon_social_factura}}</div>
                    <div style="width: 300px;">RFC: {{$fraccionamiento->rfc_factura}}</div>
                    <div style="width: 300px;">Dirección: {{$fraccionamiento->direccion_factura}}</div>
                    <div style="width: 300px;">CP: {{$fraccionamiento->cp_factura}}</div>
                    <div style="width: 300px;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">ETAPA:</span></div>
                    <div style="width: 300px;">{{$etapa->nombre}}</div>
                    <div style="width: 300px;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">UBICACIONES:</span></div>
                    <div style="width: 300px;display: table;margin-top:10px;">
                        <div style="width: 30px;display: table-row;">
                            @php
                                $total = 0;
                                $count = 0;
                            @endphp
                            @foreach ($orden->ubicaciones as $ubicacion)
                                @if($count === 0)
                                    <div style="display: inline-block;width:40px;background-color: white;">
                                @endif
                                <div style="width: 30px;margin-top: 3px;">
                                    <span style="width: 30px;color: #4A4A4A;">{{$ubicacion->manzana."/".$ubicacion->numero}}</span>
                                </div>
                                @php
                                    $total++;
                                    $count++;
                                @endphp
                                @if($count === 3 || $total === sizeof($orden->ubicaciones->toArray()))
                                    @php
                                        $count = 0;
                                    @endphp
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 15px; width: 100%;display: table; border: 1px solid #C7C7C7">
            <div style="display: table-row;color: white;">
                <div style="width: 30px; display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 10px;">CLAVE</span>
                </div>
                <div style="display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 10px;">DESCRIPCION</span>
                </div>
                <div style="width: 80px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left:9px;">CANTIDAD</span>
                </div>
                <div style="width: 80px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 17px;">UNIDAD</span>
                </div>
                <div style="width: 80px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 20px;">P.U.</span>
                </div>
                <div style="width: 100px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 30px;">IMPORTE</span>
                </div>
            </div>
            @php
                $total = 0
            @endphp
            @foreach ($orden->detalle as $index => $detalle)
                <div style="display: table-row">
                    <div style="width: 30px;display: table-cell;">
                        <span style="padding-left: 20px;">{{$detalle->insumo->id}}</span>
                    </div>
                    <div style="display: table-cell;">
                        <span style="padding-left: 10px;">{{$detalle->insumo->nombre}}</span>
                    </div>
                    <div style="width: 80px;display: table-cell;">
                        <span style="padding-left:20px;">{{number_format($detalle->cantidad,2)}}</span>
                    </div>
                    <div style="width: 80px;display: table-cell;">
                        <span style="padding-left: 26px;">{{$detalle->insumo->unidades->nombre}}</span>
                    </div>
                    <div style="width: 80px;display: table-cell;">
                        <span style="padding-left: 20px;">${{number_format($detalle->costo,2)}}</span>
                    </div>
                    <div style="width: 100px;display: table-cell;">
                        <span style="padding-left: 30px;">${{number_format($detalle->costo*$detalle->cantidad,2)}}</span>
                    </div>
                </div>
                @php
                    $total += $detalle->costo*$detalle->cantidad
                @endphp
            @endforeach

        </div>
        <div style="margin-top: 15px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 350px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">COMENTARIOS:</span></div>
                    <div style="width: 100%;border: 1px solid #C7C7C7;height: 100px;"></div>
                </div>
                <div style="margin-left: 400px;display: table-cell;">
                    <div style="width: 100%;display: table;">
                        <div style="display: table-row;">
                            <div style="background-color: white;display: table-cell;">
                                <div style="margin-left: 140px;margin-top: 3px;"><span style="color: #4A4A4A;">SUBTOTAL:&nbsp;&nbsp;$</span></div>
                                </div>
                            <div style="display: table-cell;">
                                <div style="text-align: right; padding-right: 2px;padding-left: 5px;width:120px;height:22px;background-color:#D77370;border-top: 1px solid #C7C7C7;border-left: 1px solid #C7C7C7;border-right: 1px solid #C7C7C7;">
                                    <span style="font-size: 14px;">{{number_format($total,2)}}</span>
                                </div>
                            </div>
                        </div>
                        <div style="display: table-row;">
                            <div style="background-color: white;display: table-cell;">
                                <div style="margin-left: 148px;margin-top: 3px;"><span style="color: #4A4A4A;">IVA (16%):&nbsp;&nbsp;$</span></div>
                            </div>
                            <div style="display: table-cell;">
                                <div style="text-align: right; padding-right: 2px;padding-left: 5px;width:120px;height:22px;background-color:white;border-left: 1px solid #C7C7C7;border-right: 1px solid #C7C7C7;">
                                    <span style="font-size: 14px;">{{number_format($total*.16,2)}}</span>
                                </div>
                            </div>
                        </div>
                        <div style="display: table-row;">
                            <div style="background-color: white;display: table-cell;">
                                <div style="margin-left: 162px;margin-top: 3px;"><span style="color: #7A1613;">TOTAL:&nbsp;&nbsp;$</span></div>
                            </div>
                            <div style="display: table-cell;">
                                <div style="text-align: right; padding-right: 2px;padding-left: 5px;width:120px;height:22px;background-color: #7A1613;border-bottom: 1px solid #C7C7C7;border-left: 1px solid #C7C7C7;border-right: 1px solid #C7C7C7;">
                                    <span style="font-size: 14px;margin-bottom: 3px;color: white;">{{number_format(($total*1.16),2)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 15px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">SOLICITANTE:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;padding-left: 15px;">
                    {{$orden->user->name}}
                </div>
            </div>
        </div>
        <div style="margin-top: 5px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">AUTORIZACION 1:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;padding-left: 15px;">
                    @if($usuario_aprobo !== null)
                        {{$usuario_aprobo->name}}
                    @endif
                </div>
            </div>
        </div>
        <div style="margin-top: 5px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">AUTORIZACION 2:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;">

                </div>
            </div>
        </div>
    </body>
</html>
