<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{'Orden de Compra '.$orden["folio"]}}</title>
    <style>
            /* @font-face {
                font-family: 'Lato';
                /* src: "url({{storage_path('/var/www/html/dynatech-erp-backend/storage/fonts/Lato-Regular.ttf')}})"; */
                src: "url({{storage_path('fonts/Lato-Regular.ttf')}})";
                font-weight: normal;
                font-style: normal;
            } */
    </style>
    </head>
    <body style="font-family:Lato;font-size: 12px;color: #4A4A4A;">
        <div style="width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 400px;display: table-cell;">
                    <img style="width:220px;" src={{public_path().$fraccionamiento->logo}} alt="">
                </div>
                <div style="margin-left: 400px;display: table-cell;">
                    <span style="margin-left: 20px;font-weight: 400;font-size: 23px;color: #7A1613;">ORDEN DE SALIDA</span>
                    <div style="height: 20px;">
                        <span style="margin-left: 180px;">FECHA</span>
                    <span style="margin-left: 5px;border: 1xp solid #7A1613;width: 100px;padding: 3px,2px;">{{date('d-m-Y', strtotime($orden["created_at"]))}}</span>
                    </div>
                    <div style="margin-top: 5px;height:20px;margin-bottom: 5px;">
                        <span style="margin-left: 180px;">FOLIO</span>
                        <span style="margin-left: 9px;border: 1xp solid #7A1613;width: 100px;padding: 3px,2px;">{{$orden["folio"]}}</span>
                    </div>
                    <div style="margin-top: 5px;height:20px;margin-bottom: 40px;width:100%;text-align: center">
                        @php
                            echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($orden["folio"], "C39",1,50) . '" alt="barcode"   />';
                        @endphp
                    </div>
                </div>
            </div>
            <div style="display: table-row;">
                <div style="display: table-cell;">
                    <div style="width: 95%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">FRACCIONAMIENTO:</span></div>
                    <div>{{$fraccionamiento->nombre}}</div>
                    <div>Dirección: {{$fraccionamiento->direccion}}</div>
                </div>
                <div style="margin-left: 400px;display: table-cell;">
                    <div style="width: 100%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">UBICACION:</span></div>
                <div>{{$detalles[0]->ubicacion->manzana}}/{{$detalles[0]->ubicacion->numero}}</div>
                    <div style="width: 100%;background-color:#7A1613;padding: 3px,2px;"><span style="color: white;">DESTAJO:</span></div>
                    <div>{{$detalles[0]->detalleDestajos->destajo->nombre}}</div>
                </div>
            </div>
        </div>
        <div style="margin-top: 15px; width: 100%;display: table; border: 1px solid #C7C7C7">
            <div style="display: table-row;color: white;">
                <div style="width: 30px; display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 10px;">CLAVE</span>
                </div>
                <div style="display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 10px;">DESCRIPCION</span>
                </div>
                <div style="width: 80px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left:9px;">CANTIDAD</span>
                </div>
                <div style="width: 80px;display: table-cell;background-color:#7A1613;">
                    <span style="padding-left: 17px;">UNIDAD</span>
                </div>
            </div>
            @foreach ($detalles as $index => $detalle)
                <div style="display: table-row">
                    <div style="width: 30px;display: table-cell;">
                        <span style="padding-left: 20px;">{{$detalle->detalleDestajos->insumo->id}}</span>
                    </div>
                    <div style="display: table-cell;">
                        <span style="padding-left: 10px;">{{$detalle->detalleDestajos->insumo->nombre}}</span>
                    </div>
                    <div style="width: 80px;display: table-cell;">
                        <span style="padding-left:20px;">{{number_format($detalle->cantidad,2)}}</span>
                    </div>
                    <div style="width: 80px;display: table-cell;">
                        <span style="padding-left: 26px;">{{$detalle->detalleDestajos->insumo->unidades->nombre}}</span>
                    </div>
                </div>
            @endforeach

        </div>
        <div style="margin-top: 15px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 350px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">COMENTARIOS:</span></div>
                    <div style="width: 100%;border: 1px solid #C7C7C7;height: 100px;"></div>
                </div>
            </div>
        </div>
        <div style="margin-top: 15px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">SOLICITANTE:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;padding-left: 15px;">
                    {{$orden["user"]["name"]}}
                </div>
            </div>
        </div>
        <div style="margin-top: 5px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">ENTREGAR A:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;padding-left: 15px;">

                </div>
            </div>
        </div>
        <div style="margin-top: 5px; width: 100%;display: table;">
            <div style="display: table-row;">
                <div style="width: 130px;background-color: white;display: table-cell;">
                    <div style="width: 100%;background-color:#C7C7C7;padding: 3px,2px;"><span style="color: #7A1613;">ALMACENISTA:</span></div>
                </div>
                <div style="border: 1px solid #C7C7C7;display: table-cell;">

                </div>
            </div>
        </div>
    </body>
</html>
