<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <style>
        body {
            color: black;
        }
        .container {
            margin: 1%;
        }
        .header-img {
            width:100%;
        }
        .footer-img {
            width:100%;
        }
        .date {
            position: fixed;
            color: #fff;
            right: 10px;
            font-size: 20px;

        }
        .message {
            margin: 5%;
        }
        .footer{
            width: 100%;
            position: fixed;
            bottom: 0px;
        }
        .subject{
            text-align: center;
        }
    </style>

</head>
<body>
  <div class="container">
    <div>
        <img class="header-img" src="{{ $message->embed(storage_path('./assets/mail-header.png')) }}" alt="header sacomi">
    </div>

    <div class="message">
    <h2 class="subject"> {{  $data['subject']  }} {{ $data['status'] }}</h2> <br>
        <h3> {{  $data['content']  }}</h3> <br>
        <span> {{  $data['link']  }}</span><br>
    </div>
    <div class="footer">
        <img class="footer-img" src="{{ $message->embed(storage_path('./assets/mail-footer.png')) }}" alt="footer sacomi">
    </div>
  </div>

</body>
</html>
