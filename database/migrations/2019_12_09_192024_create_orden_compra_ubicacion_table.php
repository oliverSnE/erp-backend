<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenCompraUbicacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_compra_ubicacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orden_compra_id')->after('id');
            $table->foreign('orden_compra_id')->references('id')->on('orden_compras');
            $table->unsignedBigInteger('ubicacion_id')->after('orden_compra_id');
            $table->foreign('ubicacion_id')->references('id')->on('ubicaciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_compra_ubicacion');
    }
}
