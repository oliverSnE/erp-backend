<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleOrdenSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_orden_salidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orden_salida_id')->after('id');
            $table->foreign('orden_salida_id')->references('id')->on('orden_salidas');
            $table->unsignedBigInteger('detalle_destajo_id')->after('orden_salida_id');
            $table->foreign('detalle_destajo_id')->references('id')->on('detalle_destajos');
            $table->unsignedBigInteger('ubicacion_id')->after('detalle_destajo_id');
            $table->foreign('ubicacion_id')->references('id')->on('ubicaciones');
            $table->float('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_orden_salidas');
    }
}
