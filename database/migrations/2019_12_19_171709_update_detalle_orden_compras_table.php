<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDetalleOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->dropForeign(['detalle_destajo_id']);
            $table->dropColumn('detalle_destajo_id');
            $table->unsignedBigInteger('insumo_id')->after('orden_compra_id');
            $table->foreign('insumo_id')->references('id')->on('insumos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->dropForeign(['insumo_id']);
            $table->dropColumn('insumo_id');
            $table->unsignedBigInteger('detalle_destajo_id')->after('orden_compra_id');
            $table->foreign('detalle_destajo_id')->references('id')->on('detalle_destajos');
        });
    }
}
