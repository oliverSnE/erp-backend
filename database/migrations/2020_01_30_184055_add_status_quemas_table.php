<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusQuemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quemas', function (Blueprint $table) {
            $table->enum('tipo', array('destajo','subcontrato'))->default('destajo');
            $table->unsignedBigInteger('fraccionamiento_id')->after('id');
            $table->foreign('fraccionamiento_id')->references('id')->on('fraccionamientos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quemas', function (Blueprint $table) {
            $table->dropForeign(['fraccionamiento_id']);
            $table->dropColumn('fraccionamiento_id');
        });

    }
}
