<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_entradas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entrada_id')->after('id');
            $table->foreign('entrada_id')->references('id')->on('entradas');
            $table->unsignedBigInteger('detalle_orden_compra_id')->after('entrada_id');
            $table->foreign('detalle_orden_compra_id')->references('id')->on('detalle_orden_compras');
            $table->float('cantidad');
            $table->float('costo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_entradas');
    }
}
