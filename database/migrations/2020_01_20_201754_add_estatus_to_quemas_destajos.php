<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEstatusToQuemasDestajos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quemas_destajos', function (Blueprint $table) {
            $table->enum('estatus', array('creada','cancelada', 'aprobada'))->default('creada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quemas_destajos', function (Blueprint $table) {
            $table->dropColumn('estatus')->nullable();
        });
    }
}
