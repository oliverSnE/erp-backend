<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_modelos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('destajo_id')->nullable();
            $table->foreign('destajo_id')->references('id')->on('destajos');
            $table->unsignedBigInteger('modelo_id')->nullable();
            $table->foreign('modelo_id')->references('id')->on('modelos');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_modelos', function (Blueprint $table) {
            $table->dropForeign(['destajo_id']);
            $table->dropColumn('destajo_id');
            $table->dropForeign(['modelo_id']);
            $table->dropColumn('modelo_id');
         });
         Schema::dropIfExists('detalle_modelos');
    }
}
