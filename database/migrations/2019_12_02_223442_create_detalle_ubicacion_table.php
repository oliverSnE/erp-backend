<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleUbicacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_ubicaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('destajo_id')->after('id');
            $table->foreign('destajo_id')->references('id')->on('destajos');
            $table->unsignedBigInteger('ubicacion_id')->after('destajo_id');
            $table->foreign('ubicacion_id')->references('id')->on('ubicaciones');
            $table->enum('tipo', array('agregado','eliminado'));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_ubicaciones');
    }
}
