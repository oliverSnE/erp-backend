<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedorFamiliaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familia_proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('familia_id')->after('id');
            $table->foreign('familia_id')->references('id')->on('familias');
            $table->unsignedBigInteger('proveedores_id')->after('familia_id');
            $table->foreign('proveedores_id')->references('id')->on('proveedores');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('familia_proveedores', function (Blueprint $table) {
            $table->dropForeign(['familia_id']);
            $table->dropColumn('familia_id');
            $table->dropForeign(['proveedores_id']);
            $table->dropColumn('proveedores_id');
        });
        Schema::dropIfExists('familia_proveedores');
    }
}
