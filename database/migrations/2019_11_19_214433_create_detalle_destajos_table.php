<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleDestajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_destajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('cantidad');
            $table->float('costo');
            $table->unsignedBigInteger('insumo_id');
            $table->unsignedBigInteger('destajo_id');
            $table->foreign('insumo_id')->references('id')->on('insumos');
            $table->foreign('destajo_id')->references('id')->on('destajos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_destajos', function (Blueprint $table) {
            $table->dropForeign(['insumo_id']);
            $table->dropColumn('insumo_id');
            $table->dropForeign(['destajo_id']);
            $table->dropColumn('destajo_id');
        });
        Schema::dropIfExists('detalle_destajos');
    }
}
