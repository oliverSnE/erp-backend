<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuemasDestajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quemas_destajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('quema_id')->after('id');
            $table->foreign('quema_id')->references('id')->on('quemas');
            $table->unsignedBigInteger('ubicacion_id')->after('quema_id');
            $table->foreign('ubicacion_id')->references('id')->on('ubicaciones');
            $table->unsignedBigInteger('destajo_id')->after('ubicacion_id');
            $table->foreign('destajo_id')->references('id')->on('destajos');
            $table->float('costo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quemas_destajos');
    }
}
