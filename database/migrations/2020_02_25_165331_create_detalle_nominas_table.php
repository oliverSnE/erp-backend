<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleNominasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_nominas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('quema_id')->after('id');
            $table->foreign('quema_id')->references('id')->on('quemas');
            $table->unsignedBigInteger('nomina_id')->after('quema_id');
            $table->foreign('nomina_id')->references('id')->on('nominas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_nominas', function(Blueprint $table) {
            $table->dropForeign(['quema_id']);
            $table->dropColumn('quema_id');
            $table->dropForeign(['nomina_id']);
            $table->dropColumn('nomina_id');
        });
        Schema::dropIfExists('detalle_nominas');
    }
}
