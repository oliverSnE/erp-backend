<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexDetalleOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->unsignedBigInteger('orden_compra_id')->after('id');
            $table->foreign('orden_compra_id')->references('id')->on('orden_compras');
        });

        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->unsignedBigInteger('detalle_destajo_id')->after('orden_compra_id');
            $table->foreign('detalle_destajo_id')->references('id')->on('detalle_destajos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->dropForeign(['orden_compra_id']);
            $table->dropColumn('orden_compra_id');
        });

        Schema::table('detalle_orden_compras', function (Blueprint $table) {
            $table->dropForeign(['detalle_destajo_id']);
            $table->dropColumn('detalle_destajo_id');
        });
    }
}
