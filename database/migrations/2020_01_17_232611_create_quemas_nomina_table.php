<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuemasNominaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quemas_nomina', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('quema_id')->after('id');
            $table->foreign('quema_id')->references('id')->on('quemas');
            $table->unsignedBigInteger('empleado_id')->after('quema_id');
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->boolean('is_jefe')->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quemas_nomina');
    }
}
