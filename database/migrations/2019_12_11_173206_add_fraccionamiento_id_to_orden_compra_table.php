<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFraccionamientoIdToOrdenCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orden_compras', function (Blueprint $table) {
            $table->unsignedBigInteger('fraccionamiento_id')->nullable();
            $table->foreign('fraccionamiento_id')->references('id')->on('fraccionamientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orden_compra', function (Blueprint $table) {
            $table->dropForeign(['fraccionamiento_id']);
            $table->dropColumn('fraccionamiento_id');
        });
    }
}
