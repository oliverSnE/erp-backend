<?php

use Illuminate\Database\Seeder;
use App\Models\Unidade;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unidade::create([
            'nombre'              =>  'Kg'
        ]);
        Unidade::create([
            'nombre'              =>  'gr'
        ]);
        Unidade::create([
            'nombre'              =>  'm'
        ]);
        Unidade::create([
            'nombre'              =>  'm²'
        ]);
        Unidade::create([
            'nombre'              =>  'ml'
        ]);
        Unidade::create([
            'nombre'              =>  'L'
        ]);
        Unidade::create([
            'nombre'              =>  'MT'
        ]);
        Unidade::create([
            'nombre'              =>  'pza'
        ]);
        Unidade::create([
            'nombre'              =>  'pzas'
        ]);
        Unidade::create([
            'nombre'              =>  'TON'
        ]);
    }
}
