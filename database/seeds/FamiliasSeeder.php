<?php

use Illuminate\Database\Seeder;
use App\Models\Familia;

class FamiliasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Familia::create([
            'nombre'              =>  'ACERO'
        ]);
        Familia::create([
            'nombre'              =>  'ELECTRICIDAD'
        ]);
        Familia::create([
            'nombre'              =>  'PLOMERÍA'
        ]);
        Familia::create([
            'nombre'              =>  'POLVOS'
        ]);
        Familia::create([
            'nombre'              =>  'SIN FAMILIA'
        ]);
    }
}
