<?php

use Illuminate\Database\Seeder;
use App\Models\Proveedores;

class ProveedoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proveedores::create([
            'nombre'             =>  'Materiales y viguetas del noroeste SA de CV' ,
            'direccion'          =>  'Blvd Solidaridad y Rebeico 726 Col. Norberto Ortega' ,
            'municipio'          =>  'Hermosillo' ,
            'estado'             =>  'Sonora' ,
            'cp'              	 =>  '83116' ,
            'tel'              	 =>  '1187618' ,
            'email'              =>  'contacto@myv.com'
        ]);

        Proveedores::create([
            'nombre'              =>  'Serdi SA de CV' ,
            'direccion'           =>  'Blvd García Morales 475 Col. El Llano' ,
            'municipio'           =>  'Hermosillo' ,
            'estado'              =>  'Sonora' ,
            'cp'              	  =>  '83220 ' ,
            'tel'              	  =>  '2610099' ,
            'email'               =>  'contacto@serdi.com'
        ]);

        Proveedores::create([
            'nombre'         =>  'Agregados de la Costa SA de CV (Agrecoblocks)' ,
            'direccion'      =>  'Carretera Bahía de Kino KM 5.5' ,
            'municipio'      =>  'Hermosillo' ,
            'estado'         =>  'Sonora' ,
            'cp'             =>  '83220 ' ,
            'tel'            =>  '6622365900' ,
            'email'          =>  'contacto@agrecoblocks.com'
        ]);

        Proveedores::create([
            'nombre'         =>  'Almada Urrea SA de CV' ,
            'direccion'      =>  'Carretera Internacional KM 4.5' ,
            'municipio'      =>  'Obregon' ,
            'estado'         =>  'Sonora' ,
            'cp'             =>  '85000' ,
            'tel'            =>  '6444108800' ,
            'email'          =>  'contacto@almurr.com'
        ]);        

        Proveedores::create([
            'nombre'         =>  'Precoas SA de CV' ,
            'direccion'      =>  'Blvd Antonio Quiroga S N Fracc. Villa del Prado' ,
            'municipio'      =>  'Hermosillo' ,
            'estado'         =>  'Sonora' ,
            'cp'             =>  '83287' ,
            'tel'            =>  '2091111' ,
            'email'          =>  'precoas.ventas@gmail.com'
        ]); 

        Proveedores::create([
            'nombre'         =>  'Sistema de Losas y Materiales SA de CV' ,
            'direccion'      =>  'Carretera Hermosillo-Ures S N Col. San Pedro' ,
            'municipio'      =>  'Hermosillo' ,
            'estado'         =>  'Sonora' ,
            'cp'             =>  '83200' ,
            'tel'            =>  '2800136' ,
            'email'          =>  'contacto@sisloza.com'
        ]);

        Proveedores::create([
            'nombre'         =>  'Comar Comercializadora' ,
            'direccion'      =>  'República de Guatemala 162 Col. Inalambrica' ,
            'municipio'      =>  'Hermosillo' ,
            'estado'         =>  'Sonora' ,
            'cp'             =>  '83198' ,
            'tel'            =>  '2600816' ,
            'email'          =>  'comar.contador@hotmail.com'
        ]);         
    }
}
