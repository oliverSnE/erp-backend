<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->truncateTables([
            'unidades',
            'familias',
            'proveedores'
        ]);

        $this->call(UnidadesSeeder::class);
        $this->call(FamiliasSeeder::class);
        $this->call(ProveedoresSeeder::class);

    }

    protected function truncateTables(array $tables)
    {
        DB::statement(" SET session_replication_role = 'replica'; ");
        foreach($tables as $table)
        {
            DB::table($table)->truncate();
        }
        DB::statement(" SET session_replication_role = 'origin'; ");
    }

}
